The is the Secure Distributed ML Source code.

To compile and run the following dependencies must be installed.
The software has only been tested with the version given and up.

* OCaml version 4.01.0
* Node.js version 4.4.2
* NPM 4.1.2

Setup:
Once the above requirements are installed, running the
"setup.sh" script will install the needed NPM modules,
compile the compiler, and pack and install the DC Label NPM module.

Usage:
Once setup has completed, the compiler is available as "main.native"
and compiled programs can be run with Node.js. More detailed usage
instructions are available in the report "SDML: A Secure Language
for Distributed Computing" by Frederik Skytte Nielsen.
