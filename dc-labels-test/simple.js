var labels = require("dc-labels");
function get_callback(label){
    return function(error, res){
        console.log(label + res);
    }
}

var a = labels.get_label("a");
var b = labels.get_label("b");
var c = labels.get_label("c");

labels.flows_to(a, a, null, get_callback("a > a: "));

labels.flows_to(a, b, null, get_callback("a > b: "));

labels.flows_to(labels.and(a, b), b, null, get_callback("a & b > b: "));

labels.flows_to(labels.or(a, b), b, null, get_callback("a | b > b: "));

labels.flows_to(b, labels.or(a, b), null, get_callback("b > a | b: "));

labels.flows_to(b, labels.and(a, b), null, get_callback("b > a & b: "));

labels.flows_to(labels.and(a, b), labels.and(a, b), null, get_callback("a & b > a & b: "));

labels.flows_to(labels.and(a, b), labels.and(a, labels.and(b, c)), null, get_callback("a & b > a & b & c: "));

labels.flows_to(labels.and(a, labels.and(b, c)), labels.and(a, b), null, get_callback("a & b & c > a & b: "));

labels.flows_to(a, labels.bottom, null, get_callback("a > bottom: "));

labels.flows_to(labels.bottom, a, null, get_callback("bottom > a: "));

labels.flows_to(labels.bottom, labels.bottom, null, get_callback("bottom > bottom: "));

labels.flows_to(a, labels.top, null, get_callback("a > top: "));

labels.flows_to(labels.top, a, null, get_callback("top > a: "));

labels.flows_to(labels.top, labels.top, null, get_callback("top > top: "));

