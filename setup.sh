#!/bin/sh

echo 'Compiling compiler'
make
echo 'Installing NPM dependencies'
npm install
echo 'Building DC Label module'
cd dc-labels
npm pack
cd ..
echo 'Installing DC Label module'
npm install dc-labels/dc-labels-1.0.0.tgz
echo 'SDML set up'
