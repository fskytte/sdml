exports.maps = [];

exports.create_map = function(){
    var new_i = exports.maps.length;
    exports.maps[new_i] = {};
    return new_i;
}

exports.add = function(map, key, value){
    console.log("Map: " + map);
    exports.maps[map][key] = value;
    return map;
}

exports.get = function(map, key){
    return exports.maps[map][key];
}

exports.reset = function(map){
    exports.maps[map] = {};
    return map;
}
