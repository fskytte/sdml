//Inspired by https://docs.npmjs.com/getting-started/creating-node-modules
exports.test_func = function(name){
    console.log("Test func called");
    return "Hello " + name;
};

exports.add = function(a, b){
    console.log("Add called with:");
    console.log(arguments);
    return a + b;
};

exports.async_add = function(a, b, callback){
    callback(a + b);
}

exports.three_args = function(a, b, c){
    return a + b + c;
};

exports.call_func = function(callback){
    callback();
};

exports.async_two_numbers = function(a, callback){
    callback(a, 2);
};

exports.async_many_numbers = function(callback){
    callback(1,2,3,4,5,6,7);
};

exports.return_module = function(){
    return {hello: function(){
        console.log("Hello!");
    }};
};

exports.return_val_in_object = function(){
    return {value: 24};
};

exports.express_get_like = function(s, fun){
    console.log("Arg is " + s);
    fun(1, 2);
};

exports.stateful_module = function(){
    return {num: 2,
            add_to_num: function(a){
                return this.num + a;
            }
           };
}
