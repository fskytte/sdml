# Original file from https://github.com/ocaml/ocamlbuild/blob/master/examples/05-lex-yacc/Makefile
#
# Pure OCaml, package from Opam, two directories
#

# - The -I flag introduces sub-directories 
# - -use-ocamlfind is required to find packages (from Opam)
# - _tags file introduces packages, bin_annot flag for tool chain
# - using *.mll and *.mly are handled automatically

OCB_FLAGS = -use-ocamlfind             -I src # uses ocamlyacc
#OCB_FLAGS   = -use-ocamlfind -use-menhir -I src -I lib # uses menhir

OCB = 		ocamlbuild $(OCB_FLAGS)

all: 		native

clean:
		$(OCB) -clean

native:  	
		$(OCB) main.native 

.PHONY: 	all clean native
