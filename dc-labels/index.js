//Inspired by https://docs.npmjs.com/getting-started/creating-node-modules
//https://www.npmjs.com/package/propsat
var propsat = require("propsat");

//https://nodejs.org/api/child_process.html
var exec = require("child_process").exec;

function replace_recurring(prop, elem){
    switch(prop.op){
    case "v":
        if(compare(prop, elem)){
            return undefined;
        } else {
            return propsat.copyPropForm(prop);
        }
    case "n":
        return propsat.copyPropForm(prop);
    case "a":
        var first = replace_recurring(prop.args[0], elem);
        var second = replace_recurring(prop.args[1], elem);
        if(first === undefined && second === undefined){
            return undefined;
        }
        if(first === undefined){
            return propsat.copyPropForm(prop.args[1]);
        }
        if(second === undefined){
            return propsat.copyPropForm(prop.args[0]);
        }
        return {op: 'a', args: [first, second]};
    case "o":
        if(compare(prop, elem)){
            return undefined;
        } else {
            return propsat.copyPropForm(prop);
        }
    }
}

function compare(prop1, prop2){
    switch(prop1.op){
    case "v":
        return prop2.op === "v" && prop1.name === prop2.name;
    case "n":
        return prop2.op === "n" && compare(prop1.args[0], prop2.args[0]);
    case "a":
        return prop2.op === "a" && compare(prop1.args[0], prop2.args[0]) && compare(prop1.args[1], prop2.args[1]);
    case "o":
        return prop2.op === "o" && compare(prop1.args[0], prop2.args[0]) && compare(prop1.args[1], prop2.args[1]);
    }
}

function find_clauses(prop){
    switch(prop.op){
    case "a":
        return find_clauses(prop.args[0]).concat(find_clauses(prop.args[1]));
    default:
        return [prop];
    }
}

exports.find_clauses = find_clauses;
exports.replace_recurring = replace_recurring;

function cnf(prop){
    return propsat.propFormToCnfSync(prop);
}

exports.and = function(a,b) {
    var b_clauses = find_clauses(b);
    for(var i = 0; i < b_clauses.length; i++){
        a = replace_recurring(a, b_clauses[i]);
        if(a === undefined){
            return b;
        }
    }
    //a = propsat.simplifyPropForm(a);
    var a_string = exports.to_string(a);
    var b_string = exports.to_string(b);
    if(a_string === b_string) {
        return a;
    } else if(a_string === "_bottom"){
        return b;
    } else if(b_string === "_bottom"){
        return a;
    } else if(a_string === "_top" || b_string === "_top"){
        return exports.top;
    } else {
        return cnf("(" + exports.to_string(a) + ") & (" + exports.to_string(b) + ")");
    }
};

exports.or = function(a,b){
    var b_clauses = find_clauses(b);
    for(var i = 0; i < b_clauses.length; i++){
        a = replace_recurring(a, b);
        if(a === undefined){
            return b;
        }
    }
    //a = propsat.simplifyPropForm(a);
    var a_string = exports.to_string(a);
    var b_string = exports.to_string(b);
    if(a_string === b_string){
        return a;
    } else if(a_string === "_top"){
        return b;
    } else if(b_string === "_top"){
        return a;
    } else if(a_string === "_bottom" || b_string === "_bottom"){
        return a;
    } else {
        return cnf("(" + exports.to_string(a) + ") | (" + exports.to_string(b) + ")");
    }
};

exports.get_label = function(label){
    return cnf(label);
}

exports.bottom = cnf("_bottom");

exports.top = cnf("_top");

exports.join = function(a,b) {
    if(a === undefined){
        return b;
    }
    else if(b === undefined){
        return a;
    }
    else if(a === b){
        return a;
    }
    /*console.log("Joining:");
    console.log(exports.to_string(a));
    console.log("and");
    console.log(exports.to_string(b));
    console.log("As");
    console.log(exports.to_string(exports.and(a, b)));*/
    return exports.and(a, b);
};

exports.meet = function(a,b) {
    return exports.or(a, b);
};

//https://www.dwheeler.com/essays/minisat-user-guide.html
function create_minisat_representation(prop){
    var dict = {};
    var clauses = 1;
    var counter = 0;

    function lookup(v){
        if(dict[v] === undefined){
            counter++;
            var new_val = counter;
            dict[v] = counter;
            return new_val;
        } else {
            return dict[v];
        }
    }
    
    function helper(prop){
        switch(prop.op){
        case "v":
            return lookup(prop.name);
        case "n":
            return "-" + helper(prop.args[0]);
        case "a":
            clauses++;
            return helper(prop.args[0]) + " 0\\n" + helper(prop.args[1]);
        case "o":
            return helper(prop.args[0]) + " " + helper(prop.args[1]);
        }
    }

    var solution = helper(prop);

    return "p cnf " + counter + " " + clauses + '\\n' + solution + " 0";
}

function flows_to(a, b, privileges, callback){
    var a_string = exports.to_string(a);
    var b_string = exports.to_string(b);
    if(a_string === "_bottom"){
        callback(null, true);
    } else if(a_string === "_top"){
        callback(null, b_string === "_top");
    } else if(b_string === "_top"){
        callback(null, true);
    } else if(b_string === "_bottom"){
        callback(null, a_string === "_bottom");
    } else {
        var prop = undefined;
        //Negating formula, since negating the CNF, and checking that it is unsatisfiable means that the formula is a tautology
        //http://web.cecs.pdx.edu/~hook/logicw11/Exercises/Ex4Minisat.html
        //https://www.npmjs.com/package/propsat
        if(privileges){
            prop = propsat.propFormToCnfSync("~(" + exports.to_string(privileges) + " & " + b_string + "->" + a_string + ")");
        } else {
            prop = propsat.propFormToCnfSync("~(" + b_string + "->" + a_string + ")");
        }
        //propsat.isPropFormTautology(prop, callback);
        var minisat_form = create_minisat_representation(prop);
        exec('echo "' + minisat_form + '" | minisat/minisat', function(error, res, stderr){
            //Cannot check error, since that relies on the return code of the program, which is always non-zero in propsat
            var possible_unsat = res.substring(res.length - 14, res.length);
            callback(null, possible_unsat.trim() === "UNSATISFIABLE");
        });
    }
};

exports.flows_to = flows_to;

function to_string(label){
    if(label === undefined){
        return "undefined";
    }
    if(label === null){
        return "null";
    }
    return propsat.propFormToString(label, true, "asciiSLT");
}

exports.to_string = to_string;

function array_equals(a, b){
    if(a.length !== b.length){
        return false;
    }
    for(var i = 0; i < a.length; i++){
        if(a[i] !== b[i]){
            return false;
        }
    }
    return true;
}

exports.declassify = function(old_label, new_label, authority, callback){
    if(!authority){
        callback(null, false);
        return;
    }

    var privileges = null;
    if(authority.length > 0){
        privileges = exports.bottom;
        for(var i = 0; i < authority.length; i++){
            privileges = exports.and(privileges, exports.get_label(authority[i]));
        }
    }
    
    exports.flows_to(new_label, old_label, privileges, function(err, res){
        if(err){
            callback(err, null);
            return;
        }
        if(!res){
            callback(null, false);
            return;
        }

        var old_clauses = propsat.propFormToClausesSync(old_label);
        var new_clauses = propsat.propFormToClausesSync(new_label);

        if(exports.to_string(new_label) === "_bottom"){
            if(old_clauses.find(function(e1){
                return !e1.find(function(e2){
                    return authority.indexOf(e2) !== -1;
                });
            })){
                callback(null, false);
                return;
            }
        }

        //Inspired by: https://stackoverflow.com/a/33034768
        var different = old_clauses.filter(function(x){
            var res = true;
            for(var i = 0; i < new_clauses.length; i++){
                if(array_equals(x, new_clauses[i])){
                    res = false;
                    break;
                }
            }
            return res;
        });
        /*for(var i = 0; i < old_clauses.length; i++){
            if(new_clauses.find(function(e){
                return !array_equals(old_clauses[i], e);
            })){
                different.push(old_clauses[i]);
            }
            }*/

        for(var i = 0; i < different.length; i++){
            if(!different[i].find(function(e){
                return authority.indexOf(e) !== -1;
            })){
                callback(null, false);
                return;
            }
        }

        callback(null, true);
        return;
    });
};
