time="$(date +"%T.%3N")"
echo "Start: ${time}" > $2
$1
time="$(date +"%T.%3N")"
echo "End: ${time}" >> $2
