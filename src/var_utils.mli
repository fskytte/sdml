val get_cont_name : unit -> string
val get_var_name : unit -> string
val get_func_name : unit -> string
val get_env_name : unit -> string
val get_module_name : unit -> string
val is_fix_fun : string -> bool
val replace_vars : Ast.exp_intern -> (string * Ast.exp_intern) list -> Ast.exp_intern
