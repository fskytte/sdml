open Constants

exception Should_not_happen of string

let rec get_whitespace i =
  if i = 0 then
    ""
  else
    "   " ^ (get_whitespace (i - 1));;

let rec string_of_string_list l =
  match l with
    [] -> ""
  | [x] -> x
  | x :: xs -> x ^ ", " ^ (string_of_string_list xs);;

let rec escape_string str =
  let res = ref "" in
  String.iter (fun c -> res := !res ^ (Char.escaped c)) str;
  !res;;
  
let rec list_contains l elem =
  List.exists (fun x -> x = elem) l;;

let rec replace_or_append f l1 l2 =
  let sorted_l1 = List.sort f l1 in
  let sorted_l2 = List.sort f l2 in
  match sorted_l1 with
    [] -> l2
  | x1 :: xs1 -> (match sorted_l2 with
                    [] -> x1 :: replace_or_append f xs1 sorted_l2
                  | x2 :: xs2 -> if (f x1 x2) = 0 then
                                   x2 :: replace_or_append f xs1 xs2
                                 else
                                   x1 :: replace_or_append f xs1 sorted_l2);;

let merge a b =
  let sorted = List.sort compare (a @ b) in
  let rec remove_duplicates l res =
    match l with
      [] -> res
    | x :: xs -> if res <> [] && x = List.hd res then
                   remove_duplicates xs res
                 else
                   remove_duplicates xs (x :: res)
  in remove_duplicates sorted [];;

let rng = Random.self_init ();;
  
let random_number () =
  Random.int64 Int64.max_int;;

let ensure_unique f l =
  let sorted = List.sort f l in
  let rec remove_duplicates l res =
    match l with
      [] -> res
    | x :: xs -> if res <> [] && (f x (List.hd res)) = 0 then
                   remove_duplicates xs res
                 else
                   remove_duplicates xs (x :: res)
  in remove_duplicates sorted [];;


(* File handling: https://ocaml.org/learn/tutorials/file_manipulation.html*)
let read_file file_name =
  let in_channel = open_in file_name in
  let contents = ref "" in
  let rec helper () =
    try
      let line = input_line in_channel in
      contents := !contents ^ "\n" ^ line;
      helper ()
    with End_of_file -> close_in in_channel
       | e -> close_in_noerr in_channel;
              raise e
  in
  helper ();
  !contents;;

let check_if_std a =
  ((String.get a 0) = '_' && (String.get a 1) <> '_')
  || List.exists (fun x -> x = a) standard_lib;;
