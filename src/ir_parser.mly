%{
  open Ast

  let pos = Pos (-1, -1);;

  (* Inspired by http://courses.softlab.ntua.gr/compilers/2015a/ocamlyacc-tutorial.pdf and
                 http://stackoverflow.com/questions/1933101/ocamlyacc-parse-error-what-token *)
  let parse_error s =
    let token = (match !Ast.cur_lexbuf with
                   None -> ""
                 | Some lexbuf -> Lexing.lexeme lexbuf) in
    raise (Parse_error (token, pos));;
%}
%token <int> INT_VAL
%token <float> FLOAT_VAL
%token <string> IDENT
%token <string> STRING_VAL
%token LPAREN
%token RPAREN
%token LBRACKET
%token RBRACKET
%token COMMA
%token BOOL_AND BOOL_OR
%token LOGIC_AND LOGIC_OR
%token EQUAL LT LE GT GE NE
%token PLUS MINUS
%token MULT DIV
%token CONCAT UNDERSCORE
%token INT FLOAT BOOL STRING UNIT
%token CONST FUNCTION MODULE_ACCESS LET CLASSIFY DECLASSIFY LETREC SEQ VAR APP SEND SEND_SYNC RECEIVE CHECK_LABEL
%token IF TUPLE TUPLE_ACCESS TUPLE_LENGTH LIST HD TL CONS BINOP
%token TRUE FALSE

%start main
%type <Ast.program> main
%%

main:
   module_decls exp {Program ($1, $2)}

module_decls:
   LBRACKET module_decls_contents RBRACKET {$2}

module_decls_contents:
   {[]} //Nothing
 | IDENT  {[("", $1, false)]}
 | module_decls_contents COMMA IDENT {$1 @ [("", $3, false)]}

exp:
   LPAREN exp_body RPAREN {$2}

exp_body:
   CONST const_val {Const ($2, pos)}
 | CHECK_LABEL exp label TRUE {Check_label ($2, $3, true, pos)}
 | CHECK_LABEL exp label FALSE {Check_label ($2, $3, false, pos)}
 | FUNCTION ident_list exp {Function ($2, $3, pos)}
 | MODULE_ACCESS IDENT IDENT {Module_access ($2, $3, pos)}
 | LET IDENT exp exp {Let ($2, $3, $4, pos)}
 | CLASSIFY exp label {Classify ($2, $3, pos)}
 | DECLASSIFY exp label {Declassify ($2, $3, pos)}
 | LETREC letrec_list exp {Letrec ($2, $3, pos)}
 | SEQ exp exp {Seq ($2, $3, pos)}
 | VAR IDENT {Var ($2, pos)}
 | APP exp exp_list {App ($2, $3, pos)}
 | SEND exp exp ident_list {Send ($2, $3, $4, pos)}
 | RECEIVE {Receive pos}
 | IF exp exp exp {If ($2, $3, $4, pos)}
 | TUPLE exp_list {Tuple ($2, pos)}
 | TUPLE_ACCESS exp exp {Tuple_access ($2, $3, pos)}
 | TUPLE_LENGTH exp {Tuple_length ($2, pos)}
 | LIST exp_list {List ($2, pos)}
 | HD exp {Hd ($2, pos)}
 | TL exp {Tl ($2, pos)}
 | CONS exp exp {Cons ($2, $3, pos)}
 | BINOP exp binop exp {Binop ($2, $3, $4, pos)}

binop:
   PLUS {Plus}
 | MINUS {Minus}
 | MULT {Mult}
 | DIV {Div}
 | EQUAL {Equal}
 | CONCAT {Concat}
 | LT {Lt}
 | LE {Le}
 | GT {Gt}
 | GE {Ge}
 | NE {Ne}
 | BOOL_AND {And}
 | BOOL_OR {Or}

letrec_list:
   LBRACKET letrec_list_contents RBRACKET {$2}

letrec_list_contents:
   LPAREN IDENT exp RPAREN {[($2, $3)]}
 | letrec_list_contents COMMA LPAREN IDENT exp RPAREN {$1 @ [($4, $5)]}

exp_list:
   LBRACKET exp_list_contents RBRACKET {$2}

exp_list_contents:
   {[]} //Nothing
 | exp {[$1]}
 | exp_list_contents COMMA exp {$1 @ [$3]}

label:
   LPAREN label_contents RPAREN {$2}

label_contents:
   IDENT {Principal ($1)}
 | UNDERSCORE {Bottom}
 | CONCAT {Top}
 | label_contents LOGIC_AND label_contents {And ($1, $3)}
 | label_contents LOGIC_OR label_contents {Or ($1, $3)}

ident_list:
   LBRACKET ident_list_contents RBRACKET {$2}

ident_list_contents:
   {[]} //Nothing
 | IDENT {[$1]}
 | ident_list_contents COMMA IDENT {$1 @ [$3]}

const_val:
   LPAREN const_val_body RPAREN {$2}

const_val_body:
   INT INT_VAL {Int $2}
 | INT MINUS INT_VAL {Int (-$3)}
 | FLOAT FLOAT_VAL {Float $2}
 | FLOAT MINUS FLOAT_VAL {Float (-.$3)}                 
 | BOOL TRUE {Bool true}
 | BOOL FALSE {Bool false}
 | STRING STRING_VAL {String $2}
 | UNIT {Unit}
