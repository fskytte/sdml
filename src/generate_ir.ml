open Ast
open Utility

let ir_of_const_types c =
  match c with
    Int i -> "(Int " ^ (string_of_int i) ^ ")"
  | Float f -> "(Float " ^ (string_of_float f) ^ ")"
  | Bool b -> "(Bool " ^ (string_of_bool b) ^ ")"
  | String s -> "(String \"" ^ (escape_string s) ^ "\")"
  | Unit -> "(Unit)"
  | Label l -> "(Label " ^ (string_of_label l) ^ ")"
  
let generate_ir exp module_names =
  let rec helper exp offset =
    let get_offset s = offset + (String.length s) in
    match exp with
      Const (c, pos) -> let ir = "(Const " ^ (ir_of_const_types c) ^ ")" in
                        let last = get_offset ir in
                        (Const (c, Pos (offset, last)), ir)
    | Check_label (exp, label, receive_label, pos) -> let ir = "(Check_label " in
                                                      let (transformed_exp, ir_exp) = helper exp (get_offset ir) in
                                                      let ir = ir ^ ir_exp ^ " (" ^ (string_of_label label) ^ ")" in
                                                      let ir = ir ^ (string_of_bool receive_label) ^ ")" in
                                                      let last = get_offset ir in
                                                      (Check_label (transformed_exp, label, receive_label, Pos (offset, last)), ir_exp)
    | Function (params, e, pos) -> let ir = "(Function [" ^ (string_of_string_list params) ^ "] " in
                                   let (transformed_e, ir_e) = helper e (get_offset ir) in
                                   let ir = ir ^ ir_e ^ ")" in
                                   let last = get_offset ir in
                                   (Function (params, transformed_e, Pos (offset, last)), ir)
    | Module_access (mod_name, prop_name, pos) -> let ir = "(Module_access " ^ mod_name ^ " " ^ prop_name ^ ")" in
                                                  let last = get_offset ir in
                                                  (Module_access (mod_name, prop_name, Pos (offset, last)), ir)
    | Let (ident, value, body, pos) -> let ir = "(Let " ^ ident ^ " " in
                                       let (transformed_value, ir_value) = helper value (get_offset ir) in
                                       let ir = ir ^ ir_value ^ " " in
                                       let (transformed_body, ir_body) = helper body (get_offset ir) in
                                       let ir = ir ^ ir_body ^ ")" in
                                       let last = get_offset ir in
                                       (Let (ident, transformed_value, transformed_body, Pos(offset, last)), ir)
    | Let_pattern (pattern, value, body, pos) -> raise (Should_not_happen "Let_pattern should have been desugared")
    | Classify (exp, label, pos) -> let ir_label = "(" ^ (string_of_label label) ^ ")" in
                                    let ir = "(Classify " in
                                    let (transformed_exp, ir_exp) = helper exp (get_offset ir) in
                                    let ir = ir ^ ir_exp ^ " " ^ ir_label ^ ")" in
                                    let last = get_offset ir in
                                    (Classify (transformed_exp, label, Pos (offset, last)), ir)
    | Declassify (exp, label, pos) -> let ir_label = "(" ^ (string_of_label label) ^ ")" in
                                      let ir = "(Declassify " in
                                      let (transformed_exp, ir_exp) = helper exp (get_offset ir) in
                                      let ir = ir ^ ir_exp ^ " " ^ ir_label ^ ")" in
                                      let last = get_offset ir in
                                      (Declassify (transformed_exp, label, Pos (offset, last)), ir)
    | Letrec (definitions, body, pos) -> let ir = "(Letrec " in
                                         let (transformed_definitions, ir_definitions) = letrec_list_helper definitions (get_offset ir) in
                                         let ir = ir ^ ir_definitions ^ " " in
                                         let (transformed_body, ir_body) = helper body (get_offset ir) in
                                         let ir = ir ^ ir_body ^ ")" in
                                         let last = get_offset ir in
                                         (Letrec (transformed_definitions, transformed_body, Pos (offset, last)), ir)
    | Seq (e1, e2, pos) -> let ir = "(Seq " in
                           let (transformed_e1, ir_e1) = helper e1 (get_offset ir) in
                           let ir = ir ^ ir_e1 ^ " " in
                           let (transformed_e2, ir_e2) = helper e2 (get_offset ir) in
                           let ir = ir ^ ir_e2 ^ ")" in
                           let last = get_offset ir in
                           (Seq (transformed_e1, transformed_e2, Pos (offset, last)), ir)
    | Var (name, pos) -> let ir = "(Var " ^ name ^ ")" in
                         let last = get_offset ir in
                         (Var (name, Pos (offset, last)), ir)
    | App (func, args, pos) -> let ir = "(App " in
                               let (transformed_func, ir_func) = helper func (get_offset ir) in
                               let ir = ir ^ ir_func ^ " " in
                               let (transformed_args, ir_args) = exp_list_helper args (get_offset ir) in
                               let ir = ir ^ ir_args ^ ")" in
                               let last = get_offset ir in
                               (App (transformed_func, transformed_args, Pos (offset, last)), ir)
    | Send (receiver, msg, modules, pos) -> let ir = "(Send " in
                                            let (transformed_receiver, ir_receiver) = helper receiver (get_offset ir) in
                                            let ir = ir ^ ir_receiver ^ " " in
                                            let (transformed_msg, ir_msg) = helper msg (get_offset ir) in
                                            let ir = ir ^ ir_msg ^ " [" ^ (string_of_string_list modules) ^ "])" in
                                            let last = get_offset ir in
                                            (Send (transformed_receiver, transformed_msg, modules, Pos (offset, last)), ir)
    | Receive pos -> let ir = "(Receive)" in
                     let last = get_offset ir in
                     (Receive (Pos (offset, last)), ir)
    | Guarded_receive (_, _) -> raise (Should_not_happen "Guarded_receive should have been desugared")
    | If (cond, the, els, pos) -> let ir = "(If " in
                                  let (transformed_cond, ir_cond) = helper cond (get_offset ir) in
                                  let ir = ir ^ ir_cond ^ " " in
                                  let (transformed_the, ir_the) = helper the (get_offset ir) in
                                  let ir = ir ^ ir_the ^ " " in
                                  let (transformed_els, ir_els) = helper els (get_offset ir) in
                                  let ir = ir ^ ir_els ^ ")" in
                                  let last = get_offset ir in
                                  (If (transformed_cond, transformed_the, transformed_els, Pos (offset, last)), ir)
    | Tuple (values, pos) -> let ir = "(Tuple [" in
                             let (transformed_values, ir_values) = exp_list_helper values (get_offset ir) in
                             let ir = ir ^ ir_values ^ "])" in
                             let last = get_offset ir in
                             (Tuple (transformed_values, Pos (offset, last)), ir)
    | Tuple_access (tuple, index, pos) -> let ir = "(Tuple_access " in
                                          let (transformed_tuple, ir_tuple) = helper tuple (get_offset ir) in
                                          let ir = ir ^ ir_tuple ^ " " in
                                          let (transformed_index, ir_index) = helper index (get_offset ir) in
                                          let ir = ir ^ ir_index ^ ")" in
                                          let last = get_offset ir in
                                          (Tuple_access (transformed_tuple, transformed_index, Pos (offset, last)), ir)
    | Tuple_length (tuple, pos) -> let ir = "(Tuple_length " in
                                   let (transformed_tuple, ir_tuple) = helper tuple (get_offset ir) in
                                   let ir = ir ^ ir_tuple ^")" in
                                   let last = get_offset ir in
                                   (Tuple_length (transformed_tuple, Pos (offset, last)), ir)
    | List (values, pos) -> let ir = "(List [" in
                            let (transformed_values, ir_values) = exp_list_helper values (get_offset ir) in
                            let ir = ir ^ ir_values ^ "])" in
                            let last = get_offset ir in
                            (List (transformed_values, Pos (offset, last)), ir)
    | Hd (exp, pos) -> let ir = "(Hd " in
                       let (transformed_exp, ir_exp) = helper exp (get_offset ir) in
                       let ir = ir ^ ir_exp ^ ")" in
                       let last = get_offset ir in
                       (Hd (transformed_exp, Pos (offset, last)), ir)
    | Tl (exp, pos) -> let ir = "(Tl " in
                       let (transformed_exp, ir_exp) = helper exp (get_offset ir) in
                       let ir = ir ^ ir_exp ^ ")" in
                       let last = get_offset ir in
                       (Tl (transformed_exp, Pos (offset, last)), ir)
    | Cons (exp, list, pos) -> let ir = "(Cons " in
                               let (transformed_exp, ir_exp) = helper exp (get_offset ir) in
                               let ir = ir ^ ir_exp ^ " " in
                               let (transformed_list, ir_list) = helper list (get_offset ir) in
                               let ir = ir ^ ir_list ^ ")" in
                               let last = get_offset ir in
                               (Cons (transformed_exp, transformed_list, Pos (offset, last)), ir)
    | Pattern_match (_, _, _, _) -> raise (Should_not_happen "Pattern_match should have been desugared")
    | Binop (e1, b, e2, pos) -> let ir = "(Binop " in
                                let (transformed_e1, ir_e1) = helper e1 (get_offset ir) in
                                let ir = ir ^ ir_e1 ^ " " ^ (string_of_binop b) ^ " " in
                                let (transformed_e2, ir_e2) = helper e2 (get_offset ir) in
                                let ir = ir ^ ir_e2 ^ ")" in
                                let last = get_offset ir in
                                (Binop (transformed_e1, b, transformed_e2, Pos (offset, last)), ir)
  and letrec_list_helper l offset =
    let rec intern_helper l offset = 
      match l with
        [] -> []
      | (name, value) :: xs -> let ir = "(" ^ name ^ " " in
                               let (transformed_value, ir_value) = helper value (offset + (String.length ir)) in
                               let ir = ir ^ ir_value ^ ")" in
                               ((name, transformed_value), ir) :: (intern_helper xs (offset + (String.length ir) + 2 (*The extra 2 is for the ', ' that is used to separate the elements*)))
    in
    let ir = "[" in
    let (value_list, ir_list) = List.split (intern_helper l (offset + (String.length ir))) in
    let raw_list = List.fold_right (fun x acc -> x ^ ", " ^ acc) ir_list "" in
    let finished_ir = if String.length raw_list > 2 then ir ^ (String.sub raw_list 0 ((String.length raw_list) - 2)) ^ "]" else  raw_list in
    (value_list, finished_ir)
  and exp_list_helper l offset =
    let rec intern_helper l offset = 
      match l with
        [] -> []
      | x :: xs -> let (transformed_x, ir_x) = helper x offset in
                   (transformed_x, ir_x) :: (intern_helper xs (offset + (String.length ir_x) + 2))
    in
    let ir = "[" in
    let (exp_list, ir_list) = List.split (intern_helper l (offset + (String.length ir))) in
    let raw_list = List.fold_right (fun x acc -> x ^ ", " ^ acc) ir_list "" in
    let finished_ir = ir ^ (if String.length raw_list > 2 then  (String.sub raw_list 0 ((String.length raw_list) - 2)) else raw_list) ^ "]" in
    (exp_list, finished_ir)
  in
  let modules = "[" ^ (string_of_string_list module_names) ^ "] " in
  let (exp, ir) = helper exp (String.length modules) in
  (exp, modules ^ ir, (String.length modules) - 1);;
