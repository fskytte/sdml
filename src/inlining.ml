open Utility
open Var_utils
open Ast
open Constants
       
exception Non_variable_call of string;;
exception Not_a_function;;

let inc_count table func =
  try
    let current = Hashtbl.find table func in
    Hashtbl.remove table func;
    Hashtbl.add table func (current + 1)
  with Not_found -> Hashtbl.add table func 1;;

let get_count table func =
  try
    Hashtbl.find table func
  with Not_found -> 0;;

let count_usages exp =
  let table = Hashtbl.create 10 in
  let inc = inc_count table in
  let rec helper exp =
    match exp with
      Const_i _ -> ()
    | Function_i (_, body, name, _, _, _) -> helper body
    | Module_access_i (mod_name, prop_name) -> helper mod_name
    | Fix_i (decl_list, body, _) -> ignore (List.map (fun (name, value) -> helper value) decl_list);
                                    helper body
    | Var_i name -> inc name
    | Var_decl_i (_, value, body) -> helper value;
                                     helper body
    | App_i (func, args) -> ignore (List.map (fun exp -> helper exp) args);
                            helper func
    | Monitor_app_i (func, args, _) -> ignore (List.map (fun exp -> helper exp) args);
                                       helper func
    | If_i (cond, the, els) -> helper cond;
                               helper the;
                               helper els
    | Tuple_i exps -> ignore (List.map (fun exp -> helper exp) exps)
    | Tuple_access_i (tuple, index) -> helper tuple;
                                       helper index
    | Tuple_length_i tuple -> helper tuple
    | List_i exps -> ignore (List.map (fun exp -> helper exp) exps)
    | Hd_i list -> helper list
    | Tl_i list -> helper list
    | Cons_i (e1, e2) -> helper e1;
                         helper e2
    | Object_create_i pairs -> ignore (List.map (fun (_, exp) -> helper exp) pairs)
    | Object_access_i (obj, _) -> helper obj
    | Object_access_general_i (e1, e2) -> helper e1;
                                          helper e2
    | Binop_i (e1, _, e2) -> helper e1;
                             helper e2
  in
  helper exp;
  table;;
  
let inline_functions exp table =
  let inlined = ref 0 in
  let get = get_count table in
  let rec helper exp =
    match exp with
      Const_i c -> Const_i c
    | Function_i (args, body, name, pos, fix_name, free_vars) -> let new_name = (if is_fix_fun name then fix_fun else "") ^ name in
                                                      Function_i (args, helper body, new_name, pos, fix_name, free_vars)
    | Module_access_i (mod_name, prop_name) -> Module_access_i (helper mod_name, prop_name)
    | Fix_i (decl_list, body, env_name) -> Fix_i (helper_pair_list decl_list, helper body, env_name)
    | Var_i name -> Var_i name
    | Var_decl_i (name, value, body) -> Var_decl_i (name, helper value, helper body)
    | App_i (func, args) -> let inlined_args = helper_list args in
                            (match func with
                               Function_i (formal_args, body, name, pos, closure, free_vars) -> let inlined_body = helper body in
                                                                                                let inlineable = List.fold_right (fun name acc -> if acc then (get name) = 1 else false) formal_args true in
                                                                                                if inlineable then
                                                                                                  (inlined := 1 + !inlined;
                                                                                                   replace_vars inlined_body (List.combine formal_args inlined_args))
                                                                                                else
                                                                                                  App_i (Function_i (formal_args, inlined_body, name, pos, closure, free_vars), inlined_args)
                             | x -> App_i (helper x, inlined_args))
    | Monitor_app_i (func, args, event) -> Monitor_app_i (helper func, helper_list args, map_event helper event)
    | If_i (cond, the, els) -> If_i (helper cond, helper the, helper els)
    | Tuple_i values -> Tuple_i (helper_list values)
    | Tuple_access_i (tuple, index) -> Tuple_access_i (helper tuple, helper index)
    | Tuple_length_i tuple -> Tuple_length_i (helper tuple)
    | List_i values -> List_i (helper_list values)
    | Hd_i exp -> Hd_i (helper exp)
    | Tl_i exp -> Tl_i (helper exp)
    | Cons_i (exp, list) -> Cons_i (helper exp, helper list)
    | Object_create_i pair_list -> Object_create_i (helper_pair_list pair_list)
    | Object_access_i (exp, name) -> Object_access_i (helper exp, name)
    | Object_access_general_i (obj, prop) -> Object_access_general_i (helper obj, helper prop)
    | Binop_i (e1, op, e2) -> Binop_i (helper e1, op, helper e2)
  and helper_list l =
    match l with
      [] -> []
    | x :: xs -> (helper x) :: (helper_list xs)
  and helper_pair_list l =
    match l with
      [] -> []
    | (name, value) :: xs -> (name, helper value) :: (helper_pair_list xs)
  in
  (helper exp, !inlined);;
  
      
let inline_until_done exp rounds table =
  let rec helper exp done_rounds =
    match rounds with
      Some done_rounds -> exp
    | _ -> let (res, inlined) = inline_functions exp table in
           if inlined > 0 then
             helper res (done_rounds + 1)
           else
             res
  in
  helper exp 0;;

let inline_single_use_functions exp rounds =
  let table = count_usages exp in
  let res = inline_until_done exp rounds table in
  res
