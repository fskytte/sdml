open Ast
open Utility
open Var_utils
open Constants
open Curry_ast
       
exception Not_a_function of string;;
exception Not_an_application;;
exception No_args;;

type list_access = Hd_a | Tl_a;;
  

(*http://cs.brown.edu/~sk/Publications/Books/ProgLangs/2007-04-26/plai-2007-04-26.pdf
 Programming Languages: Application and Interpretation, Shriram Krishnamurthi
 chapter 18 *)
(*https://cs.brown.edu/courses/cs173/2012/book/Control_Operations.html
 Programming Languages: Application and Interpretation, Shriram Krishnamurthi
 chapter 14 *)
let rec cps_transform_helper monitored first_for_runtime exp =
  match exp with
    Const (c, pos) -> let k = get_cont_name () in
                      let func = get_func_name () in
                      Function_i ([k],
                                  (if monitored then
                                     Monitor_app_i (Var_i k,
                                                    [Const_i c],
                                                    Const_e)
                                   else
                                     App_i (Var_i k, [Const_i c])),
                                  func,
                                  Some pos,
                                  None,
                                  None)
  | Check_label (exp, label, receive_label, pos) -> cps_transform_helper monitored false (curry_ast (App (Var ((if receive_label then original_runtime_flows_to else runtime_flows_to), pos),
                                                                                           [exp;
                                                                                            Const (Label label, pos)],
                                                                                           pos)))
  | Function (args, body, pos) -> let k = get_cont_name () in
                                  let dyn_k = get_cont_name () in
                                  let func_1 = get_func_name () in
                                  let func_2 = get_func_name () in
                                  let func_3 = get_func_name () in
                                  let ret_val = get_var_name () in
                                  let body =  Function_i (List.append args [dyn_k],
                                                          App_i (cps_transform_helper monitored false body,
                                                                 [Function_i ([ret_val],
                                                                              (if monitored then
                                                                                 Monitor_app_i (Var_i dyn_k,
                                                                                                [Var_i ret_val],
                                                                                                Return_e)
                                                                               else
                                                                                 App_i (Var_i dyn_k, [Var_i ret_val])),
                                                                              func_3,
                                                                              Some pos,
                                                                              None,
                                                                              None)]),
                                                          func_2,
                                                          Some pos,
                                                          None,
                                                          None)
                                  in
                                  if first_for_runtime then
                                    body
                                  else 
                                    Function_i ([k],
                                                (if monitored then
                                                   Monitor_app_i (Var_i k,
                                                                  [body],
                                                                  Const_e)
                                                 else
                                                   App_i (Var_i k, [body])),
                                                func_1,
                                                Some pos,
                                                None,
                                                None)
  | Module_access (mod_name, prop_name, pos) -> let k = get_cont_name () in
                                                let func = get_func_name () in
                                                Function_i ([k],
                                                            (if monitored then
                                                               Monitor_app_i (Var_i k,
                                                                              [Module_access_i (Var_i mod_name, prop_name)],
                                                                              Var_e (Var_i mod_name))
                                                             else 
                                                               App_i (Var_i k,
                                                                      [Module_access_i (Var_i mod_name, prop_name)])),
                                                            func,
                                                            Some pos,
                                                            None,
                                                            None)
  | Let (name, value, body, pos) -> let func_1 = get_func_name () in
                                    let func_2 = get_func_name () in
                                    let func_3 = get_func_name () in
                                    let k = get_cont_name () in
                                    Function_i ([k],
                                                App_i (cps_transform_helper monitored false value,
                                                       [Function_i ([name],
                                                                    App_i (Function_i ([name],
                                                                                       App_i (cps_transform_helper monitored false body,
                                                                                              [Var_i k]),
                                                                                       func_1,
                                                                                       Some pos,
                                                                                       None,
                                                                                       None),
                                                                           [Var_i name]),
                                                                    func_2,
                                                                    Some pos,
                                                                    None,
                                                                    None)]),
                                                func_3,
                                                Some pos,
                                                None,
                                                None)
  | Let_pattern (_, _, _, _) -> raise (Should_not_happen "Let_pattern should have been desugared")
  | Classify (exp, label, pos) -> let k = get_cont_name () in
                                  let res = get_var_name () in
                                  let func_1 = get_func_name () in
                                  let func_2 = get_func_name () in
                                  Function_i ([k],
                                              App_i (cps_transform_helper monitored false exp,
                                                     [Function_i ([res],
                                                                  (if monitored then
                                                                     Monitor_app_i (Var_i k,
                                                                                    [Var_i res],
                                                                                    Classify_e (Var_i res, label))
                                                                   else
                                                                     App_i (Var_i k, [Var_i res])),
                                                                  func_1,
                                                                  Some pos,
                                                                  None,
                                                                  None)]),
                                              func_2,
                                              Some pos,
                                              None,
                                              None)
  | Declassify (exp, label, pos) -> let k = get_cont_name () in
                                    let res = get_var_name () in
                                    let func_1 = get_func_name () in
                                    let func_2 = get_func_name () in
                                    Function_i ([k],
                                                App_i (cps_transform_helper monitored false exp,
                                                       [Function_i ([res],
                                                                    (if monitored then
                                                                       Monitor_app_i (Var_i k,
                                                                                      [Var_i res],
                                                                                      Declassify_e (Var_i res, label))
                                                                     else
                                                                       App_i (Var_i k, [Var_i res])),
                                                                    func_1,
                                                                    Some pos,
                                                                    None,
                                                                    None)]),
                                                func_2,
                                                Some pos,
                                                None,
                                                None)
  | Letrec (decl_list, body, pos) -> let transformed_decl_list = cps_transform_let_rec_list decl_list pos monitored in
                                     let transformed_body = cps_transform_helper monitored false body in
                                     Fix_i (transformed_decl_list,
                                            transformed_body,
                                            None)
  | Seq (e1, e2, pos) -> let k = get_cont_name () in
                         let v = get_var_name () in
                         let func_1 = get_func_name () in
                         let func_2 = get_func_name () in
                         Function_i ([k],
                                     (App_i (cps_transform_helper monitored false e1,
                                             [Function_i ([v],
                                                          App_i (cps_transform_helper monitored false e2,
                                                                 [Var_i k]),
                                                          func_2,
                                                          Some pos,
                                                          None,
                                                          None)])),
                                     func_1,
                                     Some pos,
                                     None,
                                     None)
  | Var (name, pos) -> let k = get_cont_name () in
                       let func = get_func_name () in
                       Function_i ([k],
                                   (if monitored then
                                      Monitor_app_i (Var_i k,
                                                     [Var_i name],
                                                     Var_e (Var_i name))
                                    else
                                      App_i (Var_i k, [Var_i name])),
                                   func,
                                   Some pos,
                                   None,
                                   None)
  | App (func, args, pos) -> let k = get_cont_name () in
                             let f = get_var_name () in
                             let a = get_var_name () in
                             let func_1 = get_func_name () in
                             let func_2 = get_func_name () in
                             let func_3 = get_func_name () in
                             Function_i ([k],
                                         App_i (cps_transform_helper monitored false func,
                                                [Function_i ([f],
                                                             App_i (cps_transform_helper monitored false (List.hd args),
                                                                    [Function_i ([a],
                                                                                 (if monitored then
                                                                                    Monitor_app_i (Var_i schedule_func,
                                                                                                   [Var_i current_thread_id;
                                                                                                    Var_i f;
                                                                                                    Tuple_i [Var_i a; Var_i k; Const_i Unit]],
                                                                                                   Call_e (Var_i f))
                                                                                  else
                                                                                    App_i (Var_i schedule_func,
                                                                                           [Var_i current_thread_id;
                                                                                            Var_i f;
                                                                                            Tuple_i [Var_i a; Var_i k; Const_i Unit]])),
                                                                                 func_1,
                                                                                 Some pos,
                                                                                 None,
                                                                                 None)]),
                                                             func_2,
                                                             Some pos,
                                                             None,
                                                             None)]),
                                         func_3,
                                         Some pos,
                                         None,
                                         None)
  | Send (receiver, msg, modules, pos) -> let k = get_cont_name () in
                                          let receiver_v = get_var_name () in
                                          let msg_v = get_var_name () in
                                          let res = get_var_name () in
                                          let func_1 = get_func_name () in
                                          let func_2 = get_func_name () in
                                          let func_3 = get_func_name () in
                                          let func_4 = get_func_name () in
                                          Function_i ([k],
                                                      App_i (cps_transform_helper monitored false msg,
                                                             [Function_i ([msg_v],
                                                                          App_i (cps_transform_helper monitored false receiver,
                                                                                 [Function_i ([receiver_v],
                                                                                              (if monitored then
                                                                                                 Monitor_app_i (Var_i send_func,
                                                                                                                [Var_i receiver_v;
                                                                                                                 Var_i msg_v;
                                                                                                                 Tuple_i (List.map (fun x -> Const_i (String x)) modules);
                                                                                                                 Function_i ([res],
                                                                                                                             App_i (Var_i k,
                                                                                                                                    [Var_i res]),
                                                                                                                             func_4,
                                                                                                                             Some pos,
                                                                                                                             None,
                                                                                                                             None)],
                                                                                                                Send_e (Var_i receiver_v, Var_i msg_v))
                                                                                               else
                                                                                                 App_i (Var_i send_func,
                                                                                                        [Var_i receiver_v;
                                                                                                         Var_i msg_v;
                                                                                                         Tuple_i (List.map (fun x -> Const_i (String x)) modules);
                                                                                                         Function_i ([res],
                                                                                                                     App_i (Var_i k,
                                                                                                                            [Var_i res]),
                                                                                                                     func_4,
                                                                                                                     Some pos,
                                                                                                                     None,
                                                                                                                     None)])),
                                                                                              func_3,
                                                                                              Some pos,
                                                                                              None,
                                                                                              None)]),
                                                                          func_2,
                                                                          Some pos,
                                                                          None,
                                                                          None)]),
                                                      func_1,
                                                      Some pos,
                                                      None,
                                                      None)
  | Receive pos -> let k = get_cont_name () in
                   let v = get_var_name () in
                   let func_1 = get_func_name () in
                   let func_2 = get_func_name () in
                   Function_i ([k],
                               (App_i (Var_i receive_func,
                                       [Function_i ([v],
                                                    (if monitored then
                                                       Monitor_app_i (Var_i k,
                                                                      [Var_i v],
                                                                      Receive_e)
                                                     else
                                                       App_i (Var_i k, [Var_i v])),
                                                    func_2,
                                                    Some pos,
                                                    None,
                                                    None)])),
                               func_1,
                               Some pos,
                               None,
                               None)
  | Guarded_receive (_, _) -> raise (Should_not_happen "Guarded_receive should have been desugared")
  | If (cond, the, els, pos) -> let k = get_cont_name () in
                                let cond_res = get_var_name () in
                                let func_1 = get_func_name () in
                                let func_2 = get_func_name () in
                                let func_3 = get_func_name () in
                                let func_4 = get_func_name () in
                                let func_5 = get_func_name () in
                                let res = get_var_name () in
                                Function_i ([k],
                                            (App_i (cps_transform_helper monitored false cond,
                                                    [Function_i ([cond_res],
                                                                 (if monitored then
                                                                    Monitor_app_i (Function_i ([unit_arg],
                                                                                               If_i (Var_i cond_res,
                                                                                                     App_i (cps_transform_helper monitored false the, [Function_i ([res],
                                                                                                                                                                   Monitor_app_i (Var_i k,
                                                                                                                                                                                  [Var_i res],
                                                                                                                                                                                  Join_e),
                                                                                                                                                                   func_4,
                                                                                                                                                                   Some pos,
                                                                                                                                                                   None,
                                                                                                                                                                   None)]),
                                                                                                     App_i (cps_transform_helper monitored false els, [Function_i ([res],
                                                                                                                                                                   Monitor_app_i (Var_i k,
                                                                                                                                                                                  [Var_i res],
                                                                                                                                                                                  Join_e),
                                                                                                                                                                   func_5,
                                                                                                                                                                   Some pos,
                                                                                                                                                                   None,
                                                                                                                                                                   None)])),
                                                                                               func_3,
                                                                                               Some pos,
                                                                                               None,
                                                                                               None),
                                                                                   [Const_i Unit],
                                                                                   Branch_e (Var_i cond_res))
                                                                  else
                                                                    If_i (Var_i cond_res,
                                                                          App_i (cps_transform_helper monitored false the, [Var_i k]),
                                                                          App_i (cps_transform_helper monitored false els, [Var_i k]))),
                                                                 func_2,
                                                                 Some pos,
                                                                 None,
                                                                 None)])),
                                            func_1,
                                            Some pos,
                                            None,
                                            None)
  | Tuple (values, pos) -> let k = get_cont_name () in
                           let func = get_func_name () in
                           Function_i ([k],
                                       tuple_from_cps_transform_list (cps_transform_list values monitored) k pos monitored,
                                       func,
                                       Some pos,
                                       None,
                                       None)
  | Tuple_access (tuple, index, pos) -> let k = get_cont_name () in
                                        let func_1 = get_func_name () in
                                        let func_2 = get_func_name () in
                                        let func_3 = get_func_name () in
                                        let tuple_v = get_var_name () in
                                        let index_v = get_var_name () in
                                        Function_i ([k],
                                                    App_i (cps_transform_helper monitored false tuple,
                                                           [Function_i ([tuple_v],
                                                                        App_i (cps_transform_helper monitored false index,
                                                                               [Function_i ([index_v],
                                                                                            (if monitored then
                                                                                               Monitor_app_i (Var_i k,
                                                                                                              [Tuple_access_i (Var_i tuple_v, Var_i index_v)],
                                                                                                              Var_e (Tuple_access_i (Var_i tuple_v, Var_i index_v)))
                                                                                             else
                                                                                               App_i (Var_i k,
                                                                                                      [Tuple_access_i (Var_i tuple_v, Var_i index_v)])),
                                                                                            func_1,
                                                                                            Some pos,
                                                                                            None,
                                                                                            None)]),
                                                                        func_2,
                                                                        Some pos,
                                                                        None,
                                                                        None)]),
                                                    func_3,
                                                    Some pos,
                                                    None,
                                                    None)
  | Tuple_length (tuple, pos) -> let k = get_cont_name () in
                                 let tuple_v = get_var_name () in
                                 let func_1 = get_func_name () in
                                 let func_2 = get_func_name () in
                                 Function_i ([k],
                                             App_i (cps_transform_helper monitored false tuple,
                                                    [Function_i ([tuple_v],
                                                                 (if monitored then
                                                                    Monitor_app_i (Var_i k,
                                                                                   [Tuple_length_i (Var_i tuple_v)],
                                                                                   Var_e (Var_i tuple_v))
                                                                  else
                                                                    App_i (Var_i k,
                                                                           [Tuple_length_i (Var_i tuple_v)])),
                                                                 func_1,
                                                                 Some pos,
                                                                 None,
                                                                 None)]),
                                             func_2,
                                             Some pos,
                                             None,
                                             None)
  | List (values, pos) -> let k = get_cont_name () in
                          let func = get_func_name () in
                          Function_i ([k],
                                      list_from_cps_transform_list (cps_transform_list values monitored) k pos monitored,
                                      func,
                                      Some pos,
                                      None,
                                      None)
  | Hd (exp, pos) -> let k = get_cont_name () in
                     let func_1 = get_func_name () in
                     let func_2 = get_func_name () in
                     let v = get_var_name () in
                     Function_i ([k],
                                 App_i (cps_transform_helper monitored false exp,
                                        [Function_i ([v],
                                                     (if monitored then
                                                        Monitor_app_i (Var_i k,
                                                                       [Hd_i (Var_i v)],
                                                                       Var_e (Hd_i (Var_i v)))
                                                      else
                                                        App_i (Var_i k,
                                                               [Hd_i (Var_i v)])),
                                                     func_1,
                                                     Some pos,
                                                     None,
                                                     None)]),
                                 func_2,
                                 Some pos,
                                 None,
                                 None)
  | Tl (exp, pos) -> let k = get_cont_name () in
                     let func_1 = get_func_name () in
                     let func_2 = get_func_name () in
                     let v = get_var_name () in
                     Function_i ([k],
                                 App_i (cps_transform_helper monitored false exp,
                                        [Function_i ([v],
                                                     (if monitored then
                                                        Monitor_app_i (Var_i k,
                                                                       [Tl_i (Var_i v)],
                                                                       Var_e (Tl_i (Var_i v)))
                                                      else
                                                        App_i (Var_i k,
                                                               [Tl_i (Var_i v)])),
                                                     func_1,
                                                     Some pos,
                                                     None,
                                                     None)]),
                                 func_2,
                                 Some pos,
                                 None,
                                 None)
  | Cons (exp, list, pos) -> let k = get_cont_name () in
                             let exp_v = get_var_name () in
                             let list_v = get_var_name () in
                             let func_1 = get_func_name () in
                             let func_2 = get_func_name () in
                             let func_3 = get_func_name () in
                             Function_i ([k],
                                         App_i (cps_transform_helper monitored false exp,
                                                [Function_i ([exp_v],
                                                             App_i (cps_transform_helper monitored false list,
                                                                    [Function_i ([list_v],
                                                                                 (if monitored then
                                                                                    Monitor_app_i (Var_i k,
                                                                                                   [Cons_i (Var_i exp_v, Var_i list_v)],
                                                                                                   Cons_e (Var_i exp_v, Var_i list_v))
                                                                                  else
                                                                                    App_i (Var_i k,
                                                                                           [Cons_i (Var_i exp_v, Var_i list_v)])),
                                                                                 func_1,
                                                                                 Some pos,
                                                                                 None,
                                                                                 None)]),
                                                             func_2,
                                                             Some pos,
                                                             None,
                                                             None)]),
                                         func_3,
                                         Some pos,
                                         None,
                                         None)
  | Pattern_match (_, _, _, _) -> raise (Should_not_happen "Pattern_match should have been desugared")
  | Binop (e1, b, e2, pos) -> let k = get_cont_name () in
                              let e1_val = get_var_name () in
                              let e2_val = get_var_name () in
                              let func_1 = get_func_name () in
                              let func_2 = get_func_name () in
                              let func_3 = get_func_name () in
                              Function_i ([k],
                                          (App_i (cps_transform_helper monitored false e1,
                                                  [Function_i ([e1_val],
                                                               (App_i (cps_transform_helper monitored false e2,
                                                                       [Function_i ([e2_val],
                                                                                    (if monitored then
                                                                                       Monitor_app_i (Var_i k,
                                                                                                      [Binop_i (Var_i e1_val,
                                                                                                                b,
                                                                                                                Var_i e2_val)],
                                                                                                      Binop_e (Var_i e1_val, (res_type_of_binop b), Var_i e2_val))
                                                                                     else
                                                                                       App_i (Var_i k,
                                                                                              [Binop_i (Var_i e1_val,
                                                                                                        b,
                                                                                                        Var_i e2_val)])),
                                                                                    func_3,
                                                                                    Some pos,
                                                                                    None,
                                                                                    None)])),
                                                               func_2,
                                                               Some pos,
                                                               None,
                                                               None)])),
                                          func_1,
                                          Some pos,
                                          None,
                                          None)
and cps_transform_list l monitored =
  match l with
    [] -> []
  | x :: xs -> (cps_transform_helper monitored false x) :: (cps_transform_list xs monitored)
and cps_transform_let_rec_list l pos monitored =
  match l with
    [] -> []
  | (name, value) :: xs -> (match value with
                              Function (args, func_body, _) ->
                              let rest_of_list = cps_transform_let_rec_list xs pos monitored in
                              let k = get_cont_name () in
                              let func_1 = get_func_name () in
                              let func_2 = get_func_name () in
                              let ret_val = get_var_name () in
                              let body = Function_i (List.append args [k],
                                                     App_i (cps_transform_helper monitored false func_body,
                                                            [Function_i ([ret_val],
                                                                         (if monitored then
                                                                            Monitor_app_i (Var_i k,
                                                                                           [Var_i ret_val],
                                                                                           Return_e)
                                                                          else
                                                                            App_i (Var_i k, [Var_i ret_val])),
                                                                         func_1,
                                                                         Some pos,
                                                                         None,
                                                                         None)]),
                                                     func_2,
                                                     Some pos,
                                                     Some name,
                                                     None) in
                              (name, body) :: rest_of_list
                            | _ -> raise (Not_a_function "Letrecs only support functions"))
and tuple_from_cps_transform_list l k pos monitored =
  let rec helper l vars =
    match l with
      [] -> if monitored then
              Monitor_app_i (Var_i k,
                             [Tuple_i (List.rev vars)],
                             Const_e)
            else
              App_i (Var_i k, [Tuple_i (List.rev vars)])
    | x :: xs -> let v = get_var_name () in
                 let func = get_func_name () in
                 App_i (x,
                        [Function_i ([v],
                                     helper xs (Var_i v :: vars),
                                     func,
                                     Some pos,
                                     None,
                                     None)])
  in
  helper l []
and list_from_cps_transform_list l k pos monitored =
  let rec helper l vars =
    match l with
      [] -> if monitored then
              Monitor_app_i (Var_i k,
                             [List_i (List.rev vars)],
                             List_create_e)
            else
              App_i (Var_i k,
                     [List_i (List.rev vars)])
    | x :: xs -> let v = get_var_name () in
                 let func = get_func_name () in
                 App_i (x,
                        [Function_i ([v],
                                     helper xs (Var_i v :: vars),
                                     func,
                                     Some pos,
                                     None,
                                     None)])
  in
  helper l [];;
  
let cps_transform exp runtime monitored =
  let v = get_var_name () in
  if runtime then
    cps_transform_helper monitored true exp
  else 
    App_i (cps_transform_helper monitored false exp, [Function_i ([v],
                                                                  App_i (Var_i default_continuation,
                                                                         [Var_i v]),
                                                                  get_func_name (),
                                                                  None,
                                                                  None,
                                                                  None)]);;
