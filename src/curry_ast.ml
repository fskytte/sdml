open Ast
open Constants

exception Not_an_application;;
exception Not_a_function of string;;
exception No_args;;
  
let rec curry_ast exp =
  match exp with
    Const (c, pos) -> Const (c, pos)
  | Check_label (exp, label, receive_label, pos) -> Check_label (curry_ast exp, label, receive_label, pos)
  | Function (args, body, pos) -> let rec create_functions f =
                                    match f with
                                      Function (args, body, pos) ->
                                      let curried_body = curry_ast body in
                                      (match args with
                                         [] -> Function ([unit_arg], curried_body, pos)
                                       | [x] -> Function ([x], curried_body, pos)
                                       | x :: xs -> Function ([x], (create_functions (Function (xs, curried_body, pos))), pos))
                                    | _ -> raise (Not_a_function "create_functions called with non function") (* Should never happen *)
                                  in
                                  create_functions (Function (args, body, pos))
  | Module_access (mod_name, prop_name, pos) -> Module_access (mod_name, prop_name, pos)
  | Let (ident, value, body, pos) -> Let (ident, curry_ast value, curry_ast body, pos)
  | Let_pattern (pattern, value, body, pos) -> Let_pattern (pattern, curry_ast value, curry_ast body, pos)
  | Classify (exp, label, pos) -> Classify (curry_ast exp, label, pos)
  | Declassify (exp, label, pos) -> Declassify (curry_ast exp, label, pos)
  | Letrec (decl_list, body, pos) -> Letrec (curry_let_list decl_list, curry_ast body, pos)
  | Seq (e1, e2, pos) -> Seq (curry_ast e1, curry_ast e2, pos)
  | Var (name, pos) -> Var (name, pos)
  | App (func, args, pos) -> let rec create_applications a =
                                  match a with
                                    App (func, args, pos) ->
                                    let curried_func = curry_ast func in
                                    (match args with
                                       [] -> raise No_args (* Should never happen *)
                                     | [x] -> App (curried_func, [curry_ast x], pos)
                                     | x :: xs -> App (create_applications (App (curried_func, xs, pos)), [x], pos))
                                  | _ -> raise Not_an_application (* Should never happen *)
                                in
                                create_applications (App (func, List.rev (curry_list args), pos))
  | Send (receiver, msg, modules, pos) -> Send (curry_ast receiver, curry_ast msg, modules, pos)
  | Receive pos -> Receive pos
  | Guarded_receive (rules, pos) -> Guarded_receive (curry_rules rules, pos)
  | If (cond, the, els, pos) -> If (curry_ast cond, curry_ast the, curry_ast els, pos)
  | Tuple (values, pos) -> Tuple (curry_list values, pos)
  | Tuple_access (tuple, index, pos) -> Tuple_access (curry_ast tuple, curry_ast index, pos)
  | Tuple_length (tuple, pos) -> Tuple_length (curry_ast tuple, pos)
  | List (values, pos) -> List (curry_list values, pos)
  | Hd (exp, pos) -> Hd (curry_ast exp, pos)
  | Tl (exp, pos) -> Tl (curry_ast exp, pos)
  | Cons (exp, list, pos) -> Cons (curry_ast exp, curry_ast list, pos)
  | Pattern_match (exp, rules, default, pos) -> Pattern_match (curry_ast exp, curry_rules rules, (match default with Some e -> Some (curry_ast e) | None -> None), pos)
  | Binop (e1, b, e2, pos) -> Binop (curry_ast e1, b, curry_ast e2, pos)
and curry_let_list l =
  match l with
    [] -> []
  | (name, value) :: xs -> (name, curry_ast value) :: curry_let_list xs
and curry_rules l =
  match l with
    [] -> []
  | (pattern, exp, pos) :: xs -> (pattern, curry_ast exp, pos) :: (curry_rules xs)
and curry_list l =
  match l with
    [] -> []
  | x :: xs -> (curry_ast x) :: (curry_list xs);;
