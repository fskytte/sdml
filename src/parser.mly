%{
  open Ast

  let get_position i =
    let pos = Parsing.rhs_start_pos i in
      let line = pos.pos_lnum in
      let offset = (pos.pos_cnum - pos.pos_bol) in
      Pos (line, offset);;

  (* Inspired by http://courses.softlab.ntua.gr/compilers/2015a/ocamlyacc-tutorial.pdf and
                 http://stackoverflow.com/questions/1933101/ocamlyacc-parse-error-what-token *)
  let parse_error s =
    let pos = get_position 1 in
    let token = (match !Ast.cur_lexbuf with
                   None -> ""
                 | Some lexbuf -> Lexing.lexeme lexbuf) in
    raise (Parse_error (token, pos));;

%}
%token <int> INT
%token <float> FLOAT
%token <string> IDENT
%token <string> STRING
%token RECEIVE
%token ORIGINALLY
%token COMMA
%token DOT
%token LET REC
%token CLASSIFY
%token DECLASSIFY
%token USING
%token END
%token MATCH WITH SEPARATOR
%token INIT
%token AS
%token AND
%token IN
%token FUN
%token IF THEN ELSE
%token TRUE FALSE
%token LPAREN RPAREN
%token UNIT
%token HD TL
%token CONS
%token EMPTY_LIST
%token LLIST RLIST
%token LBRACKET RBRACKET
%token LBRACE RBRACE
%token UNDERSCORE
%token SEMICOLON
%token SEND
%token RIGHTARROW
%token BOOL_AND BOOL_OR
%token LOGIC_AND
%token EQUAL LT LE GT GE NE
%token PLUS MINUS
%token MULT DIV
%token CONCAT
%token EOF

%right RIGHTARROW
%right IN
%right SEMICOLON
%right SEND
%left BOOL_AND BOOL_OR
%left EQUAL LT LE GT GE NE
%left PLUS MINUS
%left MULT DIV
%left CONCAT
%right UMINUS 

%start main
%type <Ast.program> main
%%

main:
   module_decls exp EOF {Program ($1, $2)}

module_decls:
   {[]} //Nothing
 | USING module_list SEMICOLON SEMICOLON {$2}

module_list: 
   module_decl {[$1]}
 | module_list COMMA module_decl {$1 @ [$3]}

module_decl:
   STRING AS IDENT {($1, $3, false)}
 | STRING AS IDENT INIT {($1, $3, true)}

comma_ident_list:
   {[]} //Nothing
 | IDENT {[$1]}
 | comma_ident_list COMMA IDENT {$1 @ [$3]}

const_exp:
   INT {Int $1}
 | FLOAT {Float $1}
 | STRING {String $1}
 | TRUE {Bool true}
 | FALSE {Bool false}
 | UNIT {Unit}
 
exp:
   const_exp {Const ($1, get_position 1)}
 | MINUS exp {Binop (Const (Int 0, get_position 1), Minus, $2, get_position 1)}
 | exp binop exp {Binop ($1, $2, $3, get_position 1)}
 | exp SEND LBRACE comma_ident_list RBRACE exp {Send ($1, $6, $4, get_position 1)} %prec SEND //http://osr507doc.sco.com/en/tools/Yacc_precedence_assigning.html
 | exp SEND exp {Send ($1, $3, [], get_position 1)}
 | RECEIVE {Receive (get_position 1)}
 | RECEIVE WITH pattern_rules {Guarded_receive ($3, get_position 1)}
 | EMPTY_LIST {List ([], get_position 1)}
 | LLIST list_list RLIST {List ($2, get_position 1)}
 | HD exp {Hd ($2, get_position 1)}
 | TL exp {Tl ($2, get_position 1)}
 | pattern_match {$1}
 | exp CONS exp {Cons ($1, $3, get_position 1)}
 | exp SEMICOLON exp {Seq ($1, $3, get_position 1)}
 | FUN UNIT RIGHTARROW exp {Function ([Constants.unit_arg], $4, get_position 1)}
 | FUN ident_list RIGHTARROW exp {Function ($2, $4, get_position 1)}
 | IDENT DOT IDENT {Module_access ($1, $3, get_position 1)}
 | CLASSIFY exp AS label {Classify ($2, $4, get_position 1)}
 | DECLASSIFY exp AS label {Declassify ($2, $4, get_position 1)}
 | LET IDENT EQUAL exp IN exp {Let ($2, $4, $6, get_position 1)}
 | LET pattern EQUAL exp IN exp {Let_pattern ($2, $4, $6, get_position 1)}
 | LET REC let_list IN exp {Letrec ($3, $5, get_position 1)}
 | LPAREN exp RPAREN {$2}
 | IDENT {Var ($1, get_position 1)}
 | LBRACKET exp exp_list RBRACKET {App ($2, $3, get_position 1)}
 | IF exp THEN exp ELSE exp {If ($2, $4, $6, get_position 1)}
 | LPAREN tuplelist RPAREN {Tuple ($2, get_position 1)}
 | exp LBRACE exp RBRACE {Tuple_access ($1, $3, get_position 1)}
 | error {Const (Unit, get_position 1)} // Dummy operation to suppress Parser.Parse_error

label:
   LBRACE label_contents RBRACE {$2}

label_contents:
   IDENT {Principal ($1)}
 | UNDERSCORE {Bottom}
 | CONCAT {Top}
 | label_contents LOGIC_AND label_contents {And ($1, $3)}
 | label_contents SEPARATOR label_contents {Or ($1, $3)}
   
pattern_match:
   MATCH exp WITH pattern_rules {Pattern_match ($2, $4, None, get_position 1)}

pattern_rules:
   pattern RIGHTARROW exp {[($1, $3, get_position 1)]}
 | pattern_rules SEPARATOR pattern RIGHTARROW exp {$1 @ [($3, $5, get_position 1)]}

pattern_tuple_list:
   pattern COMMA pattern {[$1; $3]}
 | pattern_tuple_list COMMA pattern {$1 @ [$3]}

pattern_list_list:
   pattern {Pattern_cons ($1, Pattern_empty_list)}
 | pattern COMMA pattern_list_list {Pattern_cons ($1, $3)}

pattern:
   const_exp {Pattern_const $1}
 | MINUS INT {Pattern_const (Int (- $2))}
 | MINUS FLOAT {Pattern_const (Float (-. $2))}
 | UNDERSCORE {Pattern_wildcard}
 | EMPTY_LIST {Pattern_empty_list}
 | IDENT {Pattern_var $1}
 | pattern CONS pattern {Pattern_cons ($1, $3)}
 | LLIST pattern_list_list RLIST {$2}
 | LPAREN pattern_tuple_list RPAREN {Pattern_tuple $2}
 | pattern label {Pattern_label ($1, $2)}
 | pattern DECLASSIFY AS label {Pattern_declassify ($1, $4)}
 | pattern ORIGINALLY label {Pattern_originally_label ($1, $3)}

let_list:
   IDENT EQUAL exp {[($1, $3)]}
 | let_list AND IDENT EQUAL exp {$1 @ [($3, $5)]}

tuplelist:
   exp COMMA exp {[$1; $3]}
 | tuplelist COMMA exp {$1 @ [$3]}

list_list:
   exp {[$1]}
 | list_list COMMA exp {$1 @ [$3]}

binop:
   PLUS {Plus}
 | MINUS {Minus}
 | MULT {Mult}
 | DIV {Div}
 | EQUAL {Equal}
 | CONCAT {Concat}
 | LT {Lt}
 | LE {Le}
 | GT {Gt}
 | GE {Ge}
 | NE {Ne}
 | BOOL_AND {And}
 | BOOL_OR {Or}

ident_list:
   IDENT {[$1]} 
 | ident_list IDENT {$1 @ [$2]}

exp_list:
   exp {[$1]}
 | exp_list exp {$1 @ [$2]}
