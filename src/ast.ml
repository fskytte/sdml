open Utility
open Map_utils

type position =
    Pos of int * int;;
  
exception Not_a_function of string
exception Parse_error of string * position

let cur_lexbuf : (Lexing.lexbuf option) ref = ref (None);;
    
type modules = (string * string * bool) list;;

type label =
    Principal of string
  | Bottom
  | Top
  | And of label * label
  | Or of label * label;;
                           
type const_types =
    Int of int
  | Float of float
  | Bool of bool
  | String of string
  | Label of label
  | Unit;;

type binop =
    Plus
  | Minus
  | Mult
  | Div
  | Equal
  | Concat
  | Lt
  | Le
  | Gt
  | Ge
  | Ne
  | And
  | Or;;
  
type exp =
    Const of const_types * position
  | Check_label of exp * label * bool * position
  | Function of string list * exp * position
  | Module_access of string * string * position
  | Let of string * exp * exp * position
  | Let_pattern of pattern * exp * exp * position
  | Classify of exp * label * position
  | Declassify of exp * label * position
  | Letrec of (string * exp) list * exp * position
  | Seq of exp * exp * position
  | Var of string * position
  | App of exp * exp list * position
  | Send of exp * exp * string list * position
  | Receive of position
  | Guarded_receive of pattern_rule list * position
  | If of exp * exp * exp * position
  | Tuple of exp list * position
  | Tuple_access of exp * exp * position
  | Tuple_length of exp * position
  | List of exp list * position
  | Hd of exp * position
  | Tl of exp * position
  | Cons of exp * exp * position
  | Pattern_match of exp * (pattern_rule list) * (exp option) * position
  | Binop of exp * binop * exp * position
 and pattern =
    Pattern_const of const_types
  | Pattern_wildcard
  | Pattern_empty_list
  | Pattern_var of string
  | Pattern_cons of pattern * pattern 
  | Pattern_tuple of pattern list
  | Pattern_label of pattern * label
  | Pattern_declassify of pattern * label
  | Pattern_originally_label of pattern * label
 and pattern_rule =
   pattern * exp * position;;

type program = Program of modules * exp;;

type res_type = Number | String | Bool;;

let res_type_of_binop op =
  match op with
    Plus | Minus | Mult | Div -> Number
    | Concat -> String
    | Equal | Lt | Le | Gt | Ge | Ne | And | Or -> Bool;;

let string_of_res_type typ =
  match typ with
    Number -> "Number"
  | String -> "String"
  | Bool -> "Bool";;
  
type exp_intern =
    Const_i of const_types
  | Function_i of string list * exp_intern * string * position option * string option * string list option
  | Module_access_i of exp_intern * string
  | Fix_i of (string * exp_intern) list * exp_intern * (string * string list) option
  | Var_i of string
  | Var_decl_i of string * exp_intern * exp_intern 
  | App_i of exp_intern * exp_intern list
  | Monitor_app_i of exp_intern * exp_intern list * event
  | If_i of exp_intern * exp_intern * exp_intern
  | Tuple_i of exp_intern list
  | Tuple_access_i of exp_intern * exp_intern
  | Tuple_length_i of exp_intern
  | List_i of exp_intern list
  | Hd_i of exp_intern
  | Tl_i of exp_intern
  | Cons_i of exp_intern * exp_intern
  | Object_create_i of (string * exp_intern) list
  | Object_access_i of exp_intern * string
  | Object_access_general_i of exp_intern * exp_intern
  | Binop_i of exp_intern * binop * exp_intern
 and event =
   Const_e
   | List_create_e
   | Var_e of exp_intern
   | Classify_e of exp_intern * label
   | Declassify_e of exp_intern * label
   | Call_e of exp_intern
   | Return_e 
   | Branch_e of exp_intern
   | Join_e
   | Send_e of exp_intern * exp_intern
   | Receive_e 
   | Cons_e of exp_intern * exp_intern
   | Binop_e of exp_intern * res_type * exp_intern;;

let rec string_of_label l =
  match l with
    Principal p -> p
  | Bottom -> "_"
  | Top -> "^"
  | And (l1, l2) -> (string_of_label l1) ^ " & " ^ (string_of_label l2)
  | Or (l1, l2) -> (string_of_label l1) ^ " | " ^ (string_of_label l2);;

let string_of_const_types c =
  match c with
  | Int i -> (string_of_int i)
  | Float f -> (string_of_float f)
  | Bool b -> (string_of_bool b)
  | String s -> s
  | Label l -> (string_of_label l)
  | Unit -> "()";;

let string_of_binop b =
  match b with
    Plus -> "+"
  | Minus -> "-"
  | Mult -> "*"
  | Div -> "/"
  | Equal -> "="
  | Concat -> "^"
  | Lt -> "<"
  | Le -> "<="
  | Gt -> ">"
  | Ge -> ">="
  | Ne -> "<>"
  | And -> "&&"
  | Or -> "||";;

let rec string_of_string_list l =
  match l with
    [] -> ""
  | [x] -> x
  | x :: xs -> x ^ ", " ^ (string_of_string_list xs);;
  
let string_of_pos p =
  match p with
    Pos (l, off) -> "line " ^ (string_of_int l) ^ " offset " ^ (string_of_int off);;

let rec string_of_pattern p =
  match p with
    Pattern_const c -> string_of_const_types c
  | Pattern_wildcard -> "_"
  | Pattern_empty_list -> "[]"
  | Pattern_var v -> v
  | Pattern_cons (hd, tl) -> (string_of_pattern hd) ^ " :: " ^ (string_of_pattern tl)
  | Pattern_tuple l -> "(" ^ (List.fold_right (fun x acc -> (string_of_pattern x) ^ ", " ^ acc) l "") ^ ")"
  | Pattern_label (pattern, label) -> (string_of_pattern pattern) ^ " {" ^ (string_of_label label) ^ "}"
  | Pattern_declassify (pattern, label) -> (string_of_pattern pattern) ^ " declassify as {" ^ (string_of_label label) ^ "}"
  | Pattern_originally_label (pattern, label) -> (string_of_pattern pattern) ^ " originally {" ^ (string_of_label label) ^ "}";;

let string_of_exp e =
  let rec helper e i =
    match e with
      Const (c, _) -> (get_whitespace i) ^"(Const " ^ (string_of_const_types c) ^ ")"
    | Check_label (exp, label, _, _) -> (get_whitespace i) ^ "(Check_label \n" ^ (helper exp (i + 1)) ^ " " ^ (string_of_label label) ^ ")"
    | Function (params, e, _) -> (get_whitespace i) ^ "(Function " ^ (string_of_string_list params) ^ "\n" ^ (helper e (i + 1)) ^ ")"
    | Module_access (mod_name, prop_name, _) -> (get_whitespace i) ^ "(Module_access \n" ^ (get_whitespace (i + 1)) ^ mod_name ^ "." ^ prop_name ^ ")"
    | Let (ident, value, body, _) -> (get_whitespace i) ^ "(Let " ^ ident ^ " = \n" ^ (helper value (i + 1)) ^ " in \n" ^ (helper body (i + 1)) ^ ")"
    | Let_pattern (pattern, value, body, _) -> (get_whitespace i) ^ "(Let " ^ (string_of_pattern pattern) ^ " = \n" ^ (helper value (i + 1)) ^ " in \n" ^ (helper body (i + 1))
    | Classify (exp, label, _) -> (get_whitespace i) ^ "(Classify \n" ^ (helper exp (i + 1)) ^ " as " ^ (string_of_label label) ^ ")"  
    | Declassify (exp, label, _) -> (get_whitespace i) ^ "(Declassify \n" ^ (helper exp (i + 1)) ^ " as " ^ (string_of_label label) ^ ")"  
    | Letrec (definitions, body, _) -> (get_whitespace i) ^ "(Letrec \n" ^ (string_of_let_list definitions i) ^ " in \n" ^ (helper body (i + 1)) ^ ")"
    | Seq (e1, e2, _) -> (get_whitespace i) ^ "(Seq \n" ^ (helper e1 (i + 1)) ^ ";\n" ^ (helper e2 (i + 1)) ^ ")"
    | Var (name, _) -> (get_whitespace i) ^ "(Var " ^ name ^ ")"
    | App (func, args, _) -> (get_whitespace i) ^ "(App \n" ^ (helper func (i + 1)) ^ "\n" ^ (string_of_exp_list args (i + 1)) ^ ")"
    | Send (receiver, msg, modules, _) -> (get_whitespace i) ^ "(Send \n" ^ (helper receiver (i + 1)) ^ "\n" ^ (helper msg (i + 1)) ^ "\n"
                                          ^ (get_whitespace (i + 1)) ^ "Modules: " ^ (string_of_string_list modules) ^ ")"
    | Receive _ -> (get_whitespace i) ^ "(Receive)"
    | Guarded_receive (rules, _) -> (get_whitespace i) ^ "(Guarded_receive \n" ^ (string_of_pattern_rules rules (i + 1)) ^ ")"
    | If (cond, the, els, _) -> (get_whitespace i) ^ "(If \n" ^ (helper cond i) ^ " then \n" ^ (helper the (i + 1)) ^ "\n" ^ (get_whitespace i) ^ " else \n" ^ (helper els (i + 1)) ^ ")"
    | Tuple (values, _) -> (get_whitespace i) ^ "(Tuple \n" ^ (string_of_exp_list values (i + 1)) ^ ")"
    | Tuple_access (tuple, index, _) -> (get_whitespace i) ^ "(Tuple_access \n" ^ (helper tuple (i + 1)) ^ "\n" ^ (helper index (i + 1))^ ")"
    | Tuple_length (tuple, _) -> "(Tuple_length \n" ^ (helper tuple (i + 1)) ^ ")"
    | List (values, _) -> (get_whitespace i) ^ "(List \n" ^ (string_of_exp_list values (i + 1)) ^ ")"
    | Hd (exp, _) -> (get_whitespace i) ^ "(Hd \n" ^ (helper exp (i + 1)) ^ ")"
    | Tl (exp, _) -> (get_whitespace i) ^ "(Tl \n" ^ (helper exp (i + 1)) ^ ")"
    | Cons (exp, list, _) -> (get_whitespace i) ^ "(Cons \n" ^ (helper exp (i + 1)) ^ "::\n" ^ (helper list (i + 1)) ^ ")"
    | Pattern_match (exp, rules, default, _) -> (get_whitespace i) ^ "(Match \n" ^ (helper exp (i + 1)) ^ "\n" ^ (string_of_pattern_rules rules (i + 1))
                                                ^ (match default with Some e -> ("\n" ^ (helper e (i + 1))) | None -> "") ^ ")"
    | Binop (e1, b, e2, _) -> (get_whitespace i) ^ "(Binop \n" ^ (helper e1 (i + 1)) ^ " " ^ (string_of_binop b) ^ "\n" ^ (helper e2 (i + 1)) ^ ")"
  and string_of_exp_list args i =
    match args with
      [] -> ""
    | [x] -> (helper x i)
    | x :: xs -> (helper x i) ^ ",\n" ^ (string_of_exp_list xs i)
  and string_of_let_list l i =
    match l with
      [] -> ""
    | [(name, value)] -> (get_whitespace i) ^ name ^ " = \n" ^ (helper value (i + 1))
    | (name, value) :: xs -> (get_whitespace i) ^ name ^ " = \n" ^ (helper value (i + 1)) ^ " and \n" ^ (string_of_let_list xs i)
  and string_of_pattern_rules l i =
    match l with
      [] -> ""
    | [(pattern, action, _)] -> (string_of_pattern pattern) ^ " -> " ^ (helper action i)
    | (pattern, action, _) :: xs-> (string_of_pattern pattern) ^ " -> " ^ (helper action i) ^ ",\n" ^ (string_of_pattern_rules xs i)
  in
  helper e 0;;

let string_of_exp_intern e =
  let rec helper e i =
    match e with
      Const_i c -> (get_whitespace i) ^"(Const " ^ (string_of_const_types c) ^ ")"
    | Function_i (params, e, _, _, _, _) -> (get_whitespace i) ^ "(Function " ^ (string_of_string_list params) ^ "\n" ^ (helper e (i + 1)) ^ ")"
    | Module_access_i (mod_name, prop_name) -> (get_whitespace i) ^ "(Module_access \n" ^ (helper mod_name (i + 1)) ^ "." ^ prop_name ^ ")"
    | Fix_i (decl_list, body, _) -> (get_whitespace i) ^ "(Fix \n" ^ (string_of_pair_list decl_list (i + 1)) ^ "\n" ^ (helper body (i + 1))
    | Var_i (name) -> (get_whitespace i) ^ "(Var " ^ name ^ ")"
    | Var_decl_i (name, value, body) -> (get_whitespace i) ^ "(Var_decl " ^ name ^ "\n" ^ (helper value (i + 1)) ^ "\n" ^ (helper body (i + 1))
    | App_i (func, args) -> (get_whitespace i) ^ "(App \n" ^ (helper func (i + 1)) ^ "\n" ^ (string_of_exp_list args (i + 1)) ^ ")"
    | Monitor_app_i (func, args, event) -> (get_whitespace i) ^ "(Monitor_app \n" ^ (helper func (i + 1)) ^ "\n" ^ (string_of_exp_list args (i + 1)) ^ "\n" ^ (get_whitespace (i + 1)) ^ "Event: " ^ (string_of_event event) ^ ")"
    | If_i (cond, the, els) -> (get_whitespace i) ^ "(If \n" ^ (helper cond i) ^ " then \n" ^ (helper the (i + 1)) ^ "\n" ^ (get_whitespace i) ^ "else \n" ^ (helper els (i + 1)) ^ ")"
    | Tuple_i values -> (get_whitespace i) ^ "(Tuple \n" ^ (string_of_exp_list values (i + 1)) ^ ")"
    | Tuple_access_i (tuple, index) -> (get_whitespace i) ^ "(Tuple_access \n" ^ (helper tuple (i + 1)) ^ "\n" ^ (helper index (i + 1))^ ")"
    | Tuple_length_i tuple -> (get_whitespace i) ^ "(Tuple_length \n" ^ (helper tuple (i + 1)) ^ ")"
    | List_i values -> (get_whitespace i) ^ "(List \n" ^ (string_of_exp_list values (i + 1)) ^ ")"
    | Hd_i exp -> (get_whitespace i) ^ "(Hd \n" ^ (helper exp (i + 1)) ^ ")"
    | Tl_i exp -> (get_whitespace i) ^ "(Tl \n" ^ (helper exp (i + 1)) ^ ")"
    | Cons_i (exp, list) -> (get_whitespace i) ^ "(Cons \n" ^ (helper exp (i + 1)) ^ "::\n" ^ (helper list (i + 1)) ^ ")"
    | Object_create_i pair_list -> (get_whitespace i) ^ "(Object_create \n" ^ (string_of_pair_list pair_list (i + 1)) ^ ")"
    | Object_access_i (e, name) -> (get_whitespace i) ^ "(Object_access \n" ^ (helper e (i + 1)) ^ "." ^ name ^ ")"
    | Object_access_general_i (obj, prop) -> (get_whitespace i) ^ "(Object_access_general \n" ^ (helper obj (i + 1)) ^ ".\n" ^ (helper prop (i + 1)) ^ ")"
    | Binop_i (e1, b, e2) -> (get_whitespace i) ^ "(Binop \n" ^ (helper e1 (i + 1)) ^ " " ^ (string_of_binop b) ^ "\n" ^ (helper e2 (i + 1)) ^ ")"
  and string_of_exp_list args i =
    match args with
      [] -> ""
    | [x] -> (helper x i)
    | x :: xs -> (helper x i) ^ ",\n" ^ (string_of_exp_list xs i)
  and string_of_pair_list args i =
    match args with
      [] -> ""
    | [(name, value)] -> (get_whitespace i) ^ name ^ " : \n" ^ (helper value i)
    | (name, value) :: xs -> (get_whitespace i) ^ name ^ " : \n" ^ (helper value i) ^ ",\n" ^ (string_of_pair_list xs i)
  and string_of_event event =
    match event with
      Const_e -> "Const"
    | List_create_e -> "List_create"
    | Var_e var -> "Var (" ^ (helper var 0) ^ ")"
    | Classify_e (exp, label) -> "Classify (" ^ (helper exp 0) ^ " as " ^ (string_of_label label) ^ ")"
    | Declassify_e (exp, label) -> "Declassify (" ^ (helper exp 0) ^ " as " ^ (string_of_label label) ^ ")"
    | Call_e var -> "Call (" ^ (helper var 0) ^ ")"
    | Return_e -> "Return"
    | Branch_e var -> "Branch (" ^ (helper var 0) ^ ")"
    | Join_e -> "Join"
    | Send_e (receiver, msg) -> "Send (" ^ (helper receiver 0) ^ ", " ^ (helper msg 0) ^ ")"
    | Receive_e -> "Receive"
    | Cons_e (var_1, var_2) -> "Cons (" ^ (helper var_1 0) ^ ", " ^ (helper var_2 0) ^ ")"
    | Binop_e (var_1, typ, var_2) -> "Binop (" ^ (helper var_1 0) ^ " " ^ (string_of_res_type typ) ^ " " ^ (helper var_2 0) ^ ")"
  in
  helper e 0;;

let rec string_of_exp_intern_list l =
  match l with
    [] -> ""
  | [x] -> (string_of_exp_intern x)
  | x :: xs -> (string_of_exp_intern x) ^ ";\n" ^ (string_of_exp_intern_list xs);;
  
let names_from_function_list l =
  let rec helper l names = 
    match l with
      [] -> names
    | x :: xs -> (match x with
                    Function_i (_, _, name, pos, _, _) -> helper xs (names @ [name])
                  | _ -> raise (Not_a_function "Non function in let rec expressions!"))
  in
  helper l [];;
  
let map_event f event =
  match event with
    Const_e -> Const_e
  | List_create_e -> List_create_e
  | Var_e var -> Var_e (f var)
  | Classify_e (exp, label) -> Classify_e (f exp, label)
  | Declassify_e (exp, label) -> Declassify_e (f exp, label)
  | Call_e func -> Call_e (f func)
  | Return_e -> Return_e
  | Branch_e var -> Branch_e (f var)
  | Join_e -> Join_e
  | Send_e (receiver, msg) -> Send_e (f receiver, f msg)
  | Receive_e -> Receive_e
  | Cons_e (var_1, var_2) -> Cons_e (f var_1, f var_2)
  | Binop_e (var_1, typ, var_2) -> Binop_e (f var_1, typ, f var_2);;
