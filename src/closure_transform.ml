open Ast
open Utility
open Var_utils
open Constants
open Map_utils

exception Non_transformed_operation of string;;
exception Not_a_function of string;;

let find_non_locals exp module_names =
  let rec helper exp non_locals locals =
    match exp with
      Const_i c -> (Const_i c, non_locals)
    | Function_i (args, e, name, pos, fix_name, _) -> let (new_e, e_env) = helper e (name :: (non_locals)) args in
                                                      (Function_i (args, new_e, name, pos, fix_name, Some e_env), e_env)
    | Module_access_i (mod_name, prop_name) -> let (new_mod_name, mod_name_env) = helper mod_name non_locals locals in
                                               (Module_access_i (new_mod_name, prop_name), mod_name_env)
    | Fix_i (decl_list, body, env) -> let (new_decl_list, decl_list_env_list) = List.split (helper_pair_list decl_list non_locals locals) in
                                      let decl_list_env = List.fold_right (fun x acc -> merge acc x) decl_list_env_list [] in
                                      let (new_body, body_env) = helper body non_locals locals in
                                      (Fix_i (new_decl_list, new_body, env), merge decl_list_env body_env)
    | Var_i name -> if (check_if_std name) || List.exists (fun x -> x = name) locals then
                      (Var_i name, non_locals)
                    else
                      (Var_i name, name :: non_locals)
    | Var_decl_i (name, value, body) -> let (new_value, value_env) = helper value non_locals locals in
                                        let (new_body, body_env) = helper body non_locals (name :: locals) in
                                        (Var_decl_i (name, new_value, new_body), merge value_env body_env)
    | App_i (func, args) -> let (new_func, func_env) = helper func non_locals locals in
                            let (new_args, args_env_list) = List.split (helper_list args non_locals locals) in
                            let args_env = List.fold_right (fun x acc -> merge acc x) args_env_list [] in
                            (App_i (new_func, new_args), merge func_env args_env)
    | Monitor_app_i (func, args, event) -> let (new_func, func_env) = helper func non_locals locals in
                                           let (new_args, args_env_list) = List.split (helper_list args non_locals locals) in
                                           let args_env = List.fold_right (fun x acc -> merge acc x) args_env_list [] in
                                           let event_env = ref [] in
                                           let new_event = map_event (fun e -> let (tmp_e, e_env) = (helper e non_locals locals) in
                                                               event_env := merge !event_env e_env;
                                                               tmp_e) event in
                                           (Monitor_app_i (new_func, new_args, new_event), merge (merge func_env args_env) !event_env)
    | If_i (cond, the, els) -> let (new_cond, cond_env) = helper cond non_locals locals in
                               let (new_the, the_env) = helper the non_locals locals in
                               let (new_els, els_env) = helper els non_locals locals in
                               (If_i (new_cond, new_the, new_els), merge (merge cond_env the_env) els_env)
    | Tuple_i values -> let (new_values, values_env_list) = List.split (helper_list values non_locals locals) in
                        let values_env = List.fold_right (fun x acc -> merge acc x) values_env_list [] in
                        (Tuple_i new_values, values_env)
    | Tuple_access_i (tuple, index) -> let (new_tuple, tuple_env) = helper tuple non_locals locals in
                                       let (new_index, index_env) = helper index non_locals locals in
                                       (Tuple_access_i (new_tuple, new_index), merge tuple_env index_env)
    | Tuple_length_i tuple -> let (new_tuple, tuple_env) = helper tuple non_locals locals in
                              (Tuple_length_i new_tuple, tuple_env)
    | List_i values -> let (new_values, values_env_list) = List.split (helper_list values non_locals locals) in
                       let values_env = List.fold_right (fun x acc -> merge acc x) values_env_list [] in
                       (List_i new_values, values_env)
    | Hd_i exp -> let (new_exp, exp_env) = helper exp non_locals locals in
                  (Hd_i new_exp, exp_env)
    | Tl_i exp -> let (new_exp, exp_env) = helper exp non_locals locals in
                  (Tl_i new_exp, exp_env)
    | Object_create_i values -> let (new_values, values_env_list) = List.split (helper_pair_list values non_locals locals) in
                                let values_env = List.fold_right (fun x acc -> merge acc x) values_env_list [] in
                                (Object_create_i new_values, values_env)
    | Object_access_i (exp, name) -> let (new_exp, exp_env) = helper exp non_locals locals in
                                     (Object_access_i (new_exp, name), exp_env)
    | Object_access_general_i (obj, prop) -> let (new_obj, obj_env) = helper obj non_locals locals in
                                             let (new_prop, prop_env) = helper prop non_locals locals in
                                             (Object_access_general_i (new_obj, new_prop), merge obj_env prop_env)
    | Cons_i (exp, list) -> let (new_exp, exp_env) = helper exp non_locals locals in
                            let (new_list, list_env) = helper list non_locals locals in
                            (Cons_i (new_exp, new_list), merge exp_env list_env)
    | Binop_i (e1, op, e2) -> let (new_e1, e1_env) = helper e1 non_locals locals in
                              let (new_e2, e2_env) = helper e2 non_locals locals in
                              (Binop_i (new_e1, op, new_e2), merge e1_env e2_env)
  and helper_list l non_locals locals =
    match l with
      [] -> []
    | x :: xs -> let xs = helper_list xs non_locals locals in
                 let (new_x, x_env) = helper x non_locals locals in
                 (new_x, x_env) :: xs
  and helper_pair_list l non_locals locals =
    match l with
      [] -> []
    | (name, x) :: xs -> let xs = helper_pair_list xs non_locals locals in
                         let (new_x, x_env) = helper x non_locals locals in
                         ((name, new_x), x_env) :: xs
  in
  helper exp [] [];;

let create_closures modules exp =
  let module_names = List.map (fun (_, name, _) -> name) modules in
  let rec helper exp locals ext_env =
    match exp with
      Const_i c -> Const_i c
    | Function_i (args, e, name, pos, fix_name, free_variables) -> let free_variables = match free_variables with
                                                                       Some e -> e
                                                                     | None -> raise (Should_not_happen "Free variables not found!") in
                                                                   (match ext_env with
                                                                      None -> Tuple_i [Function_i (args @ [closure_name], helper e args ext_env, name, pos, fix_name, Some free_variables);
                                                                                       object_from_var_list free_variables locals module_names]
                                                                    | Some (list, obj) -> list := merge (free_variables) !list;
                                                                                          Tuple_i [Function_i (args @ [closure_name], helper e args None, name, pos, fix_name, Some free_variables);
                                                                                                   Var_i obj])
    | Module_access_i (mod_name, prop_name) -> Module_access_i (helper mod_name locals ext_env, prop_name)
    | Fix_i (decl_list, body, _) -> let Some (env_list, env_name) = Some (ref [], get_var_name ()) in
                                    let env = Some (env_list, env_name) in
                                    let transformed_decl_list = List.map (fun (name, x) -> (name, helper x locals env)) decl_list in
                                    let (decl_names, _) = List.split transformed_decl_list in
                                    let transformed_body = helper body locals None in
                                    let final_env_list = replace_or_append (fun (name_1, _) (name_2, _) -> compare name_1 name_2)
                                                                           (pair_list_from_var_list !env_list locals module_names)
                                                                           transformed_decl_list in
                                    Fix_i (final_env_list, transformed_body, Some (env_name, decl_names))
    | Var_i name -> if List.exists (fun x -> x = name) module_names then
                      Object_access_general_i (Var_i global_name, Object_access_i (Var_i closure_name, name))
                    else if List.exists (fun x -> x = name) locals || check_if_std name then
                      Var_i name
                    else
                      Object_access_i (Var_i closure_name, name)
    | Var_decl_i (name, value, body) -> Var_decl_i (name, helper value locals ext_env, helper body (name :: locals) ext_env)
    | App_i (func, args) -> let transformed_func = helper func locals ext_env in
                            (match transformed_func with
                               Var_i name -> App_i (Tuple_access_i (Var_i name, Const_i (Int 0)),
                                                    (List.map (fun x -> helper x locals ext_env) args) @
                                                      [Tuple_access_i (Var_i name, Const_i (Int 1))])
                             | _ -> let func_name = get_var_name () in
                                    Var_decl_i (func_name,
                                                transformed_func,
                                                App_i (Tuple_access_i (Var_i func_name, Const_i (Int 0)),
                                                       (List.map (fun x -> helper x locals ext_env) args) @
                                                         [Tuple_access_i (Var_i func_name, Const_i (Int 1))])))
    | Monitor_app_i (func, args, event) -> let transformed_func = helper func locals ext_env in
                                           (match transformed_func with
                                              Var_i name -> Monitor_app_i (Tuple_access_i (Var_i name, Const_i (Int 0)),
                                                                           (List.map (fun x -> helper x locals ext_env) args) @
                                                                             [Tuple_access_i (Var_i name, Const_i (Int 1))],
                                                                           map_event (fun e -> helper e locals ext_env) event)
                                            | _ -> let func_name = get_var_name () in
                                                   Var_decl_i (func_name,
                                                               transformed_func,
                                                               Monitor_app_i (Tuple_access_i (Var_i func_name, Const_i (Int 0)),
                                                                              (List.map (fun x -> helper x locals ext_env) args) @
                                                                                [Tuple_access_i (Var_i func_name, Const_i (Int 1))],
                                                                              map_event (fun e -> helper e locals ext_env) event)))
    | If_i (cond, the, els) -> If_i (helper cond locals ext_env, helper the locals ext_env, helper els locals ext_env)
    | Tuple_i values -> Tuple_i (List.map (fun x -> helper x locals ext_env) values)
    | Tuple_access_i (tuple, index) -> Tuple_access_i (helper tuple locals ext_env, helper index locals ext_env)
    | Tuple_length_i tuple -> Tuple_length_i (helper tuple locals ext_env)
    | Object_create_i values -> Object_create_i (List.map (fun (name, exp) -> (name, helper exp locals ext_env)) values)
    | Object_access_i (exp, name) -> Object_access_i (helper exp locals ext_env, name)
    | Object_access_general_i (obj, prop) -> Object_access_general_i (helper obj locals ext_env, helper prop locals ext_env)
    | List_i values -> List_i (List.map (fun x -> helper x locals ext_env) values)
    | Hd_i exp -> Hd_i (helper exp locals ext_env)
    | Tl_i exp -> Tl_i (helper exp locals ext_env)
    | Cons_i (exp, list) -> Cons_i (helper exp locals ext_env, helper list locals ext_env)
    | Binop_i (e1, op, e2) -> Binop_i (helper e1 locals ext_env, op, helper e2 locals ext_env)
  and object_from_var_list l locals module_names =
    let rec helper l vars =
      match l with
        [] -> Object_create_i vars
      | x :: xs -> helper xs ((x, if List.exists (fun y -> y = x) locals then
                                    Var_i x
                                  else
                                    Object_access_i (Var_i closure_name, x)) :: vars)
    in helper l []
  and pair_list_from_var_list l locals module_names =
    let rec helper l vars =
      match l with
        [] -> vars
      | x :: xs -> helper xs ((x, if List.exists (fun y -> y = x) locals then
                                    Var_i x
                                  else
                                    Object_access_i (Var_i closure_name, x)) :: vars)
    in helper l []
  in
  let exp_with_closures = find_non_locals exp module_names in
  let (plain_exp, _) = exp_with_closures in
  helper plain_exp [] None
