
open Ast
open Utility
open Constants
open Map_utils

exception Unknown_structure of string

let rec string_of_string_list l =
  match l with
    [] -> ""
  | [x] -> x
  | x :: xs -> x ^ ", " ^ (string_of_string_list xs);;

let generate_code_binop b =
  match b with
    Plus -> "+"
  | Minus -> "-"
  | Mult -> "*"
  | Div -> "/"
  | Equal -> "==="
  | Concat -> "+"
  | Lt -> "<"
  | Le -> "<="
  | Gt -> ">"
  | Ge -> ">="
  | Ne -> "!=="
  | And -> "&&"
  | Or -> "||";;

let rec generate_label label =
    match label with
      Principal p -> runtime_label_name ^ ".get_label('" ^ p ^ "')"
    | Bottom -> runtime_label_name ^ ".bottom"
    | Top -> runtime_label_name ^ ".top"
    | And (l1, l2) -> runtime_label_name ^ ".and(" ^ (generate_label l1) ^ ", " ^ (generate_label l2) ^ ")"
    | Or (l1, l2) -> runtime_label_name ^ ".or(" ^ (generate_label l1) ^ ", " ^ (generate_label l2) ^ ")";;
  
let rec generate_code_helper exp add =
  match exp with
    Const_i c -> add (match c with
                        Int i -> "new Number(" ^ (string_of_int i) ^ ")"
                      | Float f -> "new Number(" ^ (string_of_float f) ^ ")"
                      | Bool b -> "new Boolean(" ^ (string_of_bool b) ^ ")"
                      | String s -> "new String(\"" ^ s ^ "\")"
                      | Label l -> generate_label l
                      | Unit -> unit_val) 
  | Function_i (params, body, name, pos, _, _) -> raise (Should_not_happen "Functions should have been flattened")
  | Module_access_i (mod_name, prop_name) -> add module_access;
                                             add "(";
                                             generate_code_helper mod_name add;
                                             add ".";
                                             add prop_name;
                                             add ", ";
                                             generate_code_helper mod_name add;
                                             add ")"
  | Fix_i (declarations, body, env) -> (match env with
                                          None -> raise (Should_not_happen "No env name for fix")
                                        | Some (env_name, decl_names) -> let rec env_circularize names =
                                                                           match names with
                                                                             [] -> ()
                                                                           | name :: xs -> add env_name;
                                                                                           add ".";
                                                                                           add name;
                                                                                           add "[1] = ";
                                                                                           add env_name;
                                                                                           add ";\n";
                                                                                           (env_circularize xs)
                                                                         in
                                                                         add "var ";
                                                                         add env_name;
                                                                         add " = ";
                                                                         generate_code_helper (Object_create_i declarations) add;
                                                                         add ";\n";
                                                                         env_circularize decl_names;
                                                                         add "\n";
                                                                         add closure_name;
                                                                         add " = _merge_envs(";
                                                                         add closure_name;
                                                                         add ", ";
                                                                         add env_name;
                                                                         add ");\n"; (* basically pollutes env, but should be okay as all names are unique and tests for variable usage etc. has been done before *)
                                                                         generate_code_helper body add)
  | Var_i name -> add name
  | Var_decl_i (name, value, body) -> (match value with
                                         Var_decl_i (name_inner, value_inner, body_inner) ->
                                         (generate_code_helper (Var_decl_i (name_inner, value_inner, Var_decl_i (name, body_inner, body))) add)
                                       | Fix_i (fix_decls, fix_body, fix_env) ->
                                          (generate_code_helper (Fix_i (fix_decls,
                                                                        Var_decl_i (name, fix_body, body),
                                                                        fix_env)) add)
                                       | _ ->
                                          add "var ";
                                          add name;
                                          add " = ";
                                          generate_code_helper value add;
                                          add ";\n";
                                          generate_code_helper body add)
  | App_i (func, args) -> (generate_code_helper func add);
                          add "(";
                          string_of_expression_list args add;
                          add ")\n"
  | Monitor_app_i (func, args, event) -> add monitor_func;
                                         add "(";
                                         generate_code_helper func add;
                                         add ", [";
                                         string_of_expression_list args add;
                                         add "], ";
                                         generate_code_event event add;
                                         add ")\n"
  | If_i (cond, the, els) -> add "if (";
                             generate_code_helper cond add;
                             add ".valueOf()){\n";
                             generate_code_helper the add;
                             add ";\n} else {\n";
                             generate_code_helper els add;
                             add ";\n}"
  | Tuple_i values -> add "[";
                      string_of_expression_list values add;
                      add "]"
  | Tuple_access_i (tuple, index) -> (generate_code_helper tuple add);
                                     add "[";
                                     (generate_code_helper index add);
                                     add "]"
  | Tuple_length_i tuple -> add "new Number(";
                            generate_code_helper tuple add;
                            add ".length)"
  | List_i values -> let rec create_list l =
                       match l with
                         [] -> add empty_list_val
                       | x :: xs -> add "{_type: \"list\",\nhd: ";
                                    (generate_code_helper x add);
                                    add ",\n";
                                    add "tl: ";
                                    (create_list xs);
                                    add "}"
                     in create_list values
  | Hd_i exp -> (generate_code_helper exp add);
                add ".hd"
  | Tl_i exp -> (generate_code_helper exp add);
                add ".tl"
  | Cons_i (exp, list) -> add "{_type: \"list\", \nhd: ";
                          generate_code_helper exp add;
                          add ",\n";
                          add "tl: ";
                          generate_code_helper list add;
                          add "}"
  | Object_create_i props -> add "{";
                             string_of_props_list props add;
                             add "_type: \"env\"}" (* All objects are seen as environments. Change if objects are used in other places *)
  | Object_access_i (obj, prop) -> (generate_code_helper obj add);
                                   add "[\"";
                                   add prop;
                                   add "\"]"
  | Object_access_general_i (obj, prop) -> (generate_code_helper obj add);
                                           add "[";
                                           (generate_code_helper prop add);
                                           add "]"
  | Binop_i (e1, b, e2) -> (* Ensure that JS types are coerced to expected types *)
                           (match b with
                              Plus | Minus | Mult | Div -> add "((+(";
                                                           generate_code_helper e1 add;
                                                           add ").valueOf())";
                                                           add (generate_code_binop b);
                                                           add "(+(";
                                                           generate_code_helper e2 add;
                                                           add ").valueOf()))"
                              | Concat -> add "(\"\"+(";
                                          generate_code_helper e1 add;
                                          add ").valueOf() ";
                                          add (generate_code_binop b);
                                          add " \"\"+(";
                                          generate_code_helper e2 add;
                                          add ").valueOf())"
                              | Equal -> add equal_fun;
                                         add "(";
                                         generate_code_helper e1 add;
                                         add ", ";
                                         generate_code_helper e2 add;
                                         add ", false)"
                              | Ne -> add equal_fun;
                                      add "(";
                                      generate_code_helper e1 add;
                                      add ", ";
                                      generate_code_helper e2 add;
                                      add ", true)"
                              | _ -> add "((";
                                     generate_code_helper e1 add;
                                     add ").valueOf() ";
                                     add (generate_code_binop b);
                                     add "(";
                                     generate_code_helper e2 add;
                                     add ").valueOf())")
and string_of_expression_list l add =
  match l with
    [] -> ()
  | [x] -> generate_code_helper x add
  | x :: xs -> (generate_code_helper x add);
               add ", ";
               (string_of_expression_list xs add)
and string_of_props_list l add =
  match l with
    [] -> ()
  | (name, v) :: xs -> add name;
                       add " : ";
                       generate_code_helper v add;
                       add ", ";
                       string_of_props_list xs add
and generate_code_event event add =
  match event with
    Const_e -> add "{event_type: 'Const'}"
  | List_create_e -> add "{event_type: 'List_create'}"
  | Var_e var -> add "{event_type: 'Var', var: ";
                 generate_code_helper var add;
                 add "}"
  | Classify_e (exp, label) -> add "{event_type: 'Classify', var: ";
                               generate_code_helper exp add;
                               add ", label: ";
                               add (generate_label label);
                               add "}"
  | Declassify_e (exp, label) -> add "{event_type: 'Declassify', var: ";
                                 generate_code_helper exp add;
                                 add ", label: ";
                                 add (generate_label label);
                                 add "}"
  | Call_e func_name -> add "{event_type: 'Call', func: ";
                        generate_code_helper func_name add;
                        add "}"
  | Return_e -> add "{event_type: 'Return'}"
  | Branch_e var -> add "{event_type: 'Branch', var: ";
                    generate_code_helper var add;
                    add "}"
  | Join_e -> add "{event_type: 'Join'}"
  | Send_e (receiver, msg) -> add "{event_type: 'Send', receiver: ";
                              generate_code_helper receiver add;
                              add ", msg: ";
                              generate_code_helper msg add;
                              add "}"
  | Receive_e -> add "{event_type: 'Receive'}"
  | Cons_e (var_1, var_2) -> add "{event_type: 'Cons', var1: ";
                             generate_code_helper var_1 add;
                             add ", var2: ";
                             generate_code_helper var_2 add;
                             add "}"
  | Binop_e (var1, typ, var2) -> add "{event_type: 'Binop', var1: ";
                                 generate_code_helper var1 add;
                                 add ", var2: ";
                                 generate_code_helper var2 add;
                                 add ", res_type: '";
                                 add (string_of_res_type typ);
                                 add "'}";;

let rec generate_functions functions add =
  match functions with
    [] -> ()
  | (Function_i (args, body, name, pos, fix_name, _)) :: xs -> add "function ";
                                                               add name;
                                                               add "(";
                                                               add (string_of_string_list args);
                                                               add "){\n";
                                                               generate_code_helper body add;
                                                               add "}\n";
                                                               (match pos with
                                                                  Some (Pos (beg, en)) -> add name;
                                                                                          add ".position = {begin: ";
                                                                                          add (string_of_int beg);
                                                                                          add ", end: ";
                                                                                          add (string_of_int en);
                                                                                          add "};\n"
                                                                | None -> ());
                                                               (match fix_name with
                                                                  Some f -> add name;
                                                                            add ".fix_name = \"";
                                                                            add f;
                                                                            add "\";\n"
                                                                | None -> ());
                                                               add "\n";
                                                               (generate_functions xs add)
  | _ -> raise (Unknown_structure "Non function in functions list!");;

let load_std_lib add monitored =
  add js_file_loader;
  add "\n\neval(_read_file('";
  add std_lib_location;
  add "'));\n\n";
  if not monitored then
    add "_global_monitored = false;\n\n"
  else
    ();;

let generate_global_cl functions modules runtime add =
  let rec func_helper functions =
    match functions with
      [] -> []
    | Function_i (args, body, name, pos, _, _) :: xs -> (name, Var_i name) :: func_helper xs
    | _ -> raise (Unknown_structure "Non function in functions list!")
  in
  let rec module_helper modules =
    match modules with
      [] -> []
    | (_, alias, _) :: xs -> (alias, Const_i (String alias)) :: module_helper xs
  in
  (if runtime then add "var " else add "global.");
  add closure_name;
  add " = ";
  generate_code_helper (Object_create_i ((func_helper functions) @ (module_helper modules))) add;
  add ";\n\n";;

let rec generate_module_imports modules add =
  match modules with
    [] -> ()
  | (mod_name, name, init) :: xs -> add "global.";
                                    add name;
                                    add " = require (\"";
                                    add mod_name;
                                    add "\")";
                                    if init then add "()" else ();
                                    add ";\n";
                                    generate_module_imports xs add;;

let generate_loaded_module_list modules add =
  let rec helper modules = 
    match modules with
      [] -> ()
    | [(mod_name, alias, _)] -> add "\"";
                                add mod_name;
                                add "\" : \"";
                                add alias;
                                add "\""
    | (mod_name, alias, _) :: xs -> add "\"";
                                    add mod_name;
                                    add "\" : \"";
                                    add alias;
                                    add "\", ";
                                    helper xs
  in
  add "global.";
  add loaded_modules_name;
  add " = {";
  helper modules;
  add "};\n";;

let generate_string_ir string_ir add =
  add "var __string_ir = '";
  add string_ir;
  add "';\n\n";;

let output_ir_module_length ir_module_length add =
  add "var __ir_module_length = ";
  add (string_of_int ir_module_length);
  add ";\n\n";;
  
let generate_code exp functions modules runtime string_ir ir_module_length monitored =
  let buffer = Buffer.create 100 in
  let add_to_buffer = Buffer.add_string buffer in
  (if runtime then
     ()
   else
     (generate_string_ir string_ir add_to_buffer;
      output_ir_module_length ir_module_length add_to_buffer;
      generate_module_imports modules add_to_buffer;
      generate_loaded_module_list modules add_to_buffer;
      load_std_lib add_to_buffer monitored));
  generate_global_cl functions modules runtime add_to_buffer;
  generate_functions functions add_to_buffer;
  generate_code_helper exp add_to_buffer;
  Buffer.contents buffer;;
