open Ast
open Utility
open Constants

exception Undefined_variable of string * Ast.position;;
exception Undefined_module of string * Ast.position;;
exception Set_intern_in_user_program of string;;

(*https://ocaml.org/learn/tutorials/map.html*)
module Vars = Map.Make(String);;

let rec expand_env_list l env =
  match l with
    [] -> env
  | x :: xs -> let new_env = Vars.add x true env in
               expand_env_list xs new_env;;

let expand_env name env =
  Vars.add name true env;;

let create_env v =
  let mt = Vars.empty in
  let rec create_env_helper env v =
    match v with
      [] -> env
    | x :: xs -> create_env_helper (expand_env x env) xs
  in create_env_helper mt v;;

let rec expand_env_from_let_rec_list l env =
  match l with
    [] -> env
  | (name, _) :: xs -> expand_env_from_let_rec_list xs (expand_env name env);;
  
let rec check_variables_helper exp env =
  match exp with
    Const _ -> ()
  | Check_label (exp, _, _, _) -> check_variables_helper exp env
  | Function (args, e, _) -> check_variables_helper e (expand_env_list args env)
  | Module_access (mod_name, prop_name, pos) ->
     begin
       try ignore (Vars.find mod_name env); ()
       with Not_found -> raise (Undefined_module (mod_name, pos))
     end
  | Let (name, e1, e2, _) -> check_variables_helper e1 env; check_variables_helper e2 (expand_env name env)
  | Let_pattern (pattern, value, body, _) -> let ext_env = expand_env_list (get_pattern_env_extension pattern) env in
                                             check_variables_helper value env; check_variables_helper body ext_env
  | Classify (exp, _, _) -> check_variables_helper exp env
  | Declassify (exp, _, _) -> check_variables_helper exp env
  | Letrec (decl_list, e2, _) -> let new_env = expand_env_from_let_rec_list decl_list env in
                                check_let_list decl_list new_env; check_variables_helper e2 new_env
  | Seq (e1, e2, _) -> check_variables_helper e1 env; check_variables_helper e2 env
  | Var (name, pos) ->
     begin
       try ignore (Vars.find name env); () with Not_found -> raise (Undefined_variable (name, pos))
     end
  | App (func, args, _) -> check_variables_helper func env; check_exp_list args env
  | Send (receiver, msg, modules, _) -> check_variables_helper receiver env; check_variables_helper msg env
  | Receive _ -> ()
  | Guarded_receive (rules, _) -> check_rules_list rules env
  | If (cond, the, els, _) -> check_variables_helper cond env; check_variables_helper the env; check_variables_helper els env
  | Tuple (values, pos) -> check_exp_list values env
  | Tuple_access (tuple, index, pos) -> check_variables_helper tuple env; check_variables_helper index env
  | Tuple_length (tuple, pos) -> check_variables_helper tuple env
  | List (values, pos) -> check_exp_list values env
  | Hd (exp, _) -> check_variables_helper exp env
  | Tl (exp, _) -> check_variables_helper exp env
  | Cons (exp, list, _) -> check_variables_helper exp env; check_variables_helper list env
  | Pattern_match (exp, rules, default, _) -> check_variables_helper exp env; check_rules_list rules env; (match default with Some e -> check_variables_helper e env | None -> ())
  | Binop (e1, _, e2, _) -> check_variables_helper e1 env; check_variables_helper e2 env
and check_let_list l env =
  match l with
    [] -> ()
  | (_, value) :: xs -> check_variables_helper value env; check_let_list xs env
and check_rules_list l env =
  match l with
    [] -> ()
  | (pattern, exp, _) :: xs -> let rule_env = expand_env_list (get_pattern_env_extension pattern) env in
                               check_variables_helper exp rule_env; check_rules_list xs env
and get_pattern_env_extension p =
  match p with
    Pattern_const c -> []
  | Pattern_wildcard -> []
  | Pattern_empty_list -> []
  | Pattern_var v -> [v]
  | Pattern_cons (hd, tl) -> (get_pattern_env_extension hd) @ (get_pattern_env_extension tl)
  | Pattern_tuple l -> let rec helper l =
                         match l with
                           [] -> []
                         | x :: xs -> (get_pattern_env_extension x) @ (helper xs)
                       in helper l
  | Pattern_label (pattern, label) -> get_pattern_env_extension pattern
  | Pattern_declassify (pattern, label) -> get_pattern_env_extension pattern
  | Pattern_originally_label (pattern, label) -> get_pattern_env_extension pattern
and check_exp_list l env =
  match l with
    [] -> ()
  | x :: xs -> check_variables_helper x env; check_exp_list xs env;;
  
let check_variables modules exp =
  let module_names = List.map (fun (_, name, _) -> name) modules in
  check_variables_helper exp (create_env (standard_lib @ module_names));;
