type map

val expand_map : string -> map -> map
val expand_map_list : string list -> map -> map
val get_from_map : string -> map -> int
val new_map : map
val merge_maps : map -> map -> map
val map_size : map -> int
val map_iter : (string -> int -> unit) -> map -> unit
