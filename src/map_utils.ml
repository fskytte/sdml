open Utility

module Vars = Map.Make(String);;

type map = int Vars.t

let expand_map name map =
  try ignore (Vars.find name map); map
  with Not_found -> let curr_c = Vars.cardinal map in
                    Vars.add name curr_c map;;

let rec expand_map_list l map =
  match l with
    [] -> map
  | x :: xs -> expand_map_list xs (expand_map x map);;

let get_from_map name map =
  try
    Vars.find name map
  with Not_found -> let msg = ref "" in
                    Vars.iter (fun key _ -> msg := !msg ^ ", " ^ key) map;
                    raise (Should_not_happen ("Key " ^ name ^ " not in map " ^ !msg ^ " of size: " ^ (string_of_int (Vars.cardinal map))));;

let new_map = Vars.empty;;

let merge_maps m1 m2 = Vars.merge (fun key val_a val_b -> match val_a with
                                                       Some a -> Some a
                                                     | None -> val_b) m1 m2;;
let map_size map = Vars.cardinal map;;

let map_iter f map = Vars.iter f map;;
