exception Undefined_variable of string * Ast.position
exception Undefined_module of string * Ast.position
exception Set_intern_in_user_program of string;;
val check_variables : Ast.modules -> Ast.exp -> unit
