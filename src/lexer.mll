{
   open Parser
   exception Unrecognized_token of char * Ast.position
   (*http://courses.softlab.ntua.gr/compilers/2015a/ocamlyacc-tutorial.pdf*)
   let incr_linenum lexbuf =
      let pos = lexbuf.Lexing.lex_curr_p in
      lexbuf.Lexing.lex_curr_p <- { pos with
         Lexing.pos_lnum = pos.Lexing.pos_lnum + 1;
         Lexing.pos_bol = pos.Lexing.pos_cnum;
      };;

   let get_pos lexbuf =
      let pos = lexbuf.Lexing.lex_curr_p in
      Ast.Pos (pos.Lexing.pos_lnum, (pos.Lexing.pos_cnum - pos.Lexing.pos_bol));;
}

(*http://epaperpress.com/lexandyacc/prl.html used for matching syntax*)
rule token = parse
     ['\n']                     {incr_linenum lexbuf; token lexbuf} (* increment the line number count and skip *)
   | [' ' '\t']                 {token lexbuf} (* skip whitespace *)
   | ['0' - '9']+ as lxm        {INT (int_of_string lxm)}
   | ['0' - '9']+'.'['0' - '9']+ as lxm {FLOAT (float_of_string lxm)}
   | "//" [^'\n']*              {token lexbuf} (* Skip comments *)
   | '='                        {EQUAL}
   | '('                        {LPAREN}
   | ')'                        {RPAREN}
   | "()"                       {UNIT}
   | "[]"                       {EMPTY_LIST}
   | '{'                        {LBRACE}
   | '}'                        {RBRACE}
   | '['                        {LBRACKET}
   | ']'                        {RBRACKET}
   | '_'                        {UNDERSCORE}
   | "hd"                       {HD}
   | "tl"                       {TL}
   | "[|"                       {LLIST}
   | "|]"                       {RLIST}
   | "::"                       {CONS}
   | ';'                        {SEMICOLON}
   | '!'                        {SEND} 
   | ','                        {COMMA}
   | '.'                        {DOT}
   | "->"                       {RIGHTARROW}
   | '+'                        {PLUS}
   | '-'                        {MINUS}
   | '*'                        {MULT}
   | '/'                        {DIV}
   | '^'                        {CONCAT}
   | '<'                        {LT}
   | "<="                       {LE}
   | '>'                        {GT}
   | ">="                       {GE}
   | "<>"                       {NE}
   | '&'                        {LOGIC_AND}
   | '|'                        {SEPARATOR}
   | "&&"                       {BOOL_AND}
   | "||"                       {BOOL_OR}
   | "in"                       {IN}
   | "using"                    {USING}
   | "match"                    {MATCH}
   | "end"                      {END}
   | "with"                     {WITH}
   | "|"                        {SEPARATOR}
   | "init"                     {INIT}
   | "as"                       {AS}
   | "classify"                 {CLASSIFY}
   | "declassify"               {DECLASSIFY}
   | "let"                      {LET}
   | "rec"                      {REC}
   | "and"                      {AND}
   | "fun"                      {FUN}
   | "if"                       {IF}
   | "then"                     {THEN}
   | "else"                     {ELSE}
   | "true"                     {TRUE}
   | "false"                    {FALSE}
   | "receive"                  {RECEIVE}
   | "originally"               {ORIGINALLY}
   | ['a' - 'z''A' - 'Z'] ['a' - 'z''A' - 'Z''0' - '9''-''_']* as lxm
                                {IDENT lxm}
   | '\"' [^'"']* '\"' as lxm   {STRING (String.sub lxm 1 ((String.length lxm) - 2))}
   | eof                        {EOF}
   | _ as lxm                   {raise (Unrecognized_token (lxm, get_pos lexbuf))}
