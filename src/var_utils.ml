open Ast
open Constants

let next_name = ref 0;;
  
let get_cont_name () =
  let res = "__k_" ^ (string_of_int !next_name) in
  next_name := !next_name + 1;
  res;;

let get_var_name () =
  let res = "__v_" ^ (string_of_int !next_name) in
  next_name := !next_name + 1;
  res;;

let get_func_name () =
  let res = "__f_" ^ (string_of_int !next_name) in
  next_name := !next_name + 1;
  res;;

let get_env_name () =
  let res = "__env_" ^ (string_of_int !next_name) in
  next_name := !next_name + 1;
  res;;

let get_module_name () =
  "__mod_" ^ (Int64.to_string (Utility.random_number ()));;

let is_fix_fun name =
  ((String.length name) >= (String.length fix_fun)) && (String.sub name 0 (String.length fix_fun)) = fix_fun;;

let replace_vars exp names_values =
  let rec value_for_name search_name names_values =
    match names_values with
      [] -> None
    | (name, value) :: xs -> if name = search_name then
                               Some value
                             else 
                               value_for_name search_name xs
  in
  let rec helper exp = 
    match exp with
      Const_i c -> Const_i c
    | Function_i (args, body, name, pos, closure, free_vars) -> Function_i (args, helper body, name, pos, closure, free_vars)
    | Module_access_i (mod_name, prop_name) -> Module_access_i (helper mod_name, prop_name)
    | Fix_i (decl_list, body, env) -> Fix_i (helper_pair_list decl_list, helper body, env)
    | Var_i name -> (match value_for_name name names_values with
                       Some value -> value
                     | None -> Var_i name)
    | Var_decl_i (name, value, body) -> Var_decl_i (name, helper value, helper body)
    | App_i (func, args) -> App_i (helper func, helper_list args)
    | Monitor_app_i (func, args, event) -> Monitor_app_i (helper func, helper_list args, map_event helper event)
    | If_i (cond, the, els) -> If_i (helper cond, helper the, helper els)
    | Tuple_i values -> Tuple_i (helper_list values)
    | Tuple_access_i (tuple, index) -> Tuple_access_i (helper tuple, helper index)
    | Tuple_length_i tuple -> Tuple_length_i (helper tuple)
    | List_i values -> List_i (helper_list values)
    | Hd_i exp -> Hd_i (helper exp)
    | Tl_i exp -> Tl_i (helper exp)
    | Cons_i (exp, list) -> Cons_i (helper exp, helper list)
    | Object_create_i pair_list -> Object_create_i (helper_pair_list pair_list)
    | Object_access_i (exp, name) -> Object_access_i (helper exp, name)
    | Object_access_general_i (obj, prop) -> Object_access_general_i (helper obj, helper prop)
    | Binop_i (e1, op, e2) -> Binop_i (helper e1, op, helper e2)
  and helper_list l =
    match l with
      [] -> []
    | x :: xs -> (helper x) :: (helper_list xs)
  and helper_pair_list l =
    match l with
      [] -> []
    | (name, value) :: xs -> (name, helper value) :: (helper_pair_list xs)
  in
  helper exp;;
