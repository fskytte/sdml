open Ast
open Utility
open Constants
open Map_utils

exception Non_transformed_operation of string;;

let exp_functions_list_separator l =
  let rec helper l exps functions =
    match l with
      [] -> (exps, functions)
    | (exp, exp_functions) :: xs -> helper xs (exps @ [exp]) (functions @ exp_functions)
  in
  helper l [] [];;

let pair_list_separator l =
  let rec helper l pairs functions =
    match l with
      [] -> (pairs, functions)
    | (name, (exp, exp_functions)) :: xs -> helper xs (pairs @ [(name, exp)]) (functions @ exp_functions)
  in
  helper l [] [];;

let flatten_functions exp =
  let rec helper exp functions =
    match exp with
      Const_i c -> (Const_i c, functions)
    | Function_i (args, body, name, pos, fix_name, free_vars) -> let (transformed_body, body_functions) = helper body functions in
                                                                 (Object_access_i (Var_i closure_name, name), merge [(Function_i (args, transformed_body, name, pos, fix_name, free_vars))] body_functions)
    | Module_access_i (mod_name, prop_name) -> let (transformed_mod_name, mod_name_functions) = helper mod_name functions in
                                               (Module_access_i (transformed_mod_name, prop_name), mod_name_functions)
    | Fix_i (decl_list, body, env_name) -> let decl_list_flattened = flatten_pair_list decl_list functions in
                                           let (transformed_decl_list, decl_list_functions) = List.split (List.map (fun (name, (exp, functions)) -> ((name, exp), functions)) decl_list_flattened) in
                                           let (transformed_body, body_functions) = helper body functions in
                                           (Fix_i (transformed_decl_list, transformed_body, env_name), merge body_functions (List.flatten decl_list_functions))
    | Var_i name -> (Var_i name, functions)
    | Var_decl_i (name, value, body) -> let (transformed_value, value_functions) = helper value functions in
                                        let (transformed_body, body_functions) = helper body functions in
                                        (Var_decl_i (name, transformed_value, transformed_body), merge value_functions body_functions)
    | App_i (func, args) -> let (transformed_func, func_functions) = helper func functions in
                            let transformed_args_list = flatten_list args functions in
                            let (transformed_args, args_functions) = exp_functions_list_separator transformed_args_list in
                            (App_i (transformed_func, transformed_args), merge func_functions args_functions)
    | Monitor_app_i (func, args, event) -> let (transformed_func, func_functions) = helper func functions in
                                           let transformed_args_list = flatten_list args functions in
                                           let (transformed_args, args_functions) = exp_functions_list_separator transformed_args_list in
                                           (Monitor_app_i (transformed_func, transformed_args, event), merge func_functions args_functions)
    | If_i (cond, the, els) -> let (transformed_cond, cond_functions) = helper cond functions in
                               let (transformed_the, the_functions) = helper the functions in
                               let (transformed_els, els_functions) = helper els functions in
                               (If_i (transformed_cond,
                                      transformed_the,
                                      transformed_els),
                                merge cond_functions (merge the_functions els_functions))
    | Tuple_i values -> let transformed_tuple_list = flatten_list values functions in
                        let (transformed_tuple, tuple_functions) = exp_functions_list_separator transformed_tuple_list in
                        (Tuple_i transformed_tuple, tuple_functions)
    | Tuple_access_i (tuple, index) -> let (transformed_tuple, tuple_functions) = helper tuple functions in
                                       let (transformed_index, index_functions) = helper index functions in
                                       (Tuple_access_i (transformed_tuple, transformed_index), merge tuple_functions index_functions)
    | Tuple_length_i tuple -> let (transformed_tuple, tuple_functions) = helper tuple functions in
                              (Tuple_length_i transformed_tuple, tuple_functions)
    | List_i values -> let transformed_list_list = flatten_list values functions in
                       let (transformed_list, list_functions) = exp_functions_list_separator transformed_list_list in
                       (List_i transformed_list, list_functions)
    | Hd_i exp -> let (transformed_exp, exp_functions) = helper exp functions in
                  (Hd_i transformed_exp, exp_functions)
    | Tl_i exp -> let (transformed_exp, exp_functions) = helper exp functions in
                  (Tl_i transformed_exp, exp_functions)
    | Cons_i (exp, list) -> let (transformed_exp, exp_functions) = helper exp functions in
                            let (transformed_list, list_functions) = helper list functions in
                            (Cons_i (transformed_exp, transformed_list), merge exp_functions list_functions)
    | Object_create_i pair_list -> let flattened_pair_list = flatten_pair_list pair_list functions in
                                   let (transformed_pair_list, pair_functions) = pair_list_separator flattened_pair_list in
                                   (Object_create_i transformed_pair_list, pair_functions)
    | Object_access_i (e, name) -> let (transformed_e, e_functions) = helper e functions in
                                   (Object_access_i (transformed_e, name), e_functions)
    | Object_access_general_i (obj, prop) -> let (transformed_obj, obj_functions) = helper obj functions in
                                             let (transformed_prop, prop_functions) = helper prop functions in
                                             (Object_access_general_i (transformed_obj, transformed_prop), merge obj_functions prop_functions)
    | Binop_i (e1, b, e2) -> let (transformed_e1, e1_functions) = helper e1 functions in
                             let (transformed_e2, e2_functions) = helper e2 functions in
                             (Binop_i (transformed_e1,
                                       b,
                                       transformed_e2),
                              merge e1_functions e2_functions)
  and flatten_list l functions =
    match l with
      [] -> []
    | x :: xs -> (helper x functions) :: (flatten_list xs functions)
  and flatten_pair_list l functions : (string * (exp_intern * exp_intern list)) list =
    match l with
      [] -> []
    | (name, value) :: xs -> (name, helper value functions) :: (flatten_pair_list xs functions)
  in
  helper exp [];;
