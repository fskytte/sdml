open Ast
open Utility

let do_constant_folding exp =
  let folded = ref 0 in
  let rec helper exp = 
    match exp with
      Const (c, pos) -> Const (c, pos)
    | Check_label (exp, label, receive_label, pos) -> Check_label (helper exp, label, receive_label, pos)
    | Function (args, body, pos) -> Function (args, helper body, pos)
    | Module_access (mod_name, prop_name, pos) -> Module_access (mod_name, prop_name, pos)
    | Let (name, value, body, pos) -> Let (name, helper value, helper body, pos)
    | Let_pattern (pattern, value, body, pos) -> Let_pattern (pattern, helper value, helper body, pos)
    | Classify (exp, label, pos) -> Classify (helper exp, label, pos)
    | Declassify (exp, label, pos) -> Declassify (helper exp, label, pos)
    | Letrec (decl_list, body, pos) -> Letrec (List.map (fun (name, value) -> (name, helper value)) decl_list, helper body, pos)
    | Seq (e1, e2, pos) -> Seq (helper e1, helper e2, pos)
    | Var (name, pos) -> Var (name, pos)
    | App (func, args, pos) -> App (helper func, List.map helper args, pos)
    | Send (receiver, message, modules, pos) -> Send (helper receiver, helper message, modules, pos)
    | Receive pos -> Receive pos
    | Guarded_receive (rules, pos) -> Guarded_receive (List.map (fun (pattern, exp, pos) -> (pattern, helper exp, pos)) rules, pos)
    | If (cond, the, els, pos) -> If (helper cond, helper the, helper els, pos)
    | Tuple (values, pos) -> Tuple (List.map helper values, pos)
    | Tuple_access (tuple, index, pos) -> Tuple_access (helper tuple, helper index, pos)
    | Tuple_length (tuple, pos) -> Tuple_length (helper tuple, pos)
    | List (values, pos) -> List (List.map helper values, pos)
    | Hd (list, pos) -> Hd (helper list, pos)
    | Tl (list, pos) -> Tl (helper list, pos)
    | Cons (exp, list, pos) -> Cons (helper exp, helper list, pos)
    | Pattern_match (exp, rules, default, pos) -> Pattern_match (helper exp, List.map (fun (pattern, exp, pos) -> (pattern, helper exp, pos)) rules, (match default with
                                                                                                                                                        Some e -> Some (helper e)
                                                                                                                                                      | None -> None), pos)
    | Binop (e1, op, e2, pos) ->
       let handle_int_op n1 op n2 =
         match op with
           Plus -> n1 + n2
         | Minus -> n1 - n2
         | Mult -> n1 * n2
         | Div -> n1 / n2
         | _ -> raise (Should_not_happen "Int op not int op")
       in
       let handle_float_op n1 op n2 =
         match op with
           Plus -> n1 +. n2
         | Minus -> n1 -. n2
         | Mult -> n1 *. n2
         | Div -> n1 /. n2
         | _ -> raise (Should_not_happen "Float op not float op")
       in
       let handle_bool_op b1 op b2 =
         match op with
           And -> b1 && b2
         | Or -> b1 || b2
         | _ -> raise (Should_not_happen "Bool op not bool op")
       in
       match op with 
         Plus | Minus | Mult | Div -> 
                                (match (e1, e2) with
                                   (Const (Int i1, pos1), Const (Int i2, pos2)) -> folded := !folded + 1;
                                                                                   Const (Int (handle_int_op i1 op i2), pos1)
                                 | (Const (Int n1, pos1), Const (Float n2, pos2)) -> folded := !folded + 1;
                                                                                     Const (Float (handle_float_op (float_of_int n1) op n2), pos1)
                                 | (Const (Float n1, pos1), Const (Int n2, pos2)) -> folded := !folded + 1;
                                                                                     Const (Float (handle_float_op n1 op (float_of_int n2)), pos1)
                                 | (Const (Float n1, pos1), Const (Float n2, pos2)) -> folded := !folded + 1;
                                                                                       Const (Float (handle_float_op n1 op n2), pos1)
                                 | _ -> Binop (helper e1, op, helper e2, pos))
         | Concat -> (match (e1, e2) with
                        (Const (String s1, pos1), Const (String s2, pos2)) -> folded := !folded + 1;
                                                                              Const (String (s1 ^ s2), pos1)
                      | _ -> Binop (helper e1, op, helper e2, pos))
         | And  -> (match (e1, e2) with
                      (Const (Bool b1, pos1), Const (Bool b2, pos2)) -> folded := !folded + 1;
                                                                        Const (Bool (handle_bool_op b1 op b2), pos1)
                    | (_, Const (Bool false, pos)) | (Const (Bool false, pos), _) -> folded := !folded + 1;
                                                                                     Const (Bool false, pos)
                    | (e, Const (Bool true, _)) | (Const (Bool true, _), e) -> folded := !folded + 1;
                                                                               helper e
                    | _ -> Binop (helper e1, op, helper e2, pos))
         | Or -> (match (e1, e2) with
                    (Const (Bool b1, pos1), Const (Bool b2, pos2)) -> folded := !folded + 1;
                                                                      Const (Bool (handle_bool_op b1 op b2), pos1)
                  | (_, Const (Bool true, pos)) | (Const (Bool true, pos), _) -> folded := !folded + 1;
                                                                                 Const (Bool true, pos)
                  | (e, Const (Bool false, _)) | (Const (Bool false, _), e) -> folded := !folded + 1;
                                                                               helper e
                  | _ -> Binop (helper e1, op, helper e2, pos))
         | _ -> Binop (helper e1, op, helper e2, pos)
  in
  (helper exp, !folded);;

let rec constant_folding exp =
  let (folded_exp, folded_count) = do_constant_folding exp in
  if folded_count = 0 then
    folded_exp
  else
    constant_folding folded_exp;;
