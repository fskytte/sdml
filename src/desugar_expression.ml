open Ast
open Var_utils
open Constants
open Utility

exception Not_a_function of string;;
       
let rec desugar exp =
  match exp with
    Const (c, pos) -> Const (c, pos)
  | Check_label (exp, label, receive_label, position) -> Check_label (exp, label, receive_label, position)
  | Function (args, body, pos) -> Function (args, desugar body, pos)
  | Module_access (mod_name, prop_name, pos) -> Module_access (mod_name, prop_name, pos)
  | Let (name, value, body, pos) -> Let (name, desugar value, desugar body, pos)
  | Let_pattern (pattern, value, body, pos) -> transform_rules [(pattern, body, pos)] value None pos
  | Classify (exp, label, pos) -> Classify (desugar exp, label, pos)
  | Declassify (exp, label, pos) -> Declassify (desugar exp, label, pos)
  | Letrec (decl_list, body, pos) -> let transformed_decl_list = desugar_let_rec_list decl_list in
                                     let transformed_body = desugar body in
                                     Letrec (transformed_decl_list, transformed_body, pos)
  | Seq (e1, e2, pos) -> Seq (desugar e1, desugar e2, pos)
  | Var (name, pos) -> Var (name, pos)
  | App (func, args, pos) -> App (desugar func, List.map (fun x -> desugar x) args, pos)
  | Send (receiver, msg, modules, pos) -> Send (desugar receiver, desugar msg, modules, pos)
  | Receive pos -> Receive pos
  | Guarded_receive (rules, pos) -> let rec add_send_delayed_to_actions l =
                                      match l with
                                        [] -> []
                                      | (p, exp, pos) :: xs -> (p,
                                                                Seq (App (Var (guarded_receive_success, pos),
                                                                          [Const (Unit, pos)],
                                                                          pos),
                                                                     exp,
                                                                     pos),
                                                                pos) :: (add_send_delayed_to_actions xs)
                                    in
                                    let rec_func = get_func_name () in
                                    let received = get_var_name () in
                                    let new_rules = add_send_delayed_to_actions rules in
                                    desugar (Letrec ([(rec_func, Function ([received],
                                                                           Pattern_match (Var (received, pos),
                                                                                          new_rules,
                                                                                          Some (Seq (App (Var (guarded_receive_failed, pos),
                                                                                                          [Var (received, pos)],
                                                                                                          pos),
                                                                                                     App (Var (rec_func, pos),
                                                                                                          [Receive pos],
                                                                                                          pos),
                                                                                                     pos)),
                                                                                          pos),
                                                                           pos))],
                                                     App (Var (rec_func, pos), [Receive pos], pos),
                                                     pos))
  | If (cond, the, els, pos) -> If (desugar cond, desugar the, desugar els, pos)
  | Tuple (values, pos) -> Tuple (List.map (fun x -> desugar x) values, pos)
  | Tuple_access (tuple, index, pos) -> Tuple_access (desugar tuple, desugar index, pos)
  | Tuple_length (tuple, pos) -> Tuple_length (desugar tuple, pos)
  | List (values, pos) -> List (List.map (fun x -> desugar x) values, pos)
  | Hd (list, pos) -> Hd (desugar list, pos)
  | Tl (list, pos) -> Tl (desugar list, pos)
  | Cons (exp, list, pos) -> Cons (desugar exp, desugar list, pos)
  | Pattern_match (exp, rules, default, pos) -> let exp_v = get_var_name () in
                                                Let (exp_v,
                                                     exp, 
                                                     transform_rules rules (Var (exp_v, pos)) default pos,
                                                     pos)
  | Binop (e1, b, e2, pos) -> Binop (desugar e1, b, desugar e2, pos)
and transform_rules l value default pos =
  let rec bind_var_helper p value exp =
    match p with
      Pattern_var v -> Let (v,
                            value,
                            exp,
                            pos)
    | Pattern_cons (hd, tl) -> bind_var_helper hd (Hd (value, pos)) (bind_var_helper tl (Tl (value, pos)) exp)
    | Pattern_tuple l -> let rec tuple_helper l index =
                           match l with
                             [] -> exp
                           | x :: xs -> bind_var_helper x (Tuple_access (value, (Const (Int index, pos)), pos)) (tuple_helper xs (index + 1))
                         in tuple_helper l 0
    | Pattern_label (pattern, _) -> bind_var_helper pattern value exp
    | Pattern_declassify (pattern, _) -> bind_var_helper pattern value exp
    | Pattern_originally_label (pattern, _) -> bind_var_helper pattern value exp
    | _ -> exp
  in
  let rec helper l = 
    match l with
      [] -> (match default with
               Some e -> e
             | None -> App (Var (raise_error, pos),
                            [Const (String ("Match failure at: " ^ (string_of_pos pos)), pos)], pos))
    | (pattern, exp, pos) :: xs -> If (transform_pattern pattern value pos,
                                       bind_var_helper pattern value exp,
                                       helper xs,
                                       pos)
  in desugar (helper l)
and transform_pattern p value pos =
  match p with
    Pattern_const c -> Binop (Const (c, pos), Equal, value, pos)
  | Pattern_wildcard -> Const (Bool true, pos)
  | Pattern_empty_list -> Binop (List ([], pos), Equal, value, pos)
  | Pattern_var v -> Const (Bool true, pos)
  | Pattern_cons (hd, tl) -> Binop (transform_pattern hd (Hd (value, pos)) pos,
                                    And,
                                    transform_pattern tl (Tl (value, pos)) pos,
                                    pos)
  | Pattern_tuple l -> let rec tuple_helper l index =
                         match l with
                           [x] -> transform_pattern x (Tuple_access (value, Const (Int index, pos), pos)) pos
                         | x :: xs -> Binop (transform_pattern x (Tuple_access (value, Const (Int index, pos), pos)) pos,
                                             And, tuple_helper xs (index + 1), pos)
                         | _ -> raise (Should_not_happen "Tuple was either created empty or size has decreased by more than one")
                       in
                       Binop (Binop (Const (Int (List.length l), pos),
                                     Equal,
                                     Tuple_length (value, pos),
                                     pos),
                              And,
                              tuple_helper l 0,
                              pos)
  | Pattern_label (pattern, label) -> let transformed_pattern = transform_pattern pattern value pos in
                                      Binop (transformed_pattern, And, Check_label (value, label, false, pos), pos)
  | Pattern_declassify (pattern, label) -> let transformed_pattern = transform_pattern pattern value pos in
                                           Declassify (transformed_pattern, label, pos)
  | Pattern_originally_label (pattern, label) -> let transformed_pattern = transform_pattern pattern value pos in
                                               Binop (transformed_pattern, And, Check_label (value, label, true, pos), pos)
and desugar_let_rec_list l =
  match l with
    [] -> []
  | (name, value) :: xs -> (match value with
                              Function (args, func_body, pos) -> (name, Function (args, desugar func_body, pos)) :: (desugar_let_rec_list xs)
                            | _ -> raise (Not_a_function "Letrecs only support functions"));;
