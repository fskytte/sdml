{
   open Ir_parser
   exception Unrecognized_token of char * Ast.position

   (*http://courses.softlab.ntua.gr/compilers/2015a/ocamlyacc-tutorial.pdf*)

   let get_pos lexbuf =
      let pos = lexbuf.Lexing.lex_curr_p in
      Ast.Pos (pos.Lexing.pos_lnum, (pos.Lexing.pos_cnum - pos.Lexing.pos_bol));;
}

(*http://epaperpress.com/lexandyacc/prl.html used for matching syntax*)
rule token = parse
     [' ' '\t']                 {token lexbuf} (*skip whitespace*)
   | ['0' - '9']+ as lxm         {INT_VAL (int_of_string lxm)}
   | ['0' - '9']+'.'['0' - '9']+ as lxm {FLOAT_VAL (float_of_string lxm)}
   | '('                        {LPAREN}
   | ')'                        {RPAREN}
   | '['                        {LBRACKET}
   | ']'                        {RBRACKET}
   | ','                        {COMMA}
   | '+'                        {PLUS}
   | '-'                        {MINUS}
   | '*'                        {MULT}
   | '/'                        {DIV}
   | '='                        {EQUAL}
   | '^'                        {CONCAT}
   | '_'                        {UNDERSCORE}
   | '<'                        {LT}
   | "<="                       {LE}
   | '>'                        {GT}
   | ">="                       {GE}
   | "<>"                       {NE}
   | '&'                        {LOGIC_AND}
   | '|'                        {LOGIC_OR}
   | "&&"                       {BOOL_AND}
   | "||"                       {BOOL_OR}
   | "Int"                      {INT}
   | "Float"                    {FLOAT}
   | "Bool"                     {BOOL}
   | "String"                   {STRING}
   | "Unit"                     {UNIT}
   | "Const"                    {CONST}
   | "Function"                 {FUNCTION}
   | "Module_access"            {MODULE_ACCESS}
   | "Let"                      {LET}
   | "Check_label"              {CHECK_LABEL}
   | "Classify"                 {CLASSIFY}
   | "Declassify"               {DECLASSIFY}
   | "Letrec"                   {LETREC}
   | "Seq"                      {SEQ}
   | "Var"                      {VAR}
   | "App"                      {APP}
   | "Send"                     {SEND}
   | "Send_sync"                {SEND_SYNC}
   | "Receive"                  {RECEIVE}
   | "If"                       {IF}
   | "Tuple"                    {TUPLE}
   | "Tuple_access"             {TUPLE_ACCESS}
   | "Tuple_length"             {TUPLE_LENGTH}
   | "List"                     {LIST}
   | "Hd"                       {HD}
   | "Tl"                       {TL}
   | "Cons"                     {CONS}
   | "Binop"                    {BINOP}
   | "true"                     {TRUE}
   | "false"                    {FALSE}
   | ['_']* ['a' - 'z''A' - 'Z'] ['a' - 'z''A' - 'Z''0' - '9''-''_']* as lxm
                                {IDENT lxm}
   | '\"' [^'"']* '\"' as lxm   {STRING_VAL (String.sub lxm 1 ((String.length lxm) - 2))}
   | _ as lxm                   {raise (Unrecognized_token (lxm, get_pos lexbuf))}
