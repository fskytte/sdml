open Parser
open Ir_parser
open Ast
open Constants
open Variable_checker
open Code_generator
open Cps_transform
open Curry_ast
open Closure_transform
open Flattening
open Inlining
open Var_utils
open Desugar_expression
open Generate_ir
open Constant_folding

exception Unknown_cmd_arg of string;;

let print_if_active s active =
  if active then print_endline s else ();;

let compile lexbuf debug runtime monitored print_progress inline_rounds =
  let Program (modules, ast) = if runtime then
                                 Ir_parser.main Ir_lexer.token lexbuf
                               else 
                                 Parser.main Lexer.token lexbuf in
  print_if_active "Parsed" print_progress;
  if runtime then () else check_variables modules ast;
  print_if_active "Variables checked" print_progress;
  let desugared_ast = if runtime then ast else desugar ast in
  print_if_active "Desugared" print_progress;
  let folded_ast = if runtime then desugared_ast else constant_folding desugared_ast in
  print_if_active "Constant folding performed" print_progress;
  let curried_ast = if runtime then ast else curry_ast folded_ast in
  print_if_active "Curried" print_progress;
  let (positioned_ast, ir, ir_module_length) = if runtime then (ast, "", 0) else generate_ir curried_ast (List.map (fun (_, name, _) -> name) modules) in
  print_if_active "IR generated" print_progress;
  let cps_ast = cps_transform positioned_ast runtime monitored in
  print_if_active "CPS transformed" print_progress;
  let inlined_ast = inline_single_use_functions cps_ast inline_rounds in
  print_if_active "Inlining performed" print_progress;
  let closure_ast = create_closures modules inlined_ast in
  print_if_active "Closures created" print_progress;
  let (flattened_ast, functions) = flatten_functions closure_ast in
  print_if_active "Functions flattened" print_progress;
  let code = generate_code flattened_ast functions modules runtime ir ir_module_length monitored in
  print_if_active "Code generated" print_progress;
  if debug then
    begin
      print_endline ("------ AST ----\n" ^ (string_of_exp ast) ^ "\n\n");
      print_endline ("------ AST (Desugared) ----\n" ^ (string_of_exp desugared_ast) ^ "\n\n");
      print_endline ("------ AST (Constant folded) ----\n" ^ (string_of_exp folded_ast) ^ "\n\n");
      print_endline ("------ AST (Curried) ----\n" ^ (string_of_exp curried_ast) ^ "\n\n");
      print_endline ("------ AST (CPS) ----\n" ^ (string_of_exp_intern cps_ast) ^ "\n\n");
      print_endline ("------ AST (inlined) ----\n" ^ (string_of_exp_intern inlined_ast) ^ "\n\n");
      print_endline ("------ AST (Closure) ----\n" ^ (string_of_exp_intern closure_ast) ^ "\n\n");
      print_endline ("------ AST (Flattened) ----\n" ^ (string_of_exp_intern flattened_ast) ^ "\n\n");
      print_endline ("------ CODE ----\n" ^ code)
    end
  else ();
  code;;
    
let handle_cmd_args l =
  let set_func func value state =
    let (in_name, out_name, debug, runtime, monitored, print_progress, inline_rounds) = state in
    match func with
      "-debug" -> if value = "true" then (in_name, out_name, true, runtime, monitored, print_progress, inline_rounds) else (in_name, out_name, false, runtime, monitored, print_progress, inline_rounds)
    | "-o" -> (in_name, value, debug, runtime, monitored, print_progress, inline_rounds)
    | "-inline" -> (in_name, out_name, debug, runtime, monitored, print_progress, Some (int_of_string value))
    | "-runtime" -> if value = "true" then (in_name, out_name, debug, true, monitored, print_progress, inline_rounds) else (in_name, out_name, debug, false, monitored, print_progress, inline_rounds)
    | "-monitored" -> if value = "false" then (in_name, out_name, debug, runtime, false, print_progress, inline_rounds) else (in_name, out_name, debug, runtime, true, print_progress, inline_rounds)
    | "-print-progress" -> if value = "true" then (in_name, out_name, debug, runtime, monitored, true, inline_rounds) else (in_name, out_name, debug, runtime, monitored, false, inline_rounds)
    | _ -> raise (Unknown_cmd_arg func)
  in
  let rec helper l state =
    let (in_name, out_name, debug, runtime, monitored, print_progress, inline_rounds) = state in
    match l with
      [] -> state
    | func :: value :: xs -> helper xs (set_func func value state)
    | new_in_name :: xs -> helper xs (new_in_name, out_name, debug, runtime, monitored, print_progress, inline_rounds)
  in
  helper l ("", default_out_name, false, false, true, false, None);;

let write_to_file code filename =
  let output_channel = open_out filename in
  Printf.fprintf output_channel "%s\n" code;
  close_out output_channel;;
  
(* Array and argv ops: https://ocaml.org/learn/tutorials/command-line_arguments.html*)
(* File handling: https://ocaml.org/learn/tutorials/file_manipulation.html*)
let () =
  try
    let args_without_name = Array.sub Sys.argv 1 ((Array.length Sys.argv) - 1) in
    let (in_name, out_name, debug, runtime, monitored, print_progress, inline_rounds) = handle_cmd_args (Array.to_list args_without_name) in
    if in_name = "" then
      let lexbuf = Lexing.from_channel stdin in
      Ast.cur_lexbuf := Some lexbuf;
      let code = compile lexbuf debug runtime monitored print_progress inline_rounds in
      print_endline code
    else
      let input_channel = open_in in_name in
      let lexbuf = Lexing.from_channel input_channel in
      Ast.cur_lexbuf := Some lexbuf;
      let code = compile lexbuf debug runtime monitored print_progress inline_rounds in
      close_in input_channel;
      write_to_file code out_name
  with Unknown_cmd_arg arg -> print_endline (arg ^ " is not a legal argument")
     | Lexer.Unrecognized_token (token, Pos (l, o)) -> print_endline ("Unknown character: " ^ (String.make 1 token) ^ " at line " ^ (string_of_int l) ^ " position " ^ (string_of_int 0))
     | Undefined_variable (name, Pos (l, o)) -> print_endline ("Variable " ^ name ^ " at line " ^ (string_of_int l) ^ " position " ^ (string_of_int o) ^ " has not been defined!")
     | Undefined_module (name, Pos (l, o)) -> print_endline ("Module " ^ name ^ " at line " ^ (string_of_int l ) ^ " position " ^ (string_of_int 0) ^ " has not been declared!")
     | Parse_error (token, Pos (l, o)) -> print_endline ("Parse error at line " ^ (string_of_int l) ^ " position " ^ (string_of_int 0) ^ ": " ^ token);;
