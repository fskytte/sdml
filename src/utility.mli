exception Should_not_happen of string
val get_whitespace : int -> string
val escape_string : string -> string
val string_of_string_list : string list -> string
val list_contains : 'a list -> 'a -> bool
val replace_or_append : ('a -> 'a -> int) -> 'a list -> 'a list -> 'a list
val merge : 'a list -> 'a list -> 'a list
val random_number : unit -> Int64.t
val ensure_unique : ('a -> 'a -> int) -> 'a list -> 'a list
val check_if_std : string -> bool 
val read_file : string -> string
