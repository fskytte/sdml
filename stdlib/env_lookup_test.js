var _http = require("http");


var _fs = require("fs");
//https://nodejs.org/api/fs.html#fs_fs_readfilesync_file_options
function _read_file(filename){
    return _fs.readFileSync(filename, {encoding: "utf8"});
}
eval(_read_file("cycle.js"));

if(process.argv.length <= 3){
    console.log("usage: " + process.argv[0] + " [hostname] [port] [id]");
}

var hostname = process.argv[2];
var port = process.argv[3];
var id = process.argv[4];

function _restore_message(message){
    if(message.isFunction){
        return eval("(" + message.body + ")");
    } else if(message instanceof Array) {
        var new_message = [];
        for(var i = 0; i < message.length; i++){
            (function(i){
                new_message[i] = _restore_message(message[i]);
            })(i);
        }
        return new_message;
    } else if(message instanceof Object) {
        var new_message = {};
        for(var i in message){
            new_message[i] = _restore_message(message[i]);
        }
        return new_message;
    } else {
        return message;
    }
}

var request = _http.request({hostname: hostname,
                             port : port,
                             path: "/lookup/" + id,
                             method : "GET"},
                            function(res){
                                var rawResult = "";
                                res.on("data", function(chunk){
                                    rawResult += chunk;
                                });

                                res.on("end", function(){
                                    var result = JSON.parse(JSON.retrocycle(rawResult));
                                    console.log(result);
                                });
                            });
request.on("error", function(e){
    console.log("ERROR! " + e);
});
request.end();
