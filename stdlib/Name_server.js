//Uses express 4x api http://expressjs.com/en/4x/api.html
var express = require("express");
var bodyParser = require("body-parser");
var app = express();
app.use(bodyParser.json());

var names = [];

//https://nodejs.org/docs/latest/api/process.html
if(process.argv.length < 3){
    console.log("Please give the port to run the name server. Usage: node Name_server.js [port]");
    process.exit(0);
}
var portNumber = process.argv[2];

//Uses http://expressjs.com/en/guide/routing.html
app.get("/lookup/:name", function (req, res){
    var result;
    var lookup = names[req.params.name];
    if(lookup){
        console.log("Reporting thread id for: " + req.params.name);
        result = {success: true, result: lookup};
    } else {
        console.log("Thread id for " + req.params.name + " is not found");
        result = {success: false, result: undefined};
    }
    res.send(result);
});

app.post("/register/:name", function (req, res){
    var request = req.body;
    if(!request.hasOwnProperty("location") || !request.hasOwnProperty("thread_id")){
        console.log("Request for " + req.params.name + " is incorrect");
        res.send({success: false});
    } else {
        names[req.params.name] = request;
        console.log(req.params.name + " is registered as");
        console.log(request);
        res.send({success: true});
    }
});

app.listen(portNumber, function(){
    console.log("Name server running on port " + portNumber);
});
