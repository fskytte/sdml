//https://nodejs.org/api/http.html
var _http = require("http");
//https://github.com/dominictarr/my-local-ip
var _ip = require("my-local-ip")();
var _app = require("express")();
//https://nodejs.org/api/child_process.html
var _exec = require("child_process").execSync;
var _bodyParser = require("body-parser");
_app.use(_bodyParser.json());
var _port = 3001; //Default port

//https://nodejs.org/docs/v0.6.18/api/crypto.html#crypto_crypto_createhash_algorithm
var _crypto = require("crypto");
var _labels = require("dc-labels");

var _fs = require("fs");
//https://nodejs.org/api/fs.html#fs_fs_readfilesync_file_options
function _read_file(filename){
    return _fs.readFileSync(filename, {encoding: "utf8"});
}

var _readline = require("readline");
//https://nodejs.org/api/readline.html
var _std_in = _readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

//Loading cycle https://github.com/douglascrockford/JSON-js/blob/master/cycle.js
eval(_read_file("stdlib/cycle.js"));

var _compiler_name = "main.native";

var UNIT = {type: "unit", valueOf: function(){return false}};
var EMPTY_LIST = {type: "empty_list", valueOf: function(){return false}};
var DUMMY_K = [function(){}, {}];

var _max_thread_id = 1;
var _message_queues = [];
var _receive_queue = [];

var _registered_objects = [];
var _cached_objects = [];
var _remote_module_names = [];

var _pc_stacks = [];
var _global_monitored = true;

var _trust_config = {this_node: "local", other_nodes: [], authority: ["local"]};


if(process.argv.length >= 3){
    _port = process.argv[2];
}

if(process.argv.length >= 4){
    _trust_config = JSON.parse(_read_file(process.argv[3]));
}

if(_global_monitored){
    for(var prop in _trust_config.other_nodes){
        _trust_config.other_nodes[prop] = _labels.get_label(_trust_config.other_nodes[prop]);
    }
}

var _privileges = null;
if(_global_monitored){
    if(_trust_config.authority.length > 0){
        _privileges = _labels.bottom;
        for(var i = 0; i < _trust_config.authority.length; i++){
            _privileges = _labels.and(_privileges, _labels.get_label(_trust_config.authority[i]));
        }
    }
}
   
var _running_threads = 1;
var _current_thread_id = {_type: "thread_id", location: {ip: _ip, port: _port}, thread_id: 1, node_key: _trust_config.this_node, label: _labels.get_label(_trust_config.this_node), message_label: _labels.top, monitored: true};
var _threads = [];
_threads[_current_thread_id.thread_id] = _current_thread_id;
_pc_stacks[_current_thread_id.thread_id] = [_labels.bottom];

function _create_module_indirects(modules){
    var new_mod_map = {};
    for(var prop in modules){
        new_mod_map[modules[prop]] = __mods[prop];
    }
    return new_mod_map;
}

//Receiving messages from network
_app.post("/message", function(req, res){
    var request = req.body;
    if(!request.hasOwnProperty("recipient") || !request.hasOwnProperty("message") || !request.hasOwnProperty("modules")){
        res.send({success: false});
    } else {
        var module_res = _check_modules(request.modules);
        if(module_res.res){
            var module_env = _create_module_indirects(request.modules);
            _remote_module_names[request.node_key] = _merge_envs(_remote_module_names[request.node_key], module_env);
            var message = _restore_message(JSON.retrocycle(request.message));
            res.send({success: true});
            var recipient_thread = _threads[request.recipient];
            _send_message_local(undefined, recipient_thread, message, request.node_key, DUMMY_K);
        } else {
            res.send({success: false, error: "missing_modules", params: module_res.missing_modules});
        }
    }
});

function _check_modules(remote_modules){
    var res = {res: true, missing_modules: []};
    for(var p in remote_modules){
        if(__mods[p] === undefined){
            res.res = false;
            res.missing_modules.push(p);
        }
    }
    return res;
}

_app.get("/lookup/:id", function(req, res){
    var id = req.params.id;
    var candidate = _registered_objects[id];
    if(candidate !== undefined){
        res.send({success: true, result: candidate});
    } else {
        res.send({success: false});
    }
});

_app.listen(_port);

function _wrap_and_assign_label_primitive(v, label){
    if(!_global_monitored){
        return v;
    }
    var res = v;
    switch(typeof v){
    case 'number':
        res = new Number(v);
        break;
    case 'boolean':
        res = new Boolean(v);
        break;
    case 'string':
        res = new String(v);
        break;
    }
    res.label = label;
    return res;
}

function _handle_non_cps_callback(args, level, label_wrapper){
    var curr_thread = _current_thread_id;
    if(args.length === level){
        if(_global_monitored){
            _push_to_pc_stack(_current_thread_id, label_wrapper.label);
        }
        return [function(){},{}];
    } else {
        return [function(arg){
            _update_current_thread(curr_thread);
            if(arg !== undefined && arg[0] instanceof Function){
                if(_global_monitored){
                    _push_to_pc_stack(_current_thread_id, label_wrapper.label);
                }
                arg[0](_wrap_and_assign_label_primitive(args[level], label_wrapper.label), _handle_non_cps_callback(args, level + 1, label_wrapper), arg[1]);
            }
        }, {}];
    }
}

function _transform_cps_arg(arg, label_wrapper){
    var curr_thread = _current_thread_id;
    if(_is_closure(arg)){
        return function (){
            _update_current_thread(curr_thread);
            var args = Array.from(arguments);
            if(args.length === 0){
                args.push(_copy_value_with_label(UNIT, label_wrapper.label));
                args.push([function(){},{}]);
            } else if(args.length === 1){
                args[0] = _wrap_and_assign_label_primitive(args[0], label_wrapper.label);
                args.push([function(){},{}]);
            } else {
                var new_args = [_wrap_and_assign_label_primitive(args[0], label_wrapper.label)];
                new_args.push(_handle_non_cps_callback(args, 1, label_wrapper));
                args = new_args;
            }
            args.push(arg[1]);
            arg[0].apply(undefined, args);
        }
    } else {
        if(arg) {
            return arg.valueOf();
        } else {
            return arg;
        }
    }
}

function _module_access(obj, module){
    if(obj instanceof Function){
        //Inspired by: http://stackoverflow.com/a/6772648
        return obj.bind(module);
    } else {
        return obj;
    }
}

function _execute_non_cps_call(func, args, arg_labels, label_wrapper){
    arg_labels.push(func.label);
    if(_global_monitored){
        var new_label = _upper_label(arg_labels, _get_pc());
        label_wrapper.label = new_label;
    }
    var retval = func.apply(null, args);
    if(retval === undefined){
        return UNIT;
    }
    return _wrap_and_assign_label_primitive(retval, new_label);
}

function _handle_non_cps_call(orig_func, old_args, args_done, arg_labels, label_wrapper){
    var curr_thread = _current_thread_id;
    return [function(arg, func, __cl){
        _update_current_thread(curr_thread);
        old_args.push(_transform_cps_arg(arg, label_wrapper));
        arg_labels.push(arg.label);
        args_done++;
        if(args_done === orig_func.length){
            var res = _execute_non_cps_call(orig_func, old_args, arg_labels, label_wrapper);
            if(_global_monitored){
                for(var i = 0; i < arg_labels.length; i++){
                    _pop_from_pc_stack(_current_thread_id);
                }
            }
            func[0](res, func[1]);
        } else {
            var next_func = _handle_non_cps_call(orig_func, old_args, args_done, arg_labels, label_wrapper);
            func[0](next_func, func[1]);
        }
    }, {}];
}

function _call_non_cps_function(func, args){
    var label_wrapper = {label: args[0].label};
    var new_args = [_transform_cps_arg(args[0], label_wrapper)];
    var num_args = func.length;
    
    if(num_args <= 1){
        var res = _execute_non_cps_call(func, new_args, [args[0].label], label_wrapper);
        if(_global_monitored){
            _pop_from_pc_stack(_current_thread_id);
        }
        args[1][0](res, args[1][1]);
    } else {
        args[1][0](_handle_non_cps_call(func, new_args, 1, [args[0].label], label_wrapper),
                   args[1][1]);
    }
} 

function _event_to_string(event){
    switch(event.event_type){
    case "Const":
        return "Const";
    case "Var":
        return "Var";
    case "Call":
        return "Call";
    case "Return":
        return "Return";
    case "Branch":
        return "Branch: " + event.var;
    case "Join":
        return "Join";
    case "Binop":
        return "Binop: (" + event.var1 + ", " + event.var2 + ")";
    }
}

function _pop_from_pc_stack(thread){
    var stack = _pc_stacks[thread.thread_id];
    stack.pop();
}

function _push_to_pc_stack(thread, label){
    var stack = _pc_stacks[thread.thread_id];
    stack.push(label);
}

function _pc_stack_to_string(thread){
    var stack = _pc_stacks[thread.thread_id];
    if(stack === undefined || stack.length === 0){
        return "";
    }
    var res = "";
    for(var i = 0; i < stack.length; i++){
        res += _labels.to_string(stack[i]) + ", ";
    }
    return res;
}

function _upper_label(labels, pc){
    var res = pc;
    for(var i = 0; i < labels.length; i++){
        res = _labels.join(res, labels[i]);
    }
    return res;
}

function _upper_label_list(list, pc){
    if(list.type === EMPTY_LIST.type){
        return list.label;
    }
    var res = pc;
    var curr = list;
    while(curr.tl.type !== EMPTY_LIST.type){
        res = _labels.join(res, curr.hd.label);
        curr = curr.tl;
    }
    return res;
}

function _labels_of_object(object){
    var labels = [];
    for(var p in object){
        if(p === "label"){
            //Assigning labels to label properties is silly
            continue;
        }
        if(object[p] === undefined){
            continue;//Skipping undefined properties
        }
        if(object[p].label === undefined){
            object[p].label = _labels.bottom;
        }
        if(object[p].label !== undefined){
            labels.push(object[p].label);
        }
    }
    return labels;
}

function _trust_of_key(key){
    if(key.valueOf() === _trust_config.this_node){
        //we implicitly trust ourselves
        return _labels.top;
    }
    var looked_up = _trust_config.other_nodes[key];
    if(looked_up === undefined){
        return _labels.bottom;
    } else {
        return looked_up;
    }
}

function _labels_of_array(arr){
    var labels = [];
    for(var i = 0; i < arr.length; i++){
        labels.push(arr[i].label);
    }
    return labels;
}

function _assign_access_label(obj){
    if(_is_closure(obj)){
        if(obj.label === undefined){
            obj.label = _labels.bottom;
        }
        obj.label = _upper_label(_labels_of_object(obj).concat(_labels_of_object(obj[1])), _get_pc());
        if(obj.call_label === undefined){
            obj.call_label = _get_pc();
        }
    } else if(obj instanceof Array){
        obj.label = _upper_label(_labels_of_array(obj), _get_pc());
    } else {
        obj.label = _get_pc();
    }
}

function _declassify_label(label, obj, module, callback, index){
    if(obj instanceof Array){
        if(index === undefined){
            index = 0;
            obj = obj.slice();
        }
        if(index === obj.length){
            obj.label = label;
            callback(obj);
        } else {
            _labels.flows_to(obj[index].label, label, null, function(err, res){
                if(err){
                    console.log("Flows to error:");
                    console.log(err);
                } else {
                    if(!res){
                        _declassify_label(label, obj[index], module, function(inner_res){
                            obj[index] = inner_res;
                            _declassify_label(label, obj, module, callback, index + 1);
                        });
                    } else {
                        _declassify_label(label, obj, module, callback, index + 1);
                    }
                }
            });
        }
    } else {
        var tmp = _reassign_label(_labels.join(label, _get_pc()), obj, module);
        callback(tmp);
    }
}

function _reassign_label(label, obj, module){
    if(!_global_monitored){
        return obj;
    }
    var res;
    if(obj instanceof Number || typeof obj === "number"){
        res = new Number(obj.valueOf());
        _object_assign_no_array(res, obj);
    } else if(obj instanceof String || typeof obj === "string"){
        res = new String(obj.valueOf());
        _object_assign_no_array(res, obj);
    } else if(obj instanceof Boolean || typeof obj === "boolean"){
        res = new Boolean(obj.valueOf());
        _object_assign_no_array(res, obj);
    } else if(_is_closure(obj)){
        res = obj.slice();
        _object_assign_no_array(res, obj);
        _assign_access_label(res);
        res[0].label = label;
        res[1].label = label;
    } else if(obj instanceof Array){
        res = obj.slice();
        _object_assign_no_array(res, obj);
        _assign_access_label(res);
    } else if(obj._type === "list"){
        res = _copy_list_with_label(obj, label);
    } else if(obj instanceof Function){
        res = obj.bind(module);
    } else if(obj instanceof Object){
        res = Object.assign({}, obj);
    }
    res.label = label;
    return res;
}

function _copy_value(v){
    var res;
    if(v instanceof Number){
        res = new Number(v.valueOf());
        _object_assign_no_array(res, v);
    } else if(v instanceof String){
        res = new String(v.valueOf());
        _object_assign_no_array(res, v);
    } else if(v instanceof Boolean){
        res = new Boolean(v.valueOf());
        _object_assign_no_array(res, vj);
    } else if(_is_closure(v)){
        res = v.slice();
        _assign_access_label(res);
    } else if (v.type === "list") {
        res = _copy_list(v);
        _assign_access_label(res);
    } else if(v instanceof Array){
        res = v.slice();
        _assign_access_label(res);
    } else if(v instanceof Object){
        res = Object.assign({}, v);
    }
    res.label = v.label;
    return res;
}

function _copy_list(list){
    var copy_head = _object_assign_no_array({}, list);
    copy_head.type = "list";
    copy_head.hd = _copy_value(list.hd);
    var curr = list;
    var copy = copy_head;
    while(curr.tl.type !== EMPTY_LIST.type){
        copy.tl = _object_assign_no_array({}, curr.tl);
        copy.tl._type = "list";
        copy.tl.hd = _copy_value(curr.tl.hd);
        copy = copy.tl;
        curr = curr.tl;
    }
    copy.tl = _copy_value(EMPTY_LIST);
    return copy_head;
}

function _copy_value_with_label(v, label){
    if(v.type === "list"){
        return _copy_list_with_label(v, label);
    }
    var res = _copy_value(v);
    res.label = label;
    return res;
}

function _copy_list_with_label(list, label){
    var copy_head = _object_assign_no_array({}, list);
    copy_head._type = "list";
    copy_head.hd = _copy_value_with_label(list.hd, label);
    copy_head.label = label;
    var curr = list;
    var copy = copy_head;
    while(curr.tl.type !== EMPTY_LIST.type){
        copy.tl = _object_assign_no_array({}, curr.tl);
        copy.tl._type = "list";
        copy.tl.hd = _copy_value_with_label(curr.tl.hd, label);
        copy.tl.label = label;
        copy = copy.tl;
        curr = curr.tl;
    }
    copy.tl = _copy_value_with_label(EMPTY_LIST, label);
    return copy_head;
}

function _handle_list_create_label(obj){
    if(obj.type === EMPTY_LIST.type){
        _assign_access_label(obj);
    } else {
        var upper = _upper_label_list(obj, _get_pc());
        var curr = obj;
        while(curr.type !== EMPTY_LIST.type){
            curr.hd = _copy_value(curr.hd);
            curr.hd.label = upper;
            curr.label = upper;
            curr = curr.tl;
        }
    }
}

function _object_assign_no_array(dest, src){
    for(var prop in src){
        if(typeof prop !== 'number'){
                dest[prop] = src[prop];
        }
    }
    return dest;
}

function _handle_event(args, event, callback){
    var curr_thread = _current_thread_id;
    switch(event.event_type){
    case "Const":
        _assign_access_label(args[0]);
        args[0].original_label = args[0].label;
        callback();
        break;
    case "List_create":
        _handle_list_create_label(args[0]);
        callback();
        break;
    case "Var":
        //If the label is undefined, set it as bottom. This requires that values without a label does not contain any secret information. Only values that do not have labels are initial values.
        if(args[0].label === undefined){
            args[0].label = _labels.bottom;
        }
        args[0] = _reassign_label(_labels.join(event.var.label, _get_pc()), args[0], event.var);
        callback();
        break;
    case "Classify":
        _labels.flows_to(args[0].label, event.label, null, function(error, res){
            _update_current_thread(curr_thread);
            if(error){
                console.log("Flows to error: ");
                console.log(error);
            } else {
                if(res){
                    args[0] = _reassign_label(event.label, args[0], event.var);
                    callback();
                } else {
                    console.log("Classifying error! Current label {" + _labels.to_string(args[0].label) + "} does not flow to new label {" + _labels.to_string(event.label) + "}");
                    _end_thread[0](UNIT, _end_thread[1]);
                }
            }
        });
        break;
    case "Declassify":
        _labels.declassify(args[0].label, event.label, _trust_config.authority, function(err, res){
            _update_current_thread(curr_thread);
            if(err){
                console.log("Error while declassifying: ");
                console.log(err);
            } else {
                if(res){
                    _declassify_label(event.label, args[0], event.var, function(res){
                        args[0] = res;
                        callback();
                    });
                } else {
                    console.log("Declassifying error! Cannot declassify {" + _labels.to_string(args[0].label) + "} to {" + _labels.to_string(event.label) + "}");
                    _end_thread[0](UNIT, _end_thread[1]);
                }
            }
        });
        break;
    case "Call":
        var new_label = _labels.join(event.func.call_label, _get_pc());
        _pc_stacks[_current_thread_id.thread_id].push(new_label);
           callback();
        break;
    case "Return":
        _pc_stacks[_current_thread_id.thread_id].pop();
        callback();
        break;
    case "Branch":
        var new_label = _labels.join(event.var.label, _get_pc());
        _pc_stacks[_current_thread_id.thread_id].push(new_label);
        callback();
        break;
    case "Join":
        _pc_stacks[_current_thread_id.thread_id].pop();
        callback();
        break;
    case "Cons":
        _labels.flows_to(event.var1.label, event.var2.label, null, function(err, res){
            _update_current_thread(curr_thread);
            if(err){
                console.log(err);
                return;
            }
            if(res){
                args[0].hd.label = event.var2.label;
                args[0].label = event.var2.label;
                callback();
            } else {
                console.log("Error cons'ing value to list! " + _labels.to_string(event.var1.label) + " does not flow to " + _labels.to_string(event.var2.label));
                _end_thread[0](UNIT, _end_thread[1]);
            }
        });
        break;
    case "Binop":
        var new_label = _labels.join(event.var1.label, event.var2.label);
        switch(event.res_type){
        case "Number":
            args[0] = new Number(args[0]);
            break;
        case "String":
            args[0] = new String(args[0]);
            break;
        case "Bool":
            args[0] = new Boolean(args[0]);
            break;
        }
        args[0].label = new_label;
        callback();
        break;
    case "Send":
        var receiver = args[0];
        var msg = args[1];
        var trust_level = _trust_of_key(receiver.node_key);
        var receiver_message_label = receiver.message_label;
        _labels.flows_to(_get_pc(curr_thread), receiver.message_label, null, function(err, res){
            _update_current_thread(curr_thread);
            if(err){
                console.log("Error checking flow:");
                console.log(err);
            } else {
                if(res){
                    _labels.flows_to(args[1].label, trust_level, null, function(err, res){
                        _update_current_thread(curr_thread);
                        if(err){
                            console.log("Error checking flow:");
                            console.log(err);
                        } else {
                            if(res){
                                callback();
                            } else {
                                console.log("Error sending message! Message label {" + _labels.to_string(args[1].label) + "} does not flow to trust level {" + _labels.to_string(trust_level) + "} of recipient!");
                                _end_thread[0](UNIT, _end_thread[1]);
                            }
                        }
                    });
                } else {
                    console.log("Error sending message! PC label {" + _labels.to_string(_get_pc(curr_thread)) + "} does not flow to maximum message label {" + _labels.to_string(receiver.message_label) + "} of recipient!");
                }
            }
        });
        break;
    case "Receive":
        var received = args[0];
        var sender = args[0].__sender;
        var trust_level = _trust_of_key(sender);
        _labels.flows_to(received.label, trust_level, null, function(err, res){
            _update_current_thread(curr_thread);
            if(err){
                console.log("Error checking flow:");
                console.log(err);
            } else {
                var new_received_label = received.label;
                if(!res){
                    //Capping the trust of the data to the trust of the sender
                    new_received_label = trust_level;
                }
                var final_label = _labels.join(new_received_label, _get_pc());
                args[0] = _reassign_label(final_label, args[0]);
                args[0].original_label = new_received_label;
                //If we have received a thread_id, we also need to look at the message label
                if(received._type === "thread_id"){
                    _labels.flows_to(received.message_label, trust_level, null, function(err, res){
                        _update_current_thread(curr_thread);
                        if(err){
                            console.log("Error checking flow:");
                            console.log(err);
                        } else {
                            var new_message_label = received.message_label;
                            if(!res){
                                new_message_label = trust_level;
                            }
                            received.message_label = new_message_label;
                            callback();
                        }
                    });
                } else {
                    callback();
                }
            }
        });
        break;
    }
}

var _flows_to_check = [function(exp, k1){
    var curr_thread = _current_thread_id;
    var next_fun = [function(label, k2){
        _update_current_thread(_current_thread_id);
        _flows_to_check_intern(exp.label, label, k2);
    }, {}];
    next_fun.label = exp.label;
    k1[0](next_fun, k1[1]);
}, {}];
_flows_to_check.label = _labels.bottom;

var _original_flows_to_check = [function(exp, k1){
    var curr_thread = _current_thread_id;
    var next_fun = [function(label, k2){
        _update_current_thread(_current_thread_id);
        _flows_to_check_intern(exp.original_label, label, k2);
    }, {}];
    next_fun.label = exp.label;
    k1[0](next_fun, k1[1]);
}, {}];
_original_flows_to_check.label = _labels.bottom;

function _flows_to_check_intern(exp_label, label, k){
    if(_global_monitored){
        var current_thread = _current_thread_id;
        _labels.flows_to(exp_label, label, null, function(err, res){
            _update_current_thread(current_thread);
            if(err){
                console.log("Error checking flow:");
                console.log(err);
            } else {
                var result = new Boolean(res);
                result.label = _labels.and(_get_pc(), exp_label);
                k[0](result, k[1]);
            }
        });
    } else {
        k[0](result, k[1]);
    }
}

function _get_pc(){
    var current_stack = _pc_stacks[_current_thread_id.thread_id];
    return current_stack[current_stack.length-1];
}

function _monitor(k, args, event){
    var current_thread = _current_thread_id;
    if(current_thread.monitored && _global_monitored){
        _handle_event(args, event, function(){
            k.apply(this, args);
        });
    } else {
        k.apply(this, args);
    }
}

var _schedule_limit = 50;
var _schedule_current = 0;

var _schedule = [function _schedule(thread, func, args){
    if(!_is_closure(func)){
        _update_current_thread(thread);
        _call_non_cps_function(func, args);
    }
    else if(_schedule_current === _schedule_limit){
        _schedule_current = 0;
        setImmediate(
            function(){
                _update_current_thread(thread);
                args[args.length - 1] = func[1];
                func[0].apply(this, args);
            });
    } else {
        _schedule_current++;
        _update_current_thread(thread);
        args[args.length - 1] = func[1];
        func[0].apply(this, args);
    }
}, {}];
_schedule.label = _labels.bottom;

var std_current_thread = [function std_current_thread(unit, k){
    _monitor(function std_current_thread_return(res, k){_monitor(k[0], [res, k[1]], {event_type: 'Return'})}, [_current_thread_id, k], {event_type: 'Var', var: _current_thread_id});
}, {}];
std_current_thread.label = _labels.bottom;

var std_create_thread_id = [function std_create_thread_id(ip, k1){
    var curr_thread = _current_thread_id;
    if(_global_monitored){
        _pop_from_pc_stack(_current_thread_id);
    }
    var f1 = [function std_create_thread_id_2(port, k2){
        _update_current_thread(curr_thread);
        if(_global_monitored){
            _pop_from_pc_stack(_current_thread_id);
        }
        var f2 = [function std_create_thread_id_3(id, k3){
            _update_current_thread(curr_thread);
            if(_global_monitored){
                _pop_from_pc_stack(_current_thread_id);
            }
            var f3 = [function std_create_thread_id_4(label, k4){
                _update_current_thread(curr_thread);
                var res = {type: "thread_id", location: {ip: ip, port: port}, thread_id: 1, node_key: id, monitored: true, message_label: _labels.get_label(label.valueOf())};
                if(_global_monitored){
                    res.label = _upper_label([ip.label, port.label, id.label], _get_pc());
                    _pop_from_pc_stack(_current_thread_id);
                }
                _monitor(k4[0], [res, k4[1]], {event_type: 'Var', var: res});
            }, {}];
            f3.label = _labels.join(f2.label, id.label);
            k3[0](f3, k3[1]);
        }, {}];
        f2.label = _labels.join(f1.label, port.label);
        k2[0](f2, k2[1]);
    }, {}];
    f1.label = ip.label;
    k1[0](f1, k1[1]);
}, {}];
std_create_thread_id.label = _labels.bottom;


function _update_current_thread(thread_id){
    _current_thread_id = thread_id;
}

function _thread_is_local(thread){
    return thread && thread.location.ip === _ip && thread.location.port === _port;
}

function _compare_thread_location(a, b){
    return a && b && a.ip === b.ip && a.port === b.port;
}

function _compare_thread_id(a, b){
    return a && b && _compare_thread_location(a.location, b.location) && a.thread_id === b.thread_id;
}

function _create_new_thread_id(monitored, label){
    _max_thread_id = _max_thread_id + 1;
    var res = {_type: "thread_id", location: {ip: _ip, port: _port}, thread_id: _max_thread_id, node_key: _trust_config.this_node, monitored: monitored, message_label: _labels.get_label(label.valueOf())};
    _threads[_max_thread_id] = res;
    return res;
}

var _end_thread = [function _end_thread(val){
    _running_threads--;
    if(_running_threads === 0){
        process.exit();
    }
}, {}];
_end_thread.label = _labels.bottom;

var std_start_thread = [function std_start_thread_1(func, k1){
    var current_thread = _current_thread_id;
    var f1 = [function std_start_thread_2(label, k2){
        _update_current_thread(current_thread);
        var thread_id = _create_new_thread_id(true, label);
        var base_label = _labels.bottom;
        var next_label = base_label;
        if(_global_monitored){
            thread_id.label = func.label;
            base_label = _labels.join(_get_pc(), _labels.get_label(label.valueOf()));
            next_label = _labels.join(base_label, func.label);
        }
        _pc_stacks[thread_id.thread_id] = [base_label, next_label];
        _running_threads++;
        _schedule[0](thread_id,
                     func,
                     [UNIT, _end_thread, UNIT]);
        
        _current_thread_id = current_thread;
        _monitor(function std_start_thread_return(res, k2){
            _monitor(k2[0], [res, k2[1]], {event_type: 'Return'})
        }, [thread_id, k2], {event_type: 'Var', var: thread_id});
    }, []];
    f1.label = func.label;
    _monitor(k1[0], [f1, k1[1]], {event_type: 'Return'});
}, {}];
std_start_thread.label = _labels.bottom;

var std_start_thread_unmonitored = [function std_start_thread_unmonitored_1(func, k1){
    var current_thread = _current_thread_id;
    var f1 = [function std_start_thread_unmonitored_2(label, k2){
        _update_current_thread(current_thread);
        var thread_id = _create_new_thread_id(false, label);
        var base_label = _labels.bottom;
        var next_label = base_label;
        if(_global_monitored){
            thread_id.label = func.label;
            base_label = _labels.join(_get_pc(), _labels.get_label(label.valueOf()));
            next_label = _labels.join(base_label, func.label);
        }
        _pc_stacks[thread_id.thread_id] = [base_label, next_label];
        _running_threads++;
        _schedule[0](thread_id,
                     func,
                     [UNIT, _end_thread, UNIT]);
        
        _current_thread_id = current_thread;
        _monitor(function std_start_thread_return(res, k2){
            _monitor(k2[0], [res, k2[1]], {event_type: 'Return'})
        }, [thread_id, k2], {event_type: 'Var', var: thread_id});
    }, []];
    f1.label = func.label;
    _monitor(k1[0], [f1, k1[1]], {event_type: 'Return'});
}, {}];
std_start_thread.label = _labels.bottom;

function _do_receive_message(thread_id, k){
    var thread_num = thread_id.thread_id;
    var message = _message_queues[thread_num].msg[0];
    _message_queues[thread_num].msg.splice(0, 1);
    _schedule[0](thread_id,
                 [function(){
                     _monitor(k[0], [message, k[1]], {event_type: 'Var', var: message})
                 }, {}],
                 [UNIT, UNIT]);
}

var _send_message = [function _send_message(thread, message, modules, k){
    if(_thread_is_local(thread)){
        _send_message_local(_current_thread_id, thread, message, _current_thread_id.node_key, k);
    } else {
        _send_message_remote(_current_thread_id, thread, message, modules, k);
    }
}, {}];

_send_message.label = _labels.bottom;

function _register_object(object, hash){
    _registered_objects[hash] = object;
}

function _fetch_object(fetchable_id, callback){
    var ip = fetchable_id.ip;
    var port = fetchable_id.port;
    var id = fetchable_id.id;
    var request = _http.request({hostname: ip,
                                 port: port,
                                 path: "/lookup/" + id,
                                 method: "GET",
                                },
                                function(res){
                                    var rawResult = "";
                                    res.on("data", function(chunk){
                                        rawResult += chunk;
                                    });

                                    res.on("end", function(){
                                        var result = JSON.retrocycle(JSON.parse(rawResult));
                                        if(result.success){
                                            var restored = _restore_message(result.result);
                                            callback(restored);
                                        } else {
                                            console.log("Error! Could not fetch object");
                                        }
                                    });
                                });
    request.on("error", function(e){
        console.log("Error! " + e);
    });
    request.end();
}

function _is_closure(object){
    return object instanceof Array &&
        object.length === 2 &&
        object[0] instanceof Function
}

function _prepare_message(message){
//Inspired by: http://stackoverflow.com/a/14962496
    var seen = [];
    var handled = []
    var placeholder = {};
    var json_seen = [];
    var json_handled = [];

    function flatten_for_json(message, parent){
        var index = json_seen.indexOf(message);
        var res;

        if(index === -1){
            index = json_seen.length;
            json_seen.push(message);
            json_handled.push(placeholder);
        } else {
            var candidate = json_handled[index];
            if(candidate === placeholder){
                return parent;
            } else {
                return candidate;
            }
        }

        if(message === undefined){
            res = undefined;
        } else if(message instanceof Number){
            res = {type: "Number", val: message.valueOf(), label: message.label};
        } else if(message instanceof String){
            res = {type: "String", val: message.valueOf(), label: message.label};
        } else if(message instanceof Boolean){
            res = {type: "String", val: message.valueOf(), label: message.label};
        } else if(message.type === UNIT.type){
            res = {type: "unit", label: message.label};
        } else if(message.type === EMPTY_LIST.type){
            res = {type: "empty_list", label: message.label};
        } else if(message instanceof Array){
            var tmp = [];
            for(var i = 0; i < message.length; i++){
                (function(i){
                    tmp[i] = flatten_for_json(message[i], tmp);
                })(i);
            }
            res = {type: "Array", val: tmp.valueOf(), label: message.label};
        } else if(message instanceof Object){
            var tmp = {};
            for(var prop in message){
                tmp[prop] = flatten_for_json(message[prop], tmp);
            }
            res = {type: "Object", val: tmp.valueOf(), label: message.label};
        } else {
            res = {val: message};
        }
        json_handled[index] = res;
        return res;       
    }

    function remove_recompilable_functions(env){
        var new_env = {};
        for(var prop in env){
            if(prop[0] !== '_'){
                new_env[prop] = env[prop];
            }
        }
        return new_env;
    }
    
    function prepare_message_recursive(message, parent){
        var index = seen.indexOf(message);
        var res;

        if(index === -1){
            index = seen.length;
            seen.push(message);
            handled.push(placeholder);
        } else {
            var candidate = handled[index];
            if(candidate === placeholder){
                //If something is marked as handled, but is a placeholder, then it is recursive.
                return parent;
            } else {
                return candidate;
            }
        }
        if(message === undefined){
            res = undefined;
        } else if (_is_closure(message)){
            if(message.remote_wrapper != undefined){
                var new_message = message.remote_wrapper;
                new_message.label = message.label;
                new_message.call_label = message.call_label;
                res = new_message;
            } else {
                var prepared = [];
                prepared[0] = prepare_message_recursive(message[0], prepared);
                prepared[1] = prepare_message_recursive(message[1], prepared);
                prepared[1] = remove_recompilable_functions(prepared[1]);
                prepared.label = message.label;
                prepared.call_label = message.call_label;
                var hash = _hash_of_string(JSON.stringify(JSON.decycle(prepared)));
                //Might not be nice to flatten it here
                _register_object(flatten_for_json(prepared), hash);
                res = {_type: "fetchable_id", ip: _ip, port: _port, id: hash, label: prepared.label, key: _current_thread_id.node_key, call_label: message.call_label};
            }
        } else if(message.type === EMPTY_LIST.type || message.type === UNIT.type){
            return message;
        } else if(message instanceof Function){
            var func = __string_ir.substring(message.position.begin, message.position.end);
            var modules = __string_ir.substring(0, __ir_module_length);
            var ir = modules + " " + func;
            var new_message = {_type: "function", body: ir};
            if(message.fix_name !== undefined){
                new_message.fix_name = message.fix_name;
            }
            new_message.label = message.label;
            res = new_message;
        } else if(message instanceof Array) {
            var new_message = [];
            for(var i = 0; i < message.length; i++){
                (function(i){
                    new_message[i] = prepare_message_recursive(message[i], new_message);
                })(i);
            }
            new_message.label = message.label;
            res = new_message;
        } else if(message instanceof Number || message instanceof String || message instanceof Boolean) {
            res = message;
        } else if(message instanceof Object) {
            var new_message = {};
            for(var i in message){
                new_message[i] = prepare_message_recursive(message[i], new_message);
            }
            new_message.label = message.label;
            res = new_message;
        } else {
            res = message;
        }
        handled[index] = res;
        return res;
    }

    var prepared = prepare_message_recursive(message, undefined);
    var flattened = flatten_for_json(prepared, undefined);
    return flattened;
}

function _run_remote_closure(id, args){
    var cached_candidate = _cached_objects[id.id];
    if(cached_candidate !== undefined){
        var module_env = _remote_module_names[id.key];
        //Replace the dummy closure with the cached one
        args[args.length-1] = _merge_envs(cached_candidate[1], module_env);
        //Inspired by https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Function/apply
        cached_candidate[0].apply(undefined, args);
        _pop_from_pc_stack(_current_thread_id);//Todo: check if timing of this is okay
    } else {
        _fetch_object(id, function(value){
            _cached_objects[id.id] = value;
            _run_remote_closure(id, args);
        });
    }
}
function _restore_message(message){
//Inspired by: http://stackoverflow.com/a/14962496
    var seen = [];
    var handled = [];
    var placeholder = {};
    var json_seen = [];
    var json_handled = [];

    function restore_from_json(message, parent){
        var index = json_seen.indexOf(message);
        var res;

        if(index === -1){
            index = json_seen.length;
            json_seen.push(message);
            json_handled.push(placeholder);
        } else {
            var candidate = json_handled[index];
            if(candidate === placeholder){
                return parent;
            } else {
                return candidate;
            }
        }

        if(message.type === "Number") {
            var tmp = new Number(message.val);
            tmp.label = message.label;
            res = tmp;
        } else if(message.type === "String"){
            var tmp = new String(message.val);
            tmp.label = message.label;
            res = tmp;
        } else if(message.type === "Boolean"){
            var tmp = new Boolean(message.val);
            tmp.label = message.label;
            res = tmp;
        } else if(message.type === "unit"){
            var tmp = _copy_value(UNIT);
            tmp.label = message.label;
            res = tmp
        } else if(message.type === "empty_list"){
            var tmp = _copy_value(EMPTY_LIST);
            tmp.label = message.label;
            res = tmp;
        } else if(message.type === "Array"){
            var tmp = [];
            for(var i = 0; i < message.val.length; i++){
                (function(i){
                    tmp[i] = restore_from_json(message.val[i], tmp);
                })(i);
            }
            tmp.label = message.label;
            res = tmp;
        } else if(message.type === "Object"){
            var tmp = {};
            for(var prop in message.val){
                tmp[prop] = restore_from_json(message.val[prop], tmp);
            }
            tmp.label = message.label;
            res = tmp;
        } else {
            var tmp = message.val;
            res = tmp;
        }
        json_handled[index] = res;
        return res;
    }
    
    function restore_message_recursive(message, parent){
        var index = seen.indexOf(message);
        var res;

        if(index === -1){
            index = seen.length;
            seen.push(message);
            handled.push(placeholder);
        } else {
            var candidate = handled[index];
            if(candidate === placeholder){
                //If something is marked as handled, but is a placeholder, then it is recursive.
                return parent;
            } else {
                return candidate;
            }
        }

        if(message === undefined){
            res = undefined;
        } else if(message._type === "fetchable_id"){
            //Returns closure for function that fetches, and caches, remote closure on application, and then applies the fetched closure
            var tmp = [function(){
                //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments
                _run_remote_closure(message, Array.from(arguments));
            }, {}];
            tmp.label = message.label;
            tmp.call_label = message.call_label;
            tmp.remote_wrapper = message;
            res = tmp;
        } else if (message._type === "blocking_message") {
            if(message.message instanceof Array){
                message.message.unshift(message.thread);
                res = message.message;
            } else {
                var new_message = [];
                new_message[0] = restore_message_recursive(message.thread, new_message);
                new_message[1] = restore_message_recursive(message.message, new_message);
                res = new_message;
            }
        } else if (message instanceof Number || message instanceof String || message instanceof Boolean){
            res = message;
        } else if (message.type === "empty_list" || message.type === "unit"){
            res = message;
        } else if (message instanceof Array && message.length === 2 && message[0]._type === "function") {
            var restored = [];
            var func = restore_message_recursive(message[0], restored);
            var closure = restore_message_recursive(message[1], restored);
            var final_closure = _merge_envs(func[1], closure);
            restored[0] = func[0];
            restored[1] = final_closure;
            restored.label = message.label;
            restored.call_label = message.call_label;
            if(message[0].fix_name === undefined){
                res = restored;
            } else {
                //Return the correct closure for the complete fixpoint
                res = final_closure[message[0].fix_name];
            }
        } else if(message._type === "function"){
            var compiled = _exec("echo '" + message.body + "' | ./" + _compiler_name + " -runtime true", {encoding: "UTF-8"});
            var evalled = eval(compiled);
            evalled.label = message.label;
            res = evalled;
        } else if(message instanceof Array) {
            var new_message = [];
            for(var i = 0; i < message.length; i++){
                (function(i){
                    new_message[i] = restore_message_recursive(message[i], new_message);
                })(i);
            }
            new_message.label = message.label;
            res = new_message;
        } else if(message instanceof Object) {
            var new_message = {};
            for(var i in message){
                new_message[i] = restore_message_recursive(message[i], new_message);
            }
            new_message.label = message.label;
            res = new_message;
        } else {
            res = message;
        }
        handled[index] = res;
        return res;
    }
    
    var structured = restore_from_json(message, undefined);
    var restored = restore_message_recursive(structured, undefined);
    return restored;
}

function _reverse_map(map, key){
    for(var prop in map){
        if(map[prop].valueOf() === key.valueOf()){
            return prop;
        }
    }
    return undefined;
}

function _create_module_map(modules){
    var map = {};
    for(var i = 0; i < modules.length; i++){
        var mod_name = _reverse_map(__mods, modules[i]);
        map[mod_name] = modules[i];
    }
    return map;
}

var _raise_error = [function _raise_error(message){
    console.log("Error: " + message);
    process.exit();
}, {}];
_raise_error.label = _labels.bottom;

function _send_message_remote(this_thread, thread, message, modules, k){
    var modules_used = _create_module_map(modules);
    var final_message = JSON.decycle(_prepare_message(message));
    var body = JSON.stringify({recipient: thread.thread_id, message: final_message, modules: modules_used, node_key: this_thread.node_key});
    var request = _http.request({hostname: thread.location.ip.valueOf(),
                                 port: thread.location.port.valueOf(),
                                 path: "/message",
                                 method: 'POST',
                                 headers: {
                                     "Content-Type": "application/json",
                                     "Content-Length": Buffer.byteLength(body)
                                 }
                                },
                                function(res){
                                    var rawResult = "";
                                    res.on("data", function(chunk){
                                        rawResult += chunk;
                                    });

                                    res.on("end", function(){
                                        var result = JSON.retrocycle(JSON.parse(rawResult));
                                        if(result.success){
                                            var res = new Boolean(true);
                                            res.label = thread.label;
                                            var result = [res, UNIT];
                                            result.label = thread.label;
                                            _schedule[0](this_thread,
                                                         [function(){
                                                             _monitor(k[0], [result, k[1]], {event_type: 'Var', var: result});
                                                         }, {}],
                                                         [UNIT, UNIT]);
                                        } else {
                                            var res = new Boolean(false);
                                            res.label = thread.label;
                                            var result = [res, result.error, result.params];
                                            result.label = thread.label;
                                            _schedule[0](this_thread,
                                                         [function(){
                                                             _monitor(k[0], [result, k[1]], {event_type: 'Var', var: result});
                                                         }, {}],
                                                         [UNIT, UNIT]);
                                        }
                                    });
                                });
    request.on("error", function(e){
        var result = [false, e, []];
        _schedule[0](this_thread,
                     [function(){
                         _monitor(k[0], [result, k[1]], {event_type: 'Var', var: result});
                     }, {}],
                     [UNIT, UNIT]);
    });

    request.write(body);
    request.end();
}

function _send_message_local(this_thread, thread, message, sender, k){
    var thread_num = thread.thread_id;
    if(_message_queues[thread_num] === undefined){
        _message_queues[thread_num] = {msg : [], delayed : []};
    }
    message.__sender = sender;
    message.original_label = message.label;
    _message_queues[thread_num].msg.push(message);
    if(_receive_queue[thread_num] !== undefined){
        var func = _receive_queue[thread_num];
        _receive_queue[thread_num] = undefined;
        _do_receive_message(thread, func);
    }
    var res = new Boolean(true);
    res.label = _get_pc();
    var result = [res, UNIT];
    if(this_thread != undefined){
        _schedule[0](this_thread,
                     [function(){
                         _monitor(k[0], [result, k[1]], {event_type: 'Const'});
                     }, {}],
                     [UNIT, UNIT]);
    }
}

var _receive_message = [function _receive_message(k){
    var thread_id = _current_thread_id;
    var thread_num = thread_id.thread_id;
    if(_message_queues[thread_num] === undefined || _message_queues[thread_num].msg.length === 0){
        _receive_queue[thread_num] = k;
    } else {
        _do_receive_message(thread_id, k);
    }
}, {}];
_receive_message.label = _labels.bottom;

var _guarded_receive_failed = [function _guarded_receive_failed(msg, k){
    var current_thread = _current_thread_id;
    var thread_num = _current_thread_id.thread_id;
    if(_message_queues[thread_num] === undefined){
        _message_queues[thread_num] = {msg : [], delayed : []};
    }
    _message_queues[thread_num].delayed.push(msg);
    _schedule[0](current_thread,
                 [function _guarded_receive_failed_return_1(){
                     _monitor(function _guarded_receive_failed_return_2(res, k){
                         _monitor(k[0], [res, k[1]], {event_type: 'Return'})
                     }, [UNIT, k], {event_type: 'Const'});
                 }, {}],
                 [UNIT, UNIT]);
}, {}];
_guarded_receive_failed.label = _labels.bottom;

var _guarded_receive_success = [function _guarded_receive_success(unit, k){
    var current_thread = _current_thread_id;
    var thread_num = _current_thread_id.thread_id;
    var message_queue_obj = _message_queues[thread_num];
    var dummy_k = [function(){}, {}];
    if(message_queue_obj.delayed.length > 0){
        for(var i = 0; i < message_queue_obj.delayed.length; i++){
            _send_message_local(current_thread, _current_thread_id, message_queue_obj.delayed[i], message_queue_obj.delayed[i].__sender,  dummy_k);
        }
    }
    _schedule[0](current_thread,
                 [function _guarded_receive_success_return_1(){
                     _monitor(function _guarded_receive_success_return_2(res, k){
                         _monitor(k[0], [res, k[1]], {event_type: 'Return'})
                     }, [UNIT, k], {event_type: 'Const'});
                 }, {}],
                 [UNIT, UNIT]);
}, {}];
_guarded_receive_success.label = _labels.bottom;

var std_change_tuple = [function std_change_tuple_1(tuple, k1){
    var curr_thread = _current_thread_id;
    var f1 = [function std_change_tuple_2(index, k2){
        _update_current_thread(curr_thread);
        var f2 = [function std_change_tuple_3(value, k3){
            _update_current_thread(curr_thread);
            if(_global_monitored){
                var label = _labels.join(tuple.label, value.label);
            }
            var res = _copy_value_with_label(tuple, label);
            res[index] = value;
            _monitor(k3[0], [res, k3[1]], {event_type: 'Return'});
        }, {}];
        if(_global_monitored){
            f2.label = _labels.join(f1.label, index.label);
        }
        _monitor(k2[0], [f2, k2[1]], {event_type: 'Return'});
    }, {}];
    f1.label = tuple.label;
    _monitor(k1[0], [f1, k1[1]], {event_type: 'Return'});
}, {}];
std_change_tuple.label = _labels.bottom;

//Inspired by http://www.cs.cornell.edu/courses/cs3110/2016fa/l/03-lists/notes.html
function _append_rec(l1, l2, label){
    if(l1.type === EMPTY_LIST.type){
        return _copy_value(l2);
    } else {
        return {_type: "list",
                hd: _copy_value(l1.hd),
                tl: _append_rec(l1.tl, l2),
                label: label};
    }
}

var std_append = [function std_append(l1, k1){
    var curr_thread = _current_thread_id;
    var f1 = [function std_append_2(l2, k2){
        _update_current_thread(curr_thread);
        var label = _labels.join(l1.label, l2.label);
        var res = _append_rec(l1, l2);
        res = _copy_list_with_label(res, label);
        _pop_from_pc_stack(_current_thread_id);
        _monitor(k2[0], [res, k2[1]], {event_type: 'Return'});
    }, {}];
    f1.label = l1.label;;
    k1[0](f1, k1[1]);
}, {}];
std_append.label = _labels.bottom;

function _string_of_list(l){
    if(l.type !== EMPTY_LIST.type){
        if(l.tl.type !== EMPTY_LIST.type){
            return _js_string_of_val(l.hd) + ", " + _string_of_list(l.tl);
        } else {
            return _js_string_of_val(l.hd);
        }
    } else {
        return "";
    }
}

function _equal_test(val1, val2, negate){
    var res = false;
    if(val1.type === "unit" && val2.type === "unit"){
        res = true;
    } else if(val1.type === "empty_list" && val2.type === "empty_list"){
        res = true;
    } else if(val1._type === "list" && val2._type === "list"){
        res = _equal_test(val1.hd, val2.hd, false) && _equal_test(val1.tl, val2.tl, false);
    } else if(val1 instanceof Array && val2 instanceof Array && val1.length === val2.length){
        res = true;
        for(var i = 0; i < val1.length; i++){
            if(_equal_test(val1[i], val2[i], true)){
                res = false;
                break;
            }
        }
    } else if((val1 instanceof Number || val1 instanceof String || val1 instanceof Boolean) &&
              (val2 instanceof Number || val2 instanceof String || val1 instanceof Boolean)){
        res = val1.valueOf() == val2.valueOf();
    } else if(val1 instanceof Object && val2 instanceof Object && Object.getOwnPropertyNames(val1).length === Object.getOwnPropertyNames(val2).length){
        res = true;
        for(var prop in val1){
            if(_equal_test(val1[prop], val2[prop], true)){
                res = false;
                break;
            }
        }
    } else {
        res = val1 == val2;
    }
    if(negate){
        return !res;
    } else {
        return res;
    }
}

function _js_string_of_val(v){
    if(v.type === "unit"){
        return "()";
    } else if (v.type === "empty_list"){
        return "[]";
    } else if(v instanceof Array){
        var res = "(";
        for(var i = 0; i < v.length-1; i++){
            res += _js_string_of_val(v[i]) + ", ";
        }
        res += _js_string_of_val(v[v.length-1]);
        res += ")";
        return res;
    } else if (v.hasOwnProperty("location") && v.hasOwnProperty("thread_id")){
        return "Thread id(" + v.location.ip + ":" + v.location.port + ", " + v.thread_id + ")";
    } else if (v._type === "list"){
        return "[|" + _string_of_list(v) + "|]";
    } else {
        return "" + v;
    }
}

function _hash_of_string(s){
    var hash = _crypto.createHash("sha256");
    hash.update(s);
    return hash.digest("hex");
}

var std_string_of_val = [function std_string_of_val(v, k){
    var res = new String(_js_string_of_val(v));
    res.label = v.label;
    _monitor(k[0], [res, k[1]], {event_type: 'Return'});
}, {}];
std_string_of_val.label = _labels.bottom;

var std_hash = [function std_hash(v, k) {
    var res = new String(_hash_of_string(_js_string_of_val(v)));
    res.label = v.label;
    _monitor(k[0], [res, k[1]], {event_type: 'Return'});
}, {}];
std_hash.label = _labels.bottom;

var _res = [function _res(v){
    console.log("Result: " + _js_string_of_val(v));
}, {}];
_res.label = _labels.bottom;

var std_print = [function std_print(v, k){
    if(!_global_monitored){
        console.log(_js_string_of_val(v));
        _monitor(function(res, k){
            _monitor(k[0], [res, k[1]], {event_type: 'Return'})
        }, [UNIT, k], {event_type: 'Const'});
    } else {
        var current_thread = _current_thread_id;
        _labels.flows_to(v.label, _labels.get_label(_trust_config.this_node), null, function(error, res){
            if(error){
                console.log("Flows to error:");
                console.log(error);
            } else {
                if(res){
                    console.log(_js_string_of_val(v));
                    _update_current_thread(current_thread);
                    _monitor(function(res, k){
                        _monitor(k[0], [res, k[1]], {event_type: 'Return'})
                    }, [UNIT, k], {event_type: 'Const'});
                } else {
                    console.log("Security error! Value with label: {" + _labels.to_string(v.label) + "} cannot be printed on node with label: {" + _trust_config.this_node + "}");
                    _end_thread[0](UNIT, _end_thread[1]);
                }
            }
        });
    }
}, {}];
std_print.label = _labels.bottom;

var std_read = [function std_read(unit, k){
    var current_thread = _current_thread_id;
    _std_in.question("", function(input){
        var res = new String(input);
        if(_global_monitored){
            res.label = _labels.join(_labels.get_label(_trust_config.this_node), _get_pc());
        }
        _schedule[0](current_thread,
                     [function(){
                         _monitor(k[0], [res, k[1]], {event_type: 'Return'});
                     }, {}],
                     [UNIT, UNIT]);
    });
}, {}];
std_read.label = _labels.bottom;

function _merge_envs (env1, env2){
    var new_env = {};
    for(property in env1){
        new_env[property] = env1[property];
    }
    for(property in env2){
        new_env[property] = env2[property];
    }
    return new_env;
}


