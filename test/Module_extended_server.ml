using "test-module" as test, "mathjs" as math;;
let rec loop = fun () ->
  receive with
    (sender, func) -> let func_res = [func ()] in
                      sender ! func_res;
                      [loop ()]
  | a -> [std_print ("Unknown message: " ^ [std_string_of_val a])];
         [loop ()]
in [loop ()]
