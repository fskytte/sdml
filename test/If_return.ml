let func = fun a -> if a = 1 then
                      1
                    else if a = 2 then
                      2
                    else if a = 3 then
                      3
                    else
                      4
    in
    [func 2]
