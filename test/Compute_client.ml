let server = [std_lookup "localhost" 3000 "compute"] in
    server ! (fun () -> "Hello", [std_current_thread ()]);
    [std_print ("Received: " ^ [std_string_of_val receive])];
    server ! (fun () -> 2 + 4, [std_current_thread ()]);
    [std_print ("Received: " ^ [std_string_of_val receive])];
    let a = 25 in
    server ! (fun () -> a, [std_current_thread ()]);
    [std_print ("Received: " ^ [std_string_of_val receive])]
