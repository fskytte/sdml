let rec loop = fun remaining ->
  if remaining = 0 then
    ()
  else
    [loop (remaining - 1)]
in
[loop 1000]
