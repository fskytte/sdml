[std_register "localhost" 3000 "compute" [std_current_thread ()]];
[std_print "Registered"];
let rec loop = fun () -> let value = receive in
                         let operation_tuple = [std_tuple_index value 0] in
                         let sender = [std_tuple_index value 1] in
                         let operation = [std_tuple_index operation_tuple 0] in
                         let operand_1 = [std_tuple_index operation_tuple 1] in
                         let operand_2 = [std_tuple_index operation_tuple 2] in
                         (if operation = "+" then
                            sender ! (operand_1 + operand_2)
                          else if operation = "-" then
                            sender ! (operand_1 - operand_2)
                          else
                            sender ! "Unknown operation");
                         [loop ()]
    in
    [loop ()]
