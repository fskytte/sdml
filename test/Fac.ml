let rec fac = fun n -> if n = 0 then
                     1
                   else
                     [fac (n - 1)] * n
    in [std_print [fac 30]]
