let l1 = [|1,2,3|] in
    let l2 = [|4,5,6|] in
    [std_print ("Appended: " ^ [std_string_of_val [std_append l1 l2]])];
    [std_print ("l1: " ^ [std_string_of_val l1])];
    [std_print ("l2: " ^ [std_string_of_val l2])]
