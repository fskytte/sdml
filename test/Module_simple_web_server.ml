using "express" as express;;
let app = [express ()] in
    [app.get "/" (fun req res ->
                  [std_print "Sending reply"];
                  let send_res = [res.send "<html><head><title>Test</title></head><body>This is a test page</body></html>"] in
                  [std_print ("Send res: " ^ [std_string_of_val send_res])])];
    [express.listen 3000];
    [std_print express];
    receive
