"Inspired by: http://composingprograms.com/pages/17-recursive-functions.html";
let rec is_even = fun n ->
  (if n = 0 then
     true
   else
     [is_odd (n - 1)])
and is_odd = fun n ->
  (if n = 0 then
     false
   else
     [is_even (n - 1)])
in
[is_even 4]
