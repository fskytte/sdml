let a = "hej" in
    [std_print (match a with
                  (1, "hej") -> "Is tuple?"
                | _ -> "Is something else")]
