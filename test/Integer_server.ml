let name_server = [std_create_thread_id "localhost" 3000 "local"] in
    name_server ! ([std_current_thread ()], "register", "int_server", [std_current_thread ()]);
    [std_print "Registered"];
    let a = 25 in
    let b = 87 in
    let rec loop = fun () ->
      let received = receive in
      let sender = [std_tuple_index received 0] in
      let func = [std_tuple_index received 1] in
      let res = [func a b] in
      sender ! res;
      [loop ()]
    in
    [loop ()]
