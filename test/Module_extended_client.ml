using "test-module" as testcl, "mathjs" as mathcl;;
let server = [std_create_thread_id "localhost" 3000 "local"] in
server !{testcl} ([std_current_thread ()], (fun () -> [testcl.add 1 2]));
let res = receive in
[std_print ("Received: " ^ res)];
server !{mathcl} ([std_current_thread ()], (fun () -> [mathcl.sqrt 5]));
let res = receive in
[std_print ("Received: " ^ res)];
server !{mathcl, testcl} ([std_current_thread ()], fun () -> [testcl.add [mathcl.sqrt 5] 2]);
let res = receive in
[std_print ("Received: " ^ res)]
