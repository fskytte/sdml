[std_register "localhost" 3000 "calc_server" [std_current_thread ()]];
[std_print "Registered"];
let rec loop = fun () ->
  let input = receive in
  (if [std_tuple_index input 0] = "+" then
     [std_tuple_index input 3] ! ([std_tuple_index input 1] + [std_tuple_index input 2])
   else if [std_tuple_index input 0] = "-" then
     [std_tuple_index input 3] ! ([std_tuple_index input 1] - [std_tuple_index input 2])
   else
     [std_tuple_index input 3] ! "Unknown input");
  [loop ()]
in [loop ()]
