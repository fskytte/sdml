[std_print "Who are you?"];
let name = [std_read ()] in
[std_print (match name with
              "you" -> "No, I'm me!"
            | "me" -> "Yes, you are you."
            | _ -> "I didn't get that?")]
