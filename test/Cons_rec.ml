let rec f = fun count l ->
  if count = 0 then
    l
  else
    [f (count - 1) (count :: l)]
    in
    [f 10 []]
