let a = 2 in
    let s = (match a with
               a {_} -> "Matches a"
             | a {local} -> "Matches local") in
    [std_print s]
