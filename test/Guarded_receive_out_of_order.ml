let thread = [std_start_thread (fun () -> receive with
                                  "first" -> [std_print "Received first"];
                                             receive with
                                              "second" -> [std_print "Received second"];
                                                          receive with
                                                           "third" -> [std_print "Received third"])] in
thread ! "third";
thread ! "second";
thread ! "first";
[std_print "Program end"]
