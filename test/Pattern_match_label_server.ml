receive with
  a {player1} -> [std_print ("Received: " ^ [std_string_of_val a] ^ " with label player1")]
| a -> [std_print ("Received: " ^ [std_string_of_val a] ^ " with other label")]
