let rec thread_fun = fun id ->
  [std_print (id ^ " is running!")];
  [thread_fun id]
in
[std_start_thread (fun () -> [thread_fun 1])];
[std_start_thread (fun () -> [thread_fun 2])]    
