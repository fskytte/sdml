//[std_print "Hostname:"];
//let hostname = [std_read ()] in
//[std_print "Port:"];
//let port = [std_read ()] in
//[std_print "Operations:"];
//let operation_count = [std_read ()] in
//[std_print "Starting index:"];
//let starting_index = [std_read ()] in
let hostname = "localhost" in
let port = 3000 in
let operation_count = 100 in
let starting_index = 3000 in
let host_thread = [std_create_thread_id hostname port "local" "local"] in
host_thread ! ([std_current_thread ()], "get_peer");
receive with
  (_, (peer, _)) -> let rec do_stores = fun count value ->
                      if count = operation_count then
                        ()
                      else
                        (peer ! ([std_current_thread ()], "store_data", (value, count));
                         receive with
                           "added" -> [do_stores (count + 1) (value + 1)])
                    in
                    let rec do_reads = fun count ->
                      if count = operation_count then
                        ()
                      else
                        (peer ! ([std_current_thread ()], "get_data", count);
                         receive with
                           _ -> [do_reads (count + 1)])
                    in
                    [std_print "Beginning"];
                    [do_stores 0 starting_index];
                    [std_print "Stored"];
                    [do_reads 0];
                    [std_print "Done"]
