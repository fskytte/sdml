let rec print_times = fun message times ->
  if times = 0 then
    ()
  else
    [std_print message]; [print_times message (times - 1)]
    in
    [std_start_thread (fun () -> [print_times 0 10])];
    [std_start_thread (fun () -> [print_times 1 10])];
    [print_times 2 10]
