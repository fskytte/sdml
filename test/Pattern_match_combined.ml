let value = (1, [|1,2,3|], "Hello", (1, 2)) in
[std_print (match value with
              (1, [|1, 2, 3|], "Hello", (1, 2)) -> "exact match")];
[std_print (match value with
              (_, [|1|], _, _) -> "Wrong list"
            | (1, _, "Hello", (1,2)) -> "List wildcard")];
[std_print (match value with
              (1, a, "Hello", _) -> "Pair wildcard. List is: " ^ [std_string_of_val a])];
[std_print (match value with
              (_, [|1, _, 3|], _, _) -> "Wildcard in list")]
