let test = [std_start_thread (fun () ->
                              let rec loop = fun v ->
                                receive with
                                  ("set", u, sender) -> sender ! "ok"; [loop u]
                                | ("read", sender) -> sender ! v; [loop v]
                              in
                              [loop ()]) "a & local"]
in
test ! ("set", 1, [std_current_thread ()]);
receive with
  "ok" -> test ! ("read", [std_current_thread ()]);
          let v = receive in
          v
