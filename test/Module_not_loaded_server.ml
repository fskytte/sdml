let name_server = [std_create_thread_id "localhost" 3000] in
    name_server ! ([std_current_thread ()], "register", "module", [std_current_thread ()]);
    [std_print "Registered"];
    let rec loop = fun () ->
      let received = receive in
      let sender = [std_tuple_index received 0] in
      let message = [std_tuple_index received 1] in
      [std_print message];
      sender ! "Message sent!";
      [loop ()]
    in
    [loop ()]
