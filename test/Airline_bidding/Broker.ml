let user_thread = [std_create_thread_id "localhost" 3002 "user1" "user1 & broker"] in
let agent1_thread = [std_create_thread_id "localhost" 3003 "agent1" "agent1 & broker"] in
let agent2_thread = [std_create_thread_id "localhost" 3004 "agent2" "agent2 & broker"] in
let broker = fun user agent1 agent1_id agent2 agent2_id ->
  let rec bidding_loop  = fun round current_offer -> 
    [std_print ("Round " ^ round ^ ", current offer: " ^ [std_string_of_val current_offer])];
    let agent1_offer = [agent1 user current_offer] in 
    let agent2_offer = [agent2 user current_offer] in 
    match (agent1_offer, agent2_offer) with
      ((), offer) -> [std_print declassify (agent2_id ^ " wins with: " ^ offer) as {_}];
                                            user_thread ! declassify ("best_offer", offer, agent2_id) as {user1}
    | (offer, ()) -> [std_print declassify (agent1_id ^ " wins with: " ^ offer) as {_}];
                                            user_thread ! declassify ("best_offer", offer, agent1_id) as {user1}
    | (offer1, offer2) -> (match [user offer1 offer2] with
                                                    -1 ->
                                                       [bidding_loop (round + 1) offer1]
                                                  | 0  ->
                                                       [std_print ("Offers are equally good. Choosing " ^ agent1_id ^ " as winner")];
                                                         user_thread ! declassify ("best_offer", offer1, agent1_id) as {user1}
                                                  | 1  ->
                                                       [bidding_loop (round + 1) offer2])
  in [bidding_loop 1 ()]
in
user_thread ! declassify ([std_current_thread ()], "get_choice") as {user1};
agent1_thread ! declassify ([std_current_thread ()], "get_agent") as {agent1};
agent2_thread ! declassify ([std_current_thread ()], "get_agent") as {agent2};
receive with
  ("choice", user) declassify as {_} -> receive with
     ("agent", agent1, agent1_id) declassify as {_} -> receive with
        ("agent", agent2, agent2_id) declassify as {_} ->
           [broker user agent1 agent1_id agent2 agent2_id]
