let make_offer = fun user_choice best_offer ->
  let rec find_offer = fun current_offer ->
    let new_offer = current_offer - 1 in
    let candidate_response = [user_choice new_offer best_offer] in
    if candidate_response = -1 then
      new_offer
    else
      [find_offer new_offer]
  in
  if best_offer = () then
    10000
  else
    [find_offer best_offer]
in
let rec get_agent_fun = fun () ->
  [std_print ("Receiving requests")];
  (receive with
     (sender, "get_agent") -> sender ! classify ("agent", make_offer, "agent1") as {broker & agent1});
  [get_agent_fun ()] in
[get_agent_fun ()]
