let make_offer = fun user_choice best_offer ->
  if (best_offer = ()) || (best_offer > 5000) then 
    5000
  else
    ()
in
let rec get_agent_fun = fun () ->
  [std_print ("Receiving requests")];
  (receive with
     (sender, "get_agent") -> sender ! classify ("agent", make_offer, "agent2") as {broker & agent2});
  [get_agent_fun ()] in
[get_agent_fun ()]
