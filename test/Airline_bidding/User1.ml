let choice = fun offer1 offer2 ->
  if offer1 = offer2 then
    0
  else if offer1 < offer2 then
    -1
  else 
    1
in
let rec get_choice_fun = fun () ->
  [std_print ("Receiving requests")];
  (receive with
     (sender, "get_choice") -> sender ! classify ("choice", choice) as {broker & user1}
   | ("best_offer", value, airline) -> [std_print ("Received best offer at value: " ^ value ^ " from " ^ airline)]);
  [get_choice_fun ()] in
[get_choice_fun ()]
