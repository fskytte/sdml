let t = (1, 2, "Hej") in
[std_print (match t with
              2 -> "Is two?"
            | (1, 2, "Hej") -> "Exact match")];
[std_print (match t with
              50 -> "Is 50?"
            | (1, 2, _) -> "Wildcard match")]
