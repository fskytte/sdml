let t = ("double", 5) in
    if [std_tuple_index t 0] = "id" then
      [std_tuple_index t 1]
    else if [std_tuple_index t 0] = "double" then
      [std_tuple_index t 1] * 2
    else "Unmatched"
