let name_server = [std_create_thread_id "localhost" 3000] in
    name_server ! ([std_current_thread ()], "register", "sync_server", [std_current_thread ()]);
    [std_print "Registered"];
    let received = receive in
    [std_print ("Received request" ^ [std_string_of_val received])];
    let sender = received{0} in
    let message = received{1} in
    sender ! message;
    [std_print "Response sent!"]
