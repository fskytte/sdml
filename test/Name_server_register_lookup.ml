[std_print "Registering name"];
[std_register "localhost" 3000 "test" [std_current_thread ()]];
[std_print "Looking up"];
let lookup_res = [std_lookup "localhost" 3000 "test"] in
[std_print ("Lookup result: " ^ [std_string_of_val lookup_res])]
