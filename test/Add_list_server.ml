[std_register "localhost" 3000 "add_list" [std_current_thread ()]];
let rec add_list = fun l acc -> if l = [] then
                                  acc
                                else
                                  [add_list (tl l) (acc + (hd l))]
in
let value = receive in
[std_print ("Received: " ^ [std_string_of_val value])];
let list = [std_tuple_index value 0] in
let sender = [std_tuple_index value 1] in
let result = [add_list list 0] in
[std_print result];
sender ! result
