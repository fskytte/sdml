let receiver = [std_start_thread (fun () -> [std_print "Receiving"];
                                            let received = receive in
                                            [std_print "Received"];
                                            [received ()])] in
    receiver ! (fun () -> [std_print "Yay"])
