using "test-module" as test;;
let name_server = [std_create_thread_id "localhost" 3000] in
    let lookup_res = name_server ! ([std_current_thread ()], "lookup", "module") in
    if [std_tuple_index lookup_res 0] = false then
      [std_print ("Delivery of message failed due to: " ^ [std_tuple_index lookup_res 1] ^ ": " ^ [std_tuple_index lookup_res 2])]
    else ();
    let server = receive in
    let send_res = server !{test} ([std_current_thread ()], "Hello") in
    if [std_tuple_index send_res 0] = false then
      [std_print ("Delivery of message failed due to: " ^ [std_tuple_index send_res 1] ^ ": " ^ [std_tuple_index send_res 2])]
    else ();
    let response = receive in
    [std_print "Received"];
    [std_print ("Received: " ^ [std_string_of_val response])]
