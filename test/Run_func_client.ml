let server = [std_lookup "localhost" 3000 "run"] in
    let a = 2 in
    let b = 3 in
    let adder = fun () -> [std_print (a + b)] in
    server ! adder
