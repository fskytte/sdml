let rec repl = fun () ->
  [std_print "Command:"];
  match [std_read ()] with
    "succ" -> [std_print "In succ"];
              [repl ()]
  | "pred" -> [std_print "In pred"];
              [repl ()]
  | "curr" -> [std_print "In curr"];
              [repl ()]
  | "store" -> [std_print "In store"];
               [repl ()]
  | "get" -> [std_print "In get"];
             [repl ()]
  | "join" -> [std_print "In join"];
              [repl ()]
  | "leave" -> [std_print "In leave"];
               [repl ()]
  | command -> [std_print ("Unknown: " ^ command)];
               [repl ()]
in [repl ()]
