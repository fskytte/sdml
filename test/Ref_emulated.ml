let rec reference = fun value ->
  let received = receive in
  let message = received{0} in
  let sender = received{1} in
  if message = "get" then
    sender ! value;
    [reference value]
  else if message = "set" then
    [reference received{2}]
  else
    [reference value]
in
let ref1 = [std_start_thread (fun () -> [reference 0])] in
[std_start_thread (fun () -> ref1 ! ("get", [std_current_thread ()]);
                             [std_print receive])];
ref1 ! ("set", [std_current_thread ()], 42)
