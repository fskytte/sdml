let loc = [std_new_lock ()] in
    [std_start_thread (fun () -> [std_print 1];
                                 [std_lock loc];
                                 [std_print 11];
                                 [std_unlock loc])];
    [std_start_thread (fun () -> [std_print 2];
                                 [std_lock loc];
                                 [std_print 12];
                                 [std_unlock loc])];
    [std_start_thread (fun () -> [std_print 3];
                                 [std_lock loc];
                                 [std_print 13];
                                 [std_unlock loc])]
