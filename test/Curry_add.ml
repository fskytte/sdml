let add = fun a b ->
  a + b
    in
    let add1 = [add 1] in
    let add2 = [add 2] in
    [add1 2]
