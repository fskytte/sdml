let name_server = [std_create_thread_id "localhost" 3000] in
    name_server ! ([std_current_thread ()], "register", "repeater", [std_current_thread ()]);
    [std_print "Registered"];
    let rec loop = fun () ->
      let received = receive in
      let sender = [std_tuple_index received 0] in
      let value = [std_tuple_index received 1] in
      sender ! value;
      [loop ()]
    in
    [loop ()]
