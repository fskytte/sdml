let receiver = [std_lookup "localhost" 3000 "receiver"] in
    [std_print ("Receiver: " ^ [std_string_of_val receiver])];
    let sent_val = (1,2,fun () -> "Hello") in
    receiver ! sent_val;
    [std_print ("Sent: " ^ [std_string_of_val sent_val])]
