let l = [|1, 2, 3|] in
let [|a, b, c|] = l in
[std_print ("Elements of list are: " ^ a ^ ", " ^ b ^ ", " ^ c)];
let a :: xs = l in
[std_print ("Head: " ^ a ^ " tail: " ^ [std_string_of_val xs])];
let [|a, _, c|] = l in
[std_print ("Elements of list, without 2nd element, are: " ^ a ^ ", " ^ c)]
