let create_map = fun () ->
  let add = fun map key value ->
    (key, value) :: map
  in
  let rec lookup = fun map key ->
      match map with
        () -> ()
      | (candidate, value) :: xs -> [std_print ("comparing " ^ [std_string_of_val candidate] ^ " and " ^ [std_string_of_val key])];
                                    if candidate = key then
                                      value
                                    else
                                      [lookup xs key]
  in
  let rec loop = fun map ->
    receive with
      (sender, "add", (key, value)) -> let new_map = [add map key value] in
                                       sender ! "added";
                                       [loop new_map]
    | (sender, "lookup", key) -> let res = [lookup map key] in
                                 sender ! ("found", res);
                                 [loop map]
    | err -> [std_print ("Unknown message: " ^ [std_string_of_val err])];
             [loop map]
  in
  [std_start_thread (fun () -> [loop []])]
in
let map = [create_map ()] in 
map ! ([std_current_thread ()], "add", ("test", 42));
receive with "added" -> map ! ([std_current_thread ()], "add", ("test2", 2)); 
                        receive with "added" -> map ! ([std_current_thread ()], "lookup", "test"); 
                                                receive with
                                                  ("found", res) -> [std_print ("test is: " ^ [std_string_of_val res])]
