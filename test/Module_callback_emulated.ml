let async_two_numbers = fun a callback -> [callback a 2] in
    [async_two_numbers 1 (fun a b -> [std_print ("Received: " ^ a ^ ", " ^ b)])]
