let name_server = [std_create_thread_id "localhost" 3000 "local"] in
    name_server ! ([std_current_thread ()], "lookup", "compute");
    let server = receive in
    let repeat_message = ([std_current_thread ()], fun () -> [std_print "This is a test"]; 3) in
    server ! repeat_message;
    let return_val = receive in
    [std_print ("Return val: " ^ return_val)];
    server ! repeat_message;
    let return_val = receive in
    [std_print ("Return val: " ^ return_val)]
