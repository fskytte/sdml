let name_server = [std_create_thread_id "localhost" 3000] in
    name_server ! ([std_current_thread ()], "lookup", "sync_server");
    let server = receive in
    [std_print ("Current thread before: " ^ [std_string_of_val [std_current_thread ()]])];
    let response = server !! 98 in
    [std_print ("Current thread after: " ^ [std_string_of_val [std_current_thread ()]])];
    [std_print ("Received: " ^ response)]
