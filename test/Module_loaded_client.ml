using "test-module" as test;;
let name_server = [std_create_thread_id "localhost" 3000 "local"] in
    let lookup_res = name_server ! ([std_current_thread ()], "lookup", "module") in
    if lookup_res{0} = false then
      [std_print ("Delivery of message failed due to: " ^ lookup_res{1} ^ ": " ^ lookup_res{2})]
    else
      let server = receive in
      [std_print "Server looked up"];
      let send_res = server !{test} ([std_current_thread ()], fun () -> [test.add 3 5]) in
      (if send_res{0} = false then
         [std_print ("Delivery of message failed due to: " ^ send_res{1} ^ ": " ^ send_res{2})]
       else
         [std_print "Request sent"];
         let response = receive in
         [std_print  ("Received: " ^ [std_string_of_val response])])
