[std_print (match [|1,2|] with
              2 :: _ -> "Starts with 2"
            | 1 :: _ -> "Starts with 1")];
[std_print (match [|1, 2|] with
              1 :: 2 :: () -> "Is [|1, 2|] exactly")]
