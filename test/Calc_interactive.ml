let do_calc = fun op a b -> if op = "+" then
                              [std_print "in +"];
                              a + b
                            else if op = "-" then
                              [std_print "in -"];
                              a - b
                            else if op = "*" then
                              [std_print "in *"];
                              a * b
                            else if op = "/" then
                              [std_print "in /"];
                              a / b
                            else
                              ()
    in
    let rec loop = fun () -> [std_print "Op:"];
                             let op = [std_read ()] in
                             [std_print "Number 1:"];
                             let num1 = [std_read ()] in
                             [std_print "Number 2:"];
                             let num2 = [std_read ()] in
                             let res = [do_calc op num1 num2] in
                             (if res = () then
                                [std_print ("Unknown operator: " ^ op)]
                              else
                                [std_print (num1 ^ op ^ num2 ^ " = " ^ res)]);
                             [loop ()]
    in [loop ()]
