let name_server = [std_create_thread_id "localhost" 3000 "local"] in
    name_server ! ([std_current_thread ()], "lookup", "int_server");
    let server = receive in
    server ! ([std_current_thread ()], fun c d -> c + d);
    let response = receive in
    [std_print ("Received: " ^ [std_string_of_val response])]
