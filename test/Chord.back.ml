let in_interval = fun typ left right value ->
  if typ = "open" then
    if left < right then
      (left < value) && (value < right)
    else
      if left > right then
        (left < value) || (value < right)
      else
        left <> value
  else if typ = "right_closed" then
    if left < right then
      (left < value) && (value <= right)
    else
      if left > right then
        (left < value) || (value <= right)
      else
        true
  else
    ()
in
let name_server = [std_create_thread_id "localhost" 3000] in
let peer = fun () ->
  let rec main_loop = fun pred succ id ->
    let find_successor = fun find_id response_id ->
      if [in_interval "right_closed" pred{0} id find_id] then
        response_id ! ("find_successor_res", ([std_current_thread ()], id))
      else
        succ{1} ! ("find_successor", response_id, find_id)
    in
    let received = receive in
    [std_print ("Received: " ^ [std_string_of_val received])];
    let sender = received{0} in
    let command = received{1} in
    if command = "find_successor" then
      let query_id = received{2} in
      [find_successor query_id sender];
      [main_loop pred succ id]
    else if command = "get_successor" then
      sender ! ("get_successor_res", succ);
      [main_loop pred succ id]
    else if command = "get_predecessor" then
      sender ! ("get_predecessor_res", pred);
      [main_loop pred succ id]
    else if command = "get_peer" then
      sender ! ("get_peer_res", ([std_current_thread ()], id));
      [main_loop pred succ id]
    else if command = "set_successor" then
      [main_loop pred (received{2}) id]
    else if command = "set_predecessor" then
      [main_loop (received{2}) succ id]
    else if command = "join" then
      let host = received{2} in
      let found_succ = host !! ("find_successor", id) in
      let succ = found_succ{1} in
      let pred_res = succ{0} !! "get_predecessor" in
      let pred = pred_res{1} in
      succ{0} ! ("set_predecessor", [std_current_thread ()], id);
      pred{0} ! ("set_successor", [std_current_thread ()], id);
      sender ! ("joined", [std_current_thread ()]);
      [main_loop pred succ id]
    else
      [main_loop pred succ id]
  in
  let id = [std_hash [std_current_thread ()]] in
  let this_peer = ([std_current_thread ()], id) in
  (if (name_server ! ([std_current_thread ()], "register", id, [std_current_thread ()])){0} then
     [std_print ("Registered as: " ^ id)]
   else
     [std_print "Registration failed"]);
  [main_loop this_peer this_peer id]
in
let this_peer = [std_start_thread peer] in
[std_print ("Current_thread: " ^ [std_string_of_val [std_current_thread ()]])];
[std_print ("This peer: " ^ [std_string_of_val this_peer])];
let rec repl = fun () ->
  [std_print "Command:"];
  let command = [std_read ()] in
  (if command = "succ" then
     [std_start_thread (fun () -> this_peer ! ([std_current_thread ()], "get_successor");
                                  [std_print ("Successor: " ^ [std_string_of_val receive{1}])];
                                  [repl ()])]
   else if command = "pred" then
     [std_start_thread (fun () -> this_peer ! ([std_current_thread ()], "get_predecessor");
                                  [std_print ("Predecessor: " ^ [std_string_of_val receive{1}])];
                                  [repl ()])]
   else if command = "curr" then
     [std_start_thread (fun () -> this_peer ! ([std_current_thread ()], "get_peer");
                                  [std_print ("Peer: " ^ [std_string_of_val receive{1}])];
                                  [repl ()])]
   else if command = "join" then
     [std_print "hostname:"];
     let hostname = [std_read ()] in
     [std_print "port:"];
     let port = [std_read ()] in
     let host_thread = [std_create_thread_id hostname port] in
     this_peer ! ([std_current_thread ()], ("join", host_thread));
     [repl ()]
   else
     [std_print ("Unknown command " ^ command)];
     [repl ()])
in
[std_start_thread repl];
()
