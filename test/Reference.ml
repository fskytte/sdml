let create_reference = fun init level ->
  let rec loop = fun v ->
    receive with
      ("read", sender) -> sender ! v; [loop v]
    | ("set", u, sender) -> sender ! "ok"; [loop v]
  in
  [std_start_thread (fun () -> [loop init]) level]
in ()
