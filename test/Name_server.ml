[std_print ("Running at: " ^ [std_string_of_val [std_current_thread ()]])];
let rec find_thread_id = fun name registered ->
  if registered = [] then
    ()
  else
    let candidate = hd registered in
    if candidate{0} = name then
      candidate{1}
    else
      [find_thread_id name (tl registered)]
in
let rec loop = fun registered ->
  let received = receive in
  let sender = received{0} in
  let command = received{1} in
  if command = "register" then
    let name = received{2} in
    let thread_id = received{3} in
    [std_print ("Registered: " ^ [std_string_of_val thread_id] ^ " as " ^ [std_string_of_val name])];
    [loop ((name, thread_id) :: registered)]
  else if command = "lookup" then
    let name = received{2} in
    let found_val = [find_thread_id name registered] in
    [std_print ("Looked up " ^ [std_string_of_val found_val] ^ " as " ^ [std_string_of_val name])];
    sender ! found_val;
    [loop registered]
  else
    [std_print ("Unknown command: " ^ [std_string_of_val command])];
    [loop registered]
in
[loop []]
