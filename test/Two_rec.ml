let rec a = fun () -> [std_print "a"] in
    let rec b = fun () -> [std_print "b"]; [a ()] in
    [b ()]
