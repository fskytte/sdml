let server = [std_lookup "localhost" 3000 "calc_server"] in
    server ! ("+", 1, 2, [std_current_thread ()]);
    [std_print ("1 + 2 = " ^ receive)];
    server ! ("+", 5, 6, [std_current_thread ()]);
    [std_print ("5 + 6 = " ^ receive)];
    server ! ("-", 3, 2, [std_current_thread ()]);
    [std_print ("3 - 2 = " ^ receive)];
    server ! ("*", 3, 2, [std_current_thread ()]);
    [std_print ("3 * 2 = " ^ receive)]
