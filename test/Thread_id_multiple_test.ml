let print_thread = fun () -> [std_print [std_current_thread ()]] in
    [std_start_thread print_thread];
    [std_start_thread print_thread];
    [std_start_thread print_thread];
    [print_thread ()]
