let rec map = fun f l ->
  if l = [] then
    []
  else
    [f (hd l)] :: [map f (tl l)]
    in
    [map (fun x -> x + 1) [|1, 2, 3|]]
