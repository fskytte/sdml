let a = 1 in
    [(fun () ->
      let b = 2 in
      [(fun () ->
        [(fun () ->
          let c = 3 in
          [(fun () ->
            [std_print a];
            [std_print b];
            [std_print c]) ()]
         ) ()]
       ) ()]
     ) ()]
