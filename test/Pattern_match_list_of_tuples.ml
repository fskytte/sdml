let a = [| ("a", 1), ("b", 2) |] in
    [std_print (match a with
                  () -> ()
                | (name, value) :: xs -> ("name: " ^ name ^ ", value: " ^ value))]
