let rec add_list = fun l acc -> if l = [] then
                                  acc
                                else
                                  [add_list (tl l) (acc + (hd l))]
    in
    [add_list [|1, 2, 3|] 0]
