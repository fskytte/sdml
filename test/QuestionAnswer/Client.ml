//Inspired by dDist question
let server = [std_create_thread_id "localhost" 3001] in
let rec write_answer_loop = fun () ->
  receive with
    (question, answer) -> [std_print ("The answer to '" ^ question ^ "' is '" ^ answer ^ "'")];
                          [write_answer_loop ()]
in
let print_thread = [std_start_thread write_answer_loop] in
let rec write_questions_loop = fun () ->
  let question = [std_read ()] in
  server ! (print_thread, question);
  [write_questions_loop ()]
in
[write_questions_loop ()]
