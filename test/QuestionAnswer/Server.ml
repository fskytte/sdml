//Inspired by dDist question
let answer_thread = [std_start_thread (fun () ->
                                       let rec receive_answer_loop = fun () ->
                                         receive with
                                           (sender, question) -> [std_print question];
                                                                 let answer = [std_read ()] in
                                                                 sender ! (question, answer);
                                                                 [receive_answer_loop ()]
                                       in [receive_answer_loop ()])]
in
let rec receive_questions_loop = fun () ->
  receive with
    question -> answer_thread ! question;
                [receive_questions_loop ()]
in [receive_questions_loop ()]
