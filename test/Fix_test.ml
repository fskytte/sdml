//let fix = fun f -> [(fun x -> [f (fun y -> [[x x] y])])
//                      (fun x -> [f (fun y -> [[x x] y])])]
//in
//let g = fun fct ->
//  fun n ->
//  if n = 0 then
//    1
//  else
//    n * [fct (n - 1)]
//in
//let factorial = [fix g] in
//[std_print [factorial 5]]

let fix = fun f -> [(fun x -> [f (fun y -> [[x x] y])])
                      (fun x -> [f (fun y -> [[x x] y])])]
in
let g = fun m ->
  fun f l ->
  if l = [] then
    []
  else
    [f (hd l)] :: [m f (tl l)]
in
let map = [fix g] in
let fix_poly = fun l ->
  [fix (fun self l -> [map (fun li x -> [li [self l] x]) l]) l]
in
let [|is_even, is_odd|] = [fix_poly [|(fun funs -> fun n -> let [|even, odd|] = funs in if n = 0 then true else [odd (n - 1)]),
                                      (fun funs -> fun n -> let [|even, odd|] = funs in if n = 0 then false else [even (n - 1)])|]] in
[std_print [is_even 4]]
