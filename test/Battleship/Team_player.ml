let take_turn = fun () ->
  [std_print "Column:"];
  let column = [std_read ()] in
  [std_print "Row:"];
  let row = [std_read ()] in
  (column, row)
in
let print_board = fun board ->
  [std_print "Your board: "];
  let rec helper = fun acc ->
    (if acc = 10 then
       ()
     else
       ([std_print board{acc}];
        [helper (acc + 1)]))
  in [helper 0]
in
let rec loop = fun take_vote ->
  let vote = if take_vote then [take_turn ()] else () in
  (receive with
     (server, "take_turn") -> server ! vote
   | "win" -> [std_print "You win!"]
   | "lose" -> [std_print "You lose!"]
   | "tie" -> [std_print "It was a tie!"]
   | (your_res, enemy_res) ->
      (match your_res with
         ("hit", column, row) -> [std_print ("Enemy hit at: (" ^ [std_string_of_val column] ^ ", " ^ [std_string_of_val row] ^ ")")]
       | ("not_hit", column, row) -> [std_print ("Enemy not hit at: (" ^ [std_string_of_val column] ^ ", " ^ [std_string_of_val row] ^ ")")]);
      (match enemy_res with
         ("hit", board) -> [std_print ("You are hit!")];
                           [print_board board]
       | ("not_hit", board) -> [std_print ("You are not hit!")];
                               [print_board board]));
  [loop (if take_vote then false else true)]
in
[std_print "Coordinator host:"];
let hostname = [std_read ()] in
[std_print "Coordinator port:"];
let port = [std_read ()] in
[std_print "Coordinator label:"];
let label = [std_read ()] in
[std_print "Message label:"];
let message_label = [std_read ()] in
let coordinator = [std_create_thread_id hostname port label message_label] in
coordinator ! ("player", [std_current_thread ()]);
[loop true]
