let create_map = fun label ->
  let add = fun map key value ->
    (key, value) :: map 
  in
  let rec lookup = fun map key ->
    match map with
      [] -> ()
    | (candidate, value) :: xs -> if candidate = key then
                                    value
                                  else
                                    [lookup xs key]
  in
  let rec loop = fun map ->
    receive with
      (sender, "add", (key, value)) -> let new_map = [add map key value] in
                                       sender ! "added";
                                       [loop new_map]
    | (sender, "lookup", key) -> let res = [lookup map key] in
                                 sender ! ("found", res);
                                 [loop map]
    | (sender, "get_list") -> sender ! ("list", map);
                              [loop map]
    | (sender, "reset_data") -> sender ! "data_reset";
                                [loop []]
    | err -> [std_print ("Unknown message: " ^ [std_string_of_val err])];
             [loop map]
  in
  [std_start_thread (fun () -> [loop []]) label]
in
let length_of_list = fun l ->
  let rec helper = fun l length ->
    match l with
      [] -> length
    | x :: xs -> [helper xs (length + 1)]
  in [helper l 0]
in
let take_turn = fun players ->
  let rec send_vote_requests = fun remaining_players ->
    match remaining_players with
      [] -> ()
    | x :: xs -> x ! ([std_current_thread ()], "take_turn");
                 [send_vote_requests xs]
  in
  let rec gather_votes = fun votes remaining ->
    match remaining with
      0 -> votes
    | _ -> receive with
             (column, row) -> [gather_votes ((column, row) :: votes) (remaining - 1)]
  in
  let map = [create_map "coordinator1"] in
  let rec find_most_votes = fun votes most_votes candidate ->
    match votes with
      [] -> candidate
    | vote :: xs -> map ! ([std_current_thread ()], "lookup", vote);
                    receive with
                      ("found", res) -> let new_val = if res = () then 1 else res + 1 in
                                        map ! ([std_current_thread ()], "add", (vote, new_val));
                                        receive with
                                          "added" -> if new_val > most_votes then
                                                       [find_most_votes xs new_val vote]
                                                     else
                                                       [find_most_votes xs most_votes candidate]
  in
  [send_vote_requests players];
  let votes = [gather_votes [] [length_of_list players]] in
  [std_print ("Votes: " ^ declassify [std_string_of_val votes] as {coordinator1})];
  let most_votes = [find_most_votes (declassify votes as {coordinator1})
                                    1
                                    (declassify (hd votes) as {coordinator1})] in 
  [std_print ("Most votes: " ^ [std_string_of_val most_votes])];
  most_votes
in
let rec distribute_message = fun players message ->
  match players with
    [] -> ()
  | x :: xs -> x ! message;
               [distribute_message xs message]
in
let rec loop = fun players ->
  (receive with
     (server, "take_turn") declassify as {_} -> let move = [take_turn players] in
                                                server ! move
   | "win" declassify as {_} -> [std_print "We won!"];
                                [distribute_message players "win"]
   | "lose" declassify as {_} -> [std_print "We lost!"];
                                 [distribute_message players "lose"]
   | "tie" declassify as {_} -> [std_print players "we tied!"];
                                [distribute_message "tied"]
   | (our_res, enemy_res) declassify as {_} -> [std_print ("Our: " ^ [std_string_of_val our_res])];
                                               [std_print ("Enemy: " ^ [std_string_of_val enemy_res])];
                                               [distribute_message (declassify players as {_}) (declassify (our_res, enemy_res) as {_})]
  );
  [loop players]
in
let rec get_players = fun players ->
  [std_print ("Current players: " ^ [std_string_of_val players])];
  [std_print "Add more players?"];
  let cont = declassify [std_read ()] as {_} in
  if cont = "n" then
    players
  else 
    receive with
      ("player", thread) declassify as {_} -> [get_players (declassify thread as {coordinator1} :: players)]
in
let players = [get_players classify [] as {coordinator1}] in
[loop players]
