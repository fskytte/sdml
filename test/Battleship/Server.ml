//Rules etc from http://www.papg.com/show?1TMC
let player_1_side = ((0,0,0,0,0,0,0,0,0,0),
                     (0,1,1,1,0,0,0,0,1,0),
                     (0,0,0,0,0,1,0,0,1,0),
                     (0,1,0,0,0,1,0,0,1,0),
                     (0,0,0,0,0,0,0,0,1,0),
                     (0,0,0,0,0,1,0,0,1,0),
                     (0,0,0,0,0,0,0,0,0,0),
                     (0,0,0,1,1,1,1,0,0,0),
                     (0,0,0,0,0,0,0,0,0,0),
                     (1,1,0,0,0,0,0,0,0,0)) in
let player_2_side = ((1,1,1,1,1,0,0,0,0,0),
                     (1,1,1,1,0,0,0,0,0,0),
                     (1,1,1,0,0,0,0,0,0,0),
                     (1,1,0,1,1,0,0,0,0,0),
                     (1,0,1,0,0,0,0,0,0,0),
                     (0,0,0,0,0,0,0,0,0,0),
                     (0,0,0,0,0,0,0,0,0,0),
                     (0,0,0,0,0,0,0,0,0,0),
                     (0,0,0,0,0,0,0,0,0,0),
                     (0,0,0,0,0,0,0,0,0,0)) in
let total_ship_fields = 18 in
let player1 = [std_create_thread_id "localhost" 3001 "coordinator1" "coordinator1 & server"] in
let player2 = [std_create_thread_id "localhost" 3002 "player2" "player2 & server"] in
let check_hit = fun board column row ->
  board{column}{row} = 1
in
let rec game = fun board1 board1_remaining board2 board2_remaining ->
  player1 ! declassify ([std_current_thread ()], "take_turn") as {coordinator1};
  player2 ! declassify ([std_current_thread ()], "take_turn") as {player2};
  receive with
    //(column1, row1) originally {coordinator1} declassify as {server} ->
    (column1, row1) originally {coordinator1} declassify as {_} ->             
                 receive with
                 //(column2, row2) originally {player2} declassify as {server} ->
                    (column2, row2) originally {player2} declassify as {_} ->
                            let hit1 = [check_hit board2 column1 row1] in
                            let hit2 = [check_hit board1 column2 row2] in
                            let board1_remaining = if hit2 then board1_remaining - 1 else board1_remaining in
                            let board2_remaining = if hit1 then board2_remaining - 1 else board2_remaining in
                            let new_board1 = (if hit2 then
                                                let new_column = [std_change_tuple board1{column2} row2 (-1)] in
                                                [std_change_tuple board1 column2 new_column]
                                              else
                                                board1) in
                            let new_board2 = (if hit1 then
                                                let new_column = [std_change_tuple board2{column1} row1 (-1)] in
                                                [std_change_tuple board2 column1 new_column]
                                              else
                                                board2) in
                            (match (hit1, hit2) with
                               (true, true) -> player1 ! declassify (("hit", column1, row1), ("hit", new_board1)) as {coordinator1};
                                               player2 ! declassify (("hit", column2, row2), ("hit", new_board1)) as {player2}
                             | (true, false) -> player1 ! declassify (("hit", column1, row1), ("not_hit", new_board1)) as {coordinator1};
                                                player2 ! declassify (("not_hit", column2, row2), ("hit", new_board2)) as {player2}
                             | (false, true) -> player1 ! declassify (("not_hit", column1, row1), ("hit", new_board1)) as {coordinator1};
                                                player2 ! declassify (("hit", column2, row2), ("not_hit", new_board2)) as {player2}
                             | (false, false) -> player1 ! declassify (("not_hit", column1, row1), ("not_hit", new_board1)) as {coordinator1};
                                                 player2 ! declassify (("not_hit", column2, row2), ("not_hit", new_board2)) as {player2});
                            [std_print ("Remaining ship fields:\nPlayer1: " ^ board1_remaining ^ "\tPlayer2: " ^ board2_remaining)];
                            if (board1_remaining = 0) && (board2_remaining = 0) then
                              player1 ! declassify "tie" as {coordinator1};
                              player2 ! declassify "tie" as {player2}
                            else if board1_remaining = 0 then
                              player2 ! declassify "win" as {player2};
                              player1 ! declassify "lose" as {coordinator1}
                            else if board2_remaining = 0 then
                              player1 ! declassify "lose" as {coordinator1};
                              player2 ! declassify "win" as {player2}
                            else
                              [game new_board1 board1_remaining new_board2 board2_remaining]
in
[game player_1_side total_ship_fields player_2_side total_ship_fields]
