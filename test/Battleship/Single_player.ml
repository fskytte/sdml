let take_turn = fun () ->
  [std_print "Column:"];
  let column = [std_read ()] in
  [std_print "Row:"];
  let row = [std_read ()] in
  (column, row)
in
let print_board = fun board ->
  [std_print "Your board: "];
  let rec helper = fun acc ->
    (if acc = 10 then
       ()
     else
       ([std_print board{acc}];
        [helper (acc + 1)]))
  in [helper 0]
in
let rec loop = fun () ->
  (receive with
     (server, "take_turn") -> let ans = [take_turn ()] in
                              server ! ans
   | "win" -> [std_print "You win!"]
   | "lose" -> [std_print "You lose!"]
   | "tie" -> [std_print "It was a tie!"]
   | (your_res, enemy_res) ->
      (match your_res with
         ("hit", column, row) -> [std_print ("Enemy hit at: (" ^ [std_string_of_val column] ^ ", " ^ [std_string_of_val row] ^ ")")]
       | ("not_hit", column, row) -> [std_print ("Enemy not hit at: (" ^ [std_string_of_val column] ^ ", " ^ [std_string_of_val row] ^ ")")]);
      (match enemy_res with
         ("hit", board) -> [std_print ("You are hit!")];
                           [print_board board]
       | ("not_hit", board) -> [std_print ("You are not hit!")];
                               [print_board board]));
  [loop ()]
in
[loop ()]
