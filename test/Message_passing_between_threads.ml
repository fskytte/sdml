let receiver =  [std_start_thread (fun () -> [std_print "Receiving..."]; [std_print ("Receiver received: " ^ receive)])] in
    [std_start_thread (fun () -> [std_print "Sending message"];  receiver ! ("Hello from: " ^ [std_current_thread ()]))];
    [std_print "Main done"]
