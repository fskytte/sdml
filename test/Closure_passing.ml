let exec = [std_start_thread (fun () -> let value = receive in
                                        let func = value{0} in
                                        let sender = value{1} in
                                        let res = [func ()] in
                                        sender ! res)]
    in
    let out = fun () ->
      let a = 2 in
      fun () -> a
    in
    let inner = [out ()] in
    exec ! (inner, [std_current_thread ()]);
    [std_print ("Received: " ^ [std_string_of_val receive])]
