[std_register "localhost" 3000 "compute" [std_current_thread ()]];
[std_print "Registered"];
let rec loop = fun () ->
  let value = receive in
  let func = [std_tuple_index value 0] in
  let sender = [std_tuple_index value 1] in
  let result = [func ()] in
  sender ! result;
  [loop ()]
in [loop ()]
