let server = [std_start_thread (fun () ->
                                let value = receive in
                                let func = [std_tuple_index value 0] in
                                let sender = [std_tuple_index value 1] in
                                let result = [func ()] in
                                sender ! result)] in
    let a = 25 in
    let b = 53 in
    server ! (fun () -> a + b, [std_current_thread ()]);
    [std_print ("Received: " ^ [std_string_of_val receive])]
