let server_thread = [std_create_thread_id "localhost" 3000 "local"] in
let x = 10 in
let f = fun () -> [std_print [std_string_of_val x]] in
//let g = fun () -> [f ()] in
//  let rec g = fun () -> [c ()]
//  and c = fun () -> [std_print "Yay"] in
[std_print "Expecting output:"];
[f ()];
server_thread ! f
