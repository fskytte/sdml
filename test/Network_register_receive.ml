[std_register "localhost" 3000 "receiver" [std_current_thread ()]];
[std_print "Registered"];
let rec receiver = fun () ->
  let value = receive in
  [std_print ("Received: " ^ [std_string_of_val value])];
  [std_print ("Result of last fun: " ^ [[std_tuple_index value 2] ()])];
  [receiver ()]
in
[receiver ()]
