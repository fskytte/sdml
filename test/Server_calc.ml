let calc = [std_start_thread (fun () -> let rec loop = fun () ->
                                          let input = receive in
                                          (if [std_tuple_index input 0] = "+" then
                                             [std_tuple_index input 3] ! ([std_tuple_index input 1] + [std_tuple_index input 2])
                                           else if [std_tuple_index input 0] = "-" then
                                             [std_tuple_index input 3] ! ([std_tuple_index input 1] - [std_tuple_index input 2])
                                           else
                                             [std_tuple_index input 3] ! "Unknown input");
                                          [loop ()]
                                        in [loop ()])]
    in
    calc ! ("+", 1, 2, [std_current_thread ()]);
    [std_print ("1 + 2 = " ^ receive)];
    calc ! ("+", 5, 6, [std_current_thread ()]);
    [std_print ("5 + 6 = " ^ receive)];
    calc ! ("-", 3, 2, [std_current_thread ()]);
    [std_print ("3 - 2 = " ^ receive)];
    calc ! ("*", 3, 2, [std_current_thread ()]);
    [std_print ("3 * 2 = " ^ receive)]
