let a = 2 in
    [std_current_thread ()] ! a;
    receive with
      a {_} -> "Matches a"
    | a {local} -> "Matches local"
