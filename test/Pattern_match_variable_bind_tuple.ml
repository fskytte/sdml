[std_print (match ("hello", 2, 3, 4) with
              ("hi", a, b, _) -> "Message was hi with: " ^ a ^ b
            | ("hello", a, _, b) -> "Message was hello with: " ^ a ^ ", " ^ b)]
