using "test-module" as t;;
let name_server = [std_create_thread_id "localhost" 3000 "local"] in
    let register_res = name_server ! ([std_current_thread ()], "register", "module", [std_current_thread ()]) in
    if register_res{0} = false then                 
      [std_print ("Delivery of message failed due to: " ^ register_res{1} ^ ": " ^ register_res{2})]
    else
      [std_print "Registered"];
      let rec loop = fun () ->
        let received = receive in
        let sender = received{0} in
        let func = received{1} in
        [std_print "Calling func:"];
        let ret_val = [func ()] in
        [std_print "Func called"];
        let ret_res = sender !{t} ret_val in
        (if ret_res{0} = false then
           [std_print ("Delivery of message failed due to: " ^ ret_res{1} ^ ": " ^ ret_res{2})]
         else
           [std_print "Sent res"]);
        [loop ()]
      in [loop ()]
