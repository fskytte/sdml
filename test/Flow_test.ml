let create_cell = fun v ->
  let rec loop = fun v ->
    receive with
      ("read", cb) -> cb ! v; [loop v]
    | ("set", u, cb) -> cb ! "ok"; [loop u]
  in
  [loop v]
    in 
let public = [std_start_thread (fun () -> [create_cell 1]) "local"] in 
let temp = [std_start_thread (fun () -> [create_cell 0]) "local"] in
let secret = classify 1 as {a & local} in
(if secret then
  temp ! ("set", 1, [std_current_thread ()]);
  receive with "ok" -> ()
 else ());
temp ! ("read", [std_current_thread ()]);
let t = receive in
(if t <> 1 then
   public ! ("set", 0, [std_current_thread ()]);
   receive with "ok" ->()
 else ());
public ! ("read", [std_current_thread ()]);
let res = receive in
[std_print res]
