let x = 10 in
let used = fun x -> x + 1
in let f = fun () ->
             [used x]
in
[std_current_thread ()] ! f
