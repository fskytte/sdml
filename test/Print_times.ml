let rec print_times = fun message times ->
  if times = 0 then
    ()
  else
    [std_print (message + times)]; [print_times message (times - 1)]
    in [print_times 0 10]
