let name_server = [std_create_thread_id "localhost" 3000] in
    name_server ! ([std_current_thread ()], "lookup", "repeater");
    let server = receive in
    server ! ([std_current_thread ()], "hello");
    let response = receive in
    [std_print ("Received: " + [std_string_of_val response])]
