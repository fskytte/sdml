let server_thread = [std_create_thread_id "localhost" 3000 "local"] in
let x = 10 in
let f = fun () -> [std_print [std_string_of_val x]] in
server_thread ! ([std_current_thread ()], f);
let g = receive in
[g ()]
