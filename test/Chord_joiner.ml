[std_print "Hostname:"];
let hostname = [std_read ()] in
[std_print "Port:"];
let port = [std_read ()] in
[std_print "Join host:"];
let join_host = [std_read ()] in
[std_print "Join port:"];
let join_port = [std_read ()] in
let host_thread = [std_create_thread_id hostname port "local" "local"] in
let join_thread = [std_create_thread_id join_host join_port "local" "local"] in
host_thread ! ([std_current_thread ()], "get_peer");
receive with
  (_, (peer, _)) -> peer ! ([std_current_thread ()], "join", join_thread);
                    receive with
                      "joined" -> [std_print "Peer has joined"]
