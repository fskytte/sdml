let rec append = fun l1 l2 ->
  if l1 = [] then
    l2
  else
    (hd l1) :: [append (tl l1) l2]
    in
    [std_print [append [|1, 2, 3|] [|4, 5, 6|]]]
