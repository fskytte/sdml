let a = ("Hello", 1, 2, 3) in
(let (a, b, c, d) = a in   
 [std_print ("Complete information was: " ^ a ^ ", " ^ b ^ ", " ^ c ^ ", " ^ d)]);
let (a, _, c, d) = a in
[std_print ("Ignoring the second entry: " ^ a ^ ", " ^ c ^ ", " ^ d)]
