let x = 10 in
let rec f = fun () -> [std_print "Yay"]
and g = fun () -> [std_print "Nay"] in
[f ()];
[std_print x]
