//using "map-module" as m;;

let in_interval = fun typ left right value ->
  match typ with
    "open" -> if left < right then
                (left < value) && (value < right)
              else
                if left > right then
                  (left < value) || (value < right)
                else
                  left <> value
  | "right_closed" -> if left < right then
                        (left < value) && (value <= right)
                      else
                        if left > right then
                          (left < value) || (value <= right)
                        else
                          true
  | _ -> ()
in
let create_map = fun () ->
  let add = fun map key value ->
    //[m.add map key value]
    (key, value) :: map 
  in
  let rec lookup = fun map key ->
    //[m.get map key]
    match map with
      [] -> ()
    | (candidate, value) :: xs -> if candidate = key then
                                    value
                                  else
                                    [lookup xs key]
  in
  let rec loop = fun map ->
    receive with
      (sender, "add", (key, value)) -> let new_map = [add map key value] in
                                       sender ! "added";
                                       [loop new_map]
    | (sender, "lookup", key) -> let res = [lookup map key] in
                                 sender ! ("found", res);
                                 [loop map]
    | (sender, "get_list") -> sender ! ("list", map);
                              [loop map]
    | (sender, "reset_data") -> sender ! "data_reset";
                                //[m.reset map];
                                //[loop map]
                                [loop []]
    | err -> [std_print ("Unknown message: " ^ [std_string_of_val err])];
             [loop map]
  in
  [std_start_thread (fun () -> [loop []]) "local"]
  //[std_start_thread (fun () -> [loop [m.create_map ()]]) "local"]
in
let peer = fun () ->
  let rec main_loop = fun pred succ id map ->
    let (pred_thread, pred_id) = pred in
    let (succ_thread, succ_id) = succ in
    let peer_thread = [std_current_thread ()] in
    let find_successor = fun find_id response_thread ->
      if [in_interval "right_closed" pred_id id find_id] then
        response_thread ! ("find_successor_res", (peer_thread, id))
      else 
        succ_thread ! (response_thread, "find_successor", find_id)
    in
    receive with
      (sender, "find_successor", query_id) ->
       [std_start_thread (fun () -> [find_successor query_id sender]) "local"];
       [main_loop pred succ id map]
    | (sender, "get_successor") ->
       sender ! ("get_successor_res", succ);
       [main_loop pred succ id map]
    | (sender, "get_predecessor") ->
       sender ! ("get_predecessor_res", pred);
       [main_loop pred succ id map]
    | (sender, "get_peer") ->
       sender ! ("get_peer_res", ([std_current_thread ()], id));
       [main_loop pred succ id map]
    | (sender, "set_successor", new_succ) ->
       [main_loop pred new_succ id map]
    | (sender, "set_predecessor", new_pred) ->
       [main_loop new_pred succ id map]
    | (sender, "store_data", (key, value)) -> 
       [std_start_thread (fun () -> 
                          (let hash = [std_hash key] in
                           [find_successor hash [std_current_thread ()]];
                           receive with
                             ("find_successor_res", (hash_succ, _)) -> hash_succ ! ([std_current_thread ()], "store_data_intern", (hash, value));//New thread started to avoid blocking our own messages with guarded receives, if we are the successor
                                                                       receive with
                                                                         "added" -> sender ! "added")) "local"];
       [main_loop pred succ id map]
    | (sender, "get_data", key) ->
       [std_start_thread (fun () ->
                          (let hash = [std_hash key] in
                           [find_successor hash [std_current_thread ()]];
                           receive with
                             ("find_successor_res", (hash_succ, _)) -> hash_succ ! ([std_current_thread ()], "get_data_intern", hash);
                                                                       receive with
                                                                         ("found", res) -> sender ! ("found", res))) "local"];
       [main_loop pred succ id map]
    | (sender, "store_data_intern", (key, value)) ->
       [std_start_thread (fun () ->
                          ([std_print ("Storing " ^ [std_string_of_val key])];
                           map ! ([std_current_thread ()], "add", (key, value));
                           receive with
                             "added" -> sender ! "added")) "local"];
       [main_loop pred succ id map]
    | (sender, "get_data_intern", key) ->
       [std_start_thread (fun () ->
                          (map ! ([std_current_thread ()], "lookup", key);
                           receive with
                             ("found", res) -> sender ! ("found", res))) "local"];
       [main_loop pred succ id map]
    | (sender, "take_complete_data", data) ->
       [std_start_thread (fun () ->
                          ([std_print ("Taking stored data from " ^ [std_string_of_val sender])];
                           let rec add_data = fun l ->
                             match l with
                               [] -> ()
                             | x :: xs -> map ! ([std_current_thread ()], "add", x);
                                          receive with
                                                "added" -> [add_data xs]
                           in [add_data data])) "local"];
       [main_loop pred succ id map]
    | (sender, "join", host) ->
       host ! ([std_current_thread ()], "get_peer");
       receive with
         (_, (host_thread, _)) -> host_thread ! ([std_current_thread ()], "find_successor", id);
                                  (receive with
                                     (_, (new_succ_thread, new_succ_id)) -> new_succ_thread ! ([std_current_thread ()], "get_predecessor");
                                                                            (receive with
                                                                               (_, (new_pred_thread, new_pred_id)) -> new_succ_thread ! ([std_current_thread ()], "set_predecessor", ([std_current_thread ()], id));
                                                                                                                      new_pred_thread ! ([std_current_thread ()], "set_successor", ([std_current_thread ()], id));
                                                                                                                      sender ! "joined";
                                                                                                                      [main_loop (new_pred_thread, new_pred_id) (new_succ_thread, new_succ_id) id map]))
    | (sender, "leave") ->
       (succ_thread ! ([std_current_thread ()], "set_predecessor", pred);
        pred_thread ! ([std_current_thread ()], "set_successor", succ);
        map ! ([std_current_thread ()], "get_list");
        (receive with
           ("list", l) -> succ_thread ! ([std_current_thread ()], "take_complete_data", l));
        sender ! "left";
        let own_peer = ([std_current_thread ()], id) in
        map ! ([std_current_thread ()], "reset_data");
        receive with
          "data_reset" ->
          [main_loop own_peer own_peer id map])
    | a -> [std_print ("Received unknown msg: " ^ [std_string_of_val a])];
           [main_loop pred succ id map]
  in
  let id = [std_hash [std_current_thread ()]] in
  let this_peer = ([std_current_thread ()], id) in
  let map = [create_map ()] in
  [main_loop this_peer this_peer id map]
in
let this_peer = [std_start_thread peer "local"] in
let rec repl = fun () ->
  [std_print "Command:"];
  match [std_read ()] with
    "succ" -> this_peer ! ([std_current_thread ()], "get_successor");
              (receive with
                 ("get_successor_res", succ) -> [std_print ("Successor: " ^ [std_string_of_val succ])]);
              [repl ()]
  | "pred" -> this_peer ! ([std_current_thread ()], "get_predecessor");
              (receive with
                 ("get_predecessor_res", pred) -> [std_print ("Predecessor: " ^ [std_string_of_val pred])]);
              [repl ()]
  | "curr" -> this_peer ! ([std_current_thread ()], "get_peer");
              (receive with
                 ("get_peer_res", peer) -> [std_print ("Peer: " ^ [std_string_of_val peer])]);
              [repl ()]
  | "store" -> [std_print "key:"];
               let key = [std_read ()] in
               [std_print "value:"];
               let value = [std_read ()] in
               this_peer ! ([std_current_thread ()], "store_data", (key, value));
               (receive with
                  "added" -> [std_print "Added value"]);
               [repl ()]
  | "get" -> [std_print "key:"];
             let key = [std_read ()] in
             this_peer ! ([std_current_thread ()], "get_data", key);
             (receive with
                ("found", res) -> [std_print ("Found: " ^ [std_string_of_val res])]);
             [repl ()]
  | "join" -> [std_print "hostname:"];
              let hostname = [std_read ()] in
              [std_print "port:"];
              let port = [std_read ()] in
              let host_thread = [std_create_thread_id hostname port "local" "local"] in
              this_peer ! ([std_current_thread ()], "join", host_thread);
              (receive with
                 "joined" -> [std_print "Peer has joined successfully"]
               | a -> [std_print ("Peer failed to join? " ^ [std_string_of_val a])]);
              [repl ()]
  | "leave" -> this_peer ! ([std_current_thread ()], "leave");
               (receive with
                  "left" -> [std_print "Peer has left successfully"]
                | a -> [std_print ("Peer failed to leave? " ^ [std_string_of_val a])]);
               [repl ()]
  | command -> [std_print ("Unknown command " ^ command)];
               [repl ()]
in
[std_start_thread repl "local"];
let rec receive_join = fun () ->
  (receive with
     (sender, "get_peer") -> this_peer ! (sender, "get_peer")
   | a -> [std_print ("Main thread does not understand: " ^ a)]);
  [receive_join ()]
in [receive_join ()]
