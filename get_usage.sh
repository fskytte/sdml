#https://unix.stackexchange.com/a/30371
$1 &
pid=$!
echo "pid: $pid" > $2
echo " CPU MEM - time" >> $2
#https://stackoverflow.com/a/40576129
while sleep 0.5
do
    #https://stackoverflow.com/a/4651495
    #https://unix.stackexchange.com/a/348826
    #https://unix.stackexchange.com/a/58541
    usage="$(top -p $pid -b -n1 | awk '/node/ {print $9, $10}')"
    #https://stackoverflow.com/a/18704121
    time="$(date +"%T.%3N")"
    echo "${usage} - ${time}" >> $2
done
